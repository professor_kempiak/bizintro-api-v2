# Domain used by the backend API (probably localhost)
export BACKEND_DOMAIN=localhost
# Port used by the backend API (probably 8443)
export BACKEND_PORT=8443

# Domain used by the Frontend Web Application (used for creating links in emails)
export FRONTEND_DOMAIN=app.bizintro.com

# IP of the PostgreSQL Server
export DATABASE_IP=127.0.0.1

# Intercom Keys
export INTERCOM_KEY=qtodg71u
export INTERCOM_API_KEY=dG9rOjdmNmRkYmZlX2E2NTBfNGUxNl9hZmZkXzI3ZmVkNTdmMGY5OToxOjA=

# Office 365 Keys
export MS_KEY=bc1c7434-be47-4162-86d9-9ccbe287335c
export MS_SECRET=oD58p0aab8QF05crhWSg6AE

# Stripe Keys
export STRIPE_PK=blah
export SK=blah

# Debug On/Off
export DEBUG=False