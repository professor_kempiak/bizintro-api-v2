current_time=$(date "+%Y.%m.%d-%H.%M.%S")
if [ -d "/home/django/bizintro" ]; then
	new_file="bizintro.orig."$current_time
	sudo mv /home/django/bizintro /home/django/$new_file
fi

if [ -d "/home/django/php" ]; then
	sudo rm -Rf /home/django/php/
fi

sudo cp server-conf-files/gunicorn.conf /etc/init/
sudo cp server-conf-files/django /etc/nginx/sites-enabled

sudo cp server-conf-files/gunicorn.py /etc/gunicorn.d/
sudo cp server-conf-files/celery/celery.conf /etc/supervisor/conf.d/celery.conf
sudo cp server-conf-files/ssl/* /etc/nginx/ssl/
sudo cp -r django/bizintro /home/django/
sudo cp -r php /home/django/ 

sudo -E cat /home/django/bizintro/bizintro/settings.py | sed "s/DOMAINTOREPLACE/${BACKEND_DOMAIN}/" | sed "s/FRONTENDTOREPLACE/${FRONTEND_DOMAIN}/" | sed "s/INTERCOM_KEY_REPLACE/${INTERCOM_KEY}/" | sed "s/INTERCOM_API_KEY_REPLACE/${INTERCOM_API_KEY}/" | sed "s/DATABASE_URL/${DATABASE_IP}/"| sed "s/MSKEY/${MS_KEY}/" | sed "s/MSSECRET/${MS_SECRET}/" | sed "s/DEBUG = True/DEBUG = ${DEBUG}/" | sed "s/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS = \['${BACKEND_DOMAIN}'\]/" | sed "s/pk_test/${STRIPE_PK}/" | sed "s/sk_test/${STRIPE_SK}/" | sudo tee /home/django/bizintro/bizintro/settings2.py 1>/dev/null
sudo mv /home/django/bizintro/bizintro/settings2.py /home/django/bizintro/bizintro/settings.py

sudo service gunicorn restart
sudo service nginx reload
#sudo supervisorctl reread
#sleep 1
#sudo supervisorctl reload
#sleep 5
#sudo supervisorctl restart celery
