export PYTHONWARNINGS=ignore
echo "CREATE SCHEMA django;CREATE USER django PASSWORD '0zMT6P4F6b';GRANT ALL ON SCHEMA django TO django;GRANT ALL ON ALL TABLES IN SCHEMA django TO django;CREATE DATABASE django;" > /tmp/b.sql
su postgres -c "psql postgres < /tmp/b.sql"
cd /home/django/bizintro/
python manage.py migrate auth
python manage.py migrate contenttypes
python manage.py migrate sites
python manage.py migrate --fake-initial bizintro
python manage.py migrate djstripe
python manage.py djstripe_init_customers
python manage.py djstripe_init_plans
python manage.py migrate 
