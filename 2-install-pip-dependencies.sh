pip install pyasn1==0.1.8
pip install pyasn1-modules==0.0.6
pip install urllib3[secure]
pip install amqp==1.4.9
pip install anyjson==0.3.3
pip install apt-xapian-index==0.45
pip install autoflake==0.7
pip install beautifulsoup4==4.4.1
pip install billiard==3.3.0.22
pip install certifi==2015.9.6.2
pip install Cheetah==2.4.4
pip install cloud-init==0.7.5
pip install colorama==0.2.5
pip install configobj==4.7.2
pip install coreapi==2.3.0
pip install coreschema==0.0.4
pip install cssselect==0.9.1
pip install cssutils==1.0.1
pip install Django==1.8.3
pip install django-appconf==1.0.1
pip install django-braces==1.8.1
pip install django-celery==3.1.17
pip install django-cors-headers==2.0.2
pip install django-debug-toolbar==1.3.2
pip install django-dirtyfields==0.7
pip install django-haystack==2.6.1
pip install django-jsonify==0.3.0
pip install django-model-utils==2.4
pip install django-pdb==0.5.1
pip install django-rest-swagger==2.1.2
pip install djangorestframework==3.2.4
pip install dj-stripe==0.7.0
pip install celery==3.1.20
pip install celery-with-redis==3.0
pip install drfdocs==0.0.11
pip install elasticsearch==5.4.0
pip install future==0.15.2
pip install gdata==2.0.18
pip install gevent==1.0
pip install google-api-python-client==1.4.1
pip install greenlet==0.4.2
pip install gunicorn==17.5
pip install html5lib==0.999
pip install httplib2==0.9.1
pip install icalendar==3.9.1
pip install inflection==0.3.1
pip install itypes==1.1.0
pip install Jinja2==2.9.6
pip install jsonfield==1.0.3
pip install jsonpatch==1.3
pip install jsonpointer==1.0
pip install kombu==3.0.33
pip install Landscape-Client==14.12
pip install lxml==3.3.3
pip install Markdown==2.6.2
pip install MarkupSafe==1.0
pip install meld3==0.6.10
pip install Metafone==0.5
pip install newrelic==2.72.1.53
pip install oauth==1.0.1
pip install oauth2==1.5.211
pip install oauth2client==1.4.12
pip install oauthlib==1.0.3
pip install openapi-codec==1.3.1
pip install PAM==0.4.2
pip install premailer==2.9.6
pip install prettytable==0.7.2
pip install psycopg2==2.7
pip install pycrypto==2.3
pip install pycurl==7.19.3
pip install pyflakes==1.5.0
pip install PyJWT==1.4.0
pip install pyserial==2.6
pip install python-crfsuite==0.8.4
pip install python-gflags==2.0
pip install python-intercom==3.1.0
pip install python-openid==2.2.5
pip install python-social-auth==0.2.13
pip install pytz==2015.4
pip install PyYAML==3.10
pip install redis==2.10.5
pip install requestbuilder==0.1.0-beta2
pip install requests==2.5.1
pip install requests-oauthlib==0.5.0
pip install rest-social-auth==1.1.0
pip install rollbar==0.13.8
pip install rsa==3.1.4
pip install simplejson==3.8.0
pip install six==1.9.0
pip install social-auth-app-django==0.1.0
pip install social-auth-core==0.2.1
pip install South==0.7.5
pip install sqlparse==0.1.15
pip install stripe==1.27.1
pip install supervisor==3.0b2
pip install Twisted-Core==13.2.0
pip install Twisted-Names==13.2.0
pip install Twisted-Web==13.2.0
pip install unicodecsv==0.14.1
pip install uritemplate==0.6
pip install usaddress==0.5.5
pip install virtualenv==1.11.4
pip install vobject==0.9.2