<?php

if(isset($_REQUEST['debug'])) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
} else {
    error_reporting(E_NONE);
    ini_set("display_errors", 0);
}

// TODO: Look at this: https://github.com/Garethp/php-ews


/**
 * Function to autoload the requested class name.
 *
 * @param string $class_name Name of the class to be loaded.
 * @return boolean Whether the class was loaded or not.
 */
function __autoload($class_name)
{
    // Start from the base path and determine the location from the class name,
    $base_path = '/home/django/php/php-ews';
    $include_file = $base_path . '/' . $class_name . '.php';
    if(file_exists($include_file))
        return require_once $include_file;
    
    $include_file = $base_path . '/' . str_replace('_', '/', $class_name) . '.php';
    return (file_exists($include_file) ? require_once $include_file : false);
}

/*$config = array(
  'server' => 'mail.ex2.secureserver.net',
  'email' => 'mike@chateaudemontigny.com',
  'password' => 'Norton01#'
);*/

$config = array(
  'server' => $_REQUEST['server'],
  'email' => $_REQUEST['email'],
  'password' => $_REQUEST['password']
);

// set response type to json, when we're not debugging
if(!isset($_REQUEST['debug']))
    header('Content-Type: application/json');

if($_REQUEST['type'] == 'autodiscover') {
    $json = array('success' => false);
    
    try {
        $ews = EWSAutodiscover::getEWS($config['email'], $config['password']);
        if($ews) {
            $server = $ews->getServer();
            $json['data'] = array('server' => $server);
            $json['success'] = !empty($server);
        } else {
            if(isset($_REQUEST['debug'])) {
                $auto = new EWSAutodiscover($config['email'], $config['password']);
                $auto->discover();
                $json['discovered'] = $auto->discoveredSettings();
            }
        }
    } catch(Exception $e) {
        $json['message'] = $e->getMessage();
    }
    
    echo json_encode($json);
    exit;
}

$ews = new ExchangeWebServices($config['server'], $config['email'], $config['password']);

if($_REQUEST['type'] == 'getContacts') {
    $json = array('success' => false);
    
    try {
        $request = new EWSType_FindItemType();
        $request->ItemShape = new EWSType_ItemResponseShapeType();
        $request->ItemShape->BaseShape = EWSType_DefaultShapeNamesType::ALL_PROPERTIES;
    
        $request->ParentFolderIds = new EWSType_NonEmptyArrayOfBaseFolderIdsType();
        $request->ParentFolderIds->DistinguishedFolderId = new EWSType_DistinguishedFolderIdType();
        $request->ParentFolderIds->DistinguishedFolderId->Id = EWSType_DistinguishedFolderIdNameType::CONTACTS;
    
        $request->Traversal = EWSType_ItemQueryTraversalType::SHALLOW;
    
        $response = $ews->FindItem($request);
    
        if(isset($_REQUEST['debug'])) {
            echo '<pre>'.print_r($response, true).'</pre>';
        }
    
        $contacts = array();
    
        // Loop through each item
        if($response->ResponseMessages->FindItemResponseMessage->RootFolder->TotalItemsInView > 0) {
            $raw_contacts = $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->Contact;
            if(!is_array($raw_contacts))
                $raw_contacts = array($raw_contacts);
            
            foreach($raw_contacts as $raw_contact) {
                $contact = array(
                    'DisplayName' => $raw_contact->DisplayName,
                    'GivenName' => $raw_contact->GivenName,
                    'MiddleName' => $raw_contact->MiddleName,
                    'Surname' => $raw_contact->Surname,
                    'JobTitle' => $raw_contact->JobTitle,
                    'CompanyName' => $raw_contact->CompanyName,
                    'Id' => $raw_contact->ItemId->Id,
                    'EmailAddresses' => array(),
                    'PhoneNumbers' => array(),
                    'PhysicalAddresses' => array()
                );
            
                foreach($raw_contact->EmailAddresses->Entry as $raw_email) {
                    if($raw_email->_) {
                        array_push($contact['EmailAddresses'], array(
                            'Key' => $raw_email->Key,
                            'Address' => $raw_email->_
                        ));
                    }
                }
            
                foreach($raw_contact->PhoneNumbers->Entry as $raw_phone) {
                    if($raw_phone->_) {
                        array_push($contact['PhoneNumbers'], array(
                            'Key' => $raw_phone->Key,
                            'Phone' => $raw_phone->_
                        ));
                    }
                }
            
                foreach($raw_contact->PhysicalAddresses->Entry as $raw_address) {
                    if($raw_address->Street || $raw_address->City || $raw_address->State || $raw_address->PostalCode || $raw_address->CountryOrRegion) {
                        array_push($contact['PhysicalAddresses'], $raw_address);
                    }
                }
            
                // TODO: add any other details we need
            
                if(isset($_REQUEST['raw'])) {
                    $contact['raw'] = $raw_contact;
                }
            
                array_push($contacts, $contact);
            }
        }
        
        $json['success'] = ($response->ResponseMessages->FindItemResponseMessage->ResponseCode == EWSType_ResponseCodeType::NO_ERROR);
        $json['data'] = $contacts;
    } catch(Exception $e) {
        $json['message'] = $e->getMessage();
    }
    
    echo json_encode($json);
} else if($_REQUEST['type'] == 'getCalendars') {
    $json = array('success' => false);
    
    try {
        $request = new EWSType_FindFolderType();
        $request->FolderShape = new EWSType_FolderResponseShapeType();
        $request->FolderShape->BaseShape = EWSType_DefaultShapeNamesType::DEFAULT_PROPERTIES;
    
        $request->ParentFolderIds = new EWSType_NonEmptyArrayOfBaseFolderIdsType();
        $request->ParentFolderIds->DistinguishedFolderId = new EWSType_DistinguishedFolderIdType();
        $request->ParentFolderIds->DistinguishedFolderId->Id = EWSType_DistinguishedFolderIdNameType::ROOT;
    
        $request->Traversal = EWSType_FolderQueryTraversalType::DEEP;
    
        $response = $ews->FindFolder($request);
    
        if(isset($_REQUEST['debug'])) {
            echo '<pre>'.print_r($response, true).'</pre>';
        }
    
        $calendars = array();
    
        if($response->ResponseMessages->FindFolderResponseMessage->RootFolder->TotalItemsInView > 0) {
            $raw_calendars = $response->ResponseMessages->FindFolderResponseMessage->RootFolder->Folders->CalendarFolder;
            if(!is_array($raw_calendars))
                $raw_calendars = array($raw_calendars);
            
            foreach($raw_calendars as $raw_calendar) {
                $calendar = array(
                    'id' => $raw_calendar->FolderId->Id,
                    'name' => $raw_calendar->DisplayName
                );
        
                if(isset($_REQUEST['raw'])) {
                    $calendar['raw'] = $raw_calendar;
                }
        
                array_push($calendars, $calendar);
            }
        }
        
        $json['success'] = ($response->ResponseMessages->FindFolderResponseMessage->ResponseCode == EWSType_ResponseCodeType::NO_ERROR);
        $json['data'] = $calendars;
    } catch(Exception $e) {
        $json['message'] = $e->getMessage();
    }
    
    echo json_encode($json);
} else if($_REQUEST['type'] == 'getFreeBusy') {
    $json = array('success' => false);
    
    try {
        // mysql style date - 2016-01-12 -- but we might be better off switching to a full ISO8601 datetime, with time zone included
        $start_date = $_REQUEST['startDate'];
        $end_date = $_REQUEST['endDate'];
        $time_zone = isset($_REQUEST['timeZone']) ? $_REQUEST['timeZone'] : 'Z';
        $calendar_id = isset($_REQUEST['calendar']) ? $_REQUEST['calendar'] : false;

        $request = new EWSType_FindItemType();
        $request->ItemShape = new EWSType_ItemResponseShapeType();
        $request->ItemShape->BaseShape = EWSType_DefaultShapeNamesType::ALL_PROPERTIES;
    
        // Define the timeframe to load calendar items
        $request->CalendarView = new EWSType_CalendarViewType();
        //$request->CalendarView->StartDate = $start_date.'T00:00:00'.$time_zone; // an ISO8601 date e.g. 2012-06-12T15:18:34+03:00
        //$request->CalendarView->EndDate = $end_date.'T11:59:59'.$time_zone; // an ISO8601 date later than the above
        $request->CalendarView->StartDate = $start_date; // an ISO8601 date e.g. 2012-06-12T15:18:34+03:00
        $request->CalendarView->EndDate = $end_date; // an ISO8601 date later than the above

        $request->ParentFolderIds = new EWSType_NonEmptyArrayOfBaseFolderIdsType();
        
        if(!$calendar_id) {
            $request->ParentFolderIds->DistinguishedFolderId = new EWSType_DistinguishedFolderIdType();
            $request->ParentFolderIds->DistinguishedFolderId->Id = EWSType_DistinguishedFolderIdNameType::CALENDAR;
        } else {
            $request->ParentFolderIds->FolderId = new EWSType_FolderIdType();
            $request->ParentFolderIds->FolderId->Id = $calendar_id;
        }
    
        $request->Traversal = EWSType_ItemQueryTraversalType::SHALLOW;
    
        $response = $ews->FindItem($request);
    
        if(isset($_REQUEST['debug'])) {
            echo '<pre>'.print_r($response, true).'</pre>';
        }
    
        $events = array();
    
        // Loop through each item if event(s) were found in the timeframe specified
        if ($response->ResponseMessages->FindItemResponseMessage->RootFolder->TotalItemsInView > 0){
            $raw_events = $response->ResponseMessages->FindItemResponseMessage->RootFolder->Items->CalendarItem;
            if(!is_array($raw_events))
                $raw_events = array($raw_events);
            
            $UTC = new DateTimeZone("UTC");
            $newTZ = new DateTimeZone($time_zone);
                
            foreach($raw_events as $raw_event) {
                $start_date = new DateTime( $raw_event->Start, $UTC );
                $start_date->setTimezone( $newTZ );
                $end_date = new DateTime( $raw_event->End, $UTC );
                $end_date->setTimezone( $newTZ );
                
                $event = array(
                    'title' => $raw_event->Subject,
                    'start' => $start_date->format('Y-m-d') . 'T' . $start_date->format('H:i:sP'),
                    'end' => $end_date->format('Y-m-d') . 'T' . $end_date->format('H:i:sP')
                );
            
                if(isset($_REQUEST['raw'])) {
                    $event['raw'] = $raw_event;
                }
            
                array_push($events, $event);
            }
        }
        
        $json['success'] = ($response->ResponseMessages->FindItemResponseMessage->ResponseCode == EWSType_ResponseCodeType::NO_ERROR);
        $json['data'] = $events;
    } catch(Exception $e) {
        $json['message'] = $e->getMessage();
    }
    
    echo json_encode($json);
} else if($_REQUEST['type'] == 'createAppointment') {
    $json = array('success' => false);
    
    try {
        $calendar_id = isset($_REQUEST['calendar']) ? $_REQUEST['calendar'] : false;
        $subject = $_REQUEST['subject'];
        $location = $_REQUEST['location'];
        $body = $_REQUEST['body'];
        $start_date = $_REQUEST['startDate']; // should be ISO8601 with timezone
        $end_date = $_REQUEST['endDate']; // should be ISO8601 with timezone
    
        $email1 = $_REQUEST['email1'];
        $email2 = $_REQUEST['email2'];
        $name1 = $_REQUEST['name1'];
        $name2 = $_REQUEST['name2'];
    
        $emails = array(
            array('name' => $name1, 'email' => $email1),
            array('name' => $name2, 'email' => $email2)
        );
    
        /*
        data = {
            'Subject': subject,
            'Body': {'Content Type': 'HTML', 'Content': body},
            'Start': appt_start_date_time_tz_aware.strftime('%Y-%m-%dT%H:%M:%SZ'),
            'End': appt_end_date_time_tz_aware.strftime('%Y-%m-%dT%H:%M:%SZ'),
            'Location': {'DisplayName': location},
            'Attendees': [
                {'EmailAddress': {'Address': self.user.email, 'Name': self.user.profile.full_name()}, 'Type': 'Required'},
                {'EmailAddress': {'Address': recipient.email, 'Name': recipient.profile.full_name()}, 'Type': 'Required'},
            ],
        }
        */
    
    
        // Start building the request.
        $request = new EWSType_CreateItemType();
        
        if($calendar_id) {
            $request->SavedItemFolderId = new EWSType_TargetFolderIdType();
            $request->SavedItemFolderId->FolderId = new EWSType_FolderIdType();
            $request->SavedItemFolderId->FolderId->Id = $calendar_id;
        }
        
        $request->Items = new EWSType_NonEmptyArrayOfAllItemsType();
        $request->Items->CalendarItem = new EWSType_CalendarItemType();

        // Set the subject and location
        $request->Items->CalendarItem->Subject = $subject;
        $request->Items->CalendarItem->Location = $location;

        // Set the start and end times. For Exchange 2007, you need to include the timezone offset.
        // For Exchange 2010, you should set the StartTimeZone and EndTimeZone properties.
        $request->Items->CalendarItem->Start = $start_date;
        $request->Items->CalendarItem->End = $end_date;

        // Set no reminders
        $request->Items->CalendarItem->ReminderIsSet = false;

        // Or use this to specify when reminder is displayed (if this is not set, the default is 15 minutes)
        // $request->Items->CalendarItem->ReminderMinutesBeforeStart = 15;

        // Build the body.
        $request->Items->CalendarItem->Body = new EWSType_BodyType();
        $request->Items->CalendarItem->Body->BodyType = EWSType_BodyTypeType::HTML;
        $request->Items->CalendarItem->Body->_ = $body;

        // Set the item class type (not required).
        $request->Items->CalendarItem->ItemClass = new EWSType_ItemClassType();
        $request->Items->CalendarItem->ItemClass->_ = EWSType_ItemClassType::APPOINTMENT;

        // Set the sensativity of the event (defaults to normal).
        $request->Items->CalendarItem->Sensitivity = new EWSType_SensitivityChoicesType();
        $request->Items->CalendarItem->Sensitivity->_ = EWSType_SensitivityChoicesType::NORMAL;

        // If we want, we can add categories to the event
        // $request->Items->CalendarItem->Categories = new EWSType_ArrayOfStringsType();
        // $request->Items->CalendarItem->Categories->String = array('Bizintro');

        // Set the importance of the event.
        $request->Items->CalendarItem->Importance = new EWSType_ImportanceChoicesType();
        $request->Items->CalendarItem->Importance->_ = EWSType_ImportanceChoicesType::NORMAL;

        // Don't send meeting invitations.
        $request->SendMeetingInvitations = EWSType_CalendarItemCreateOrDeleteOperationType::SEND_TO_NONE;
    
        $request->Items->CalendarItem->RequiredAttendees = new EWSType_NonEmptyArrayOfAttendeesType();
        $request->Items->CalendarItem->RequiredAttendees->Attendee = array();
    
        foreach($emails as $email) {
            $attendee = new EWSType_AttendeeType();
            $attendee->Mailbox = new EWSType_EmailAddressType();
            $attendee->Mailbox->Name = $email['name'];
            $attendee->Mailbox->EmailAddress = $email['email'];
            array_push($request->Items->CalendarItem->RequiredAttendees->Attendee, $attendee);
        }

        $response = $ews->CreateItem($request);
    
        if(isset($_REQUEST['debug'])) {
            echo '<pre>'.print_r($response, true).'</pre>';
        }
    
        $response_code = $response->ResponseMessages->CreateItemResponseMessage->ResponseCode;
        $response_class = $response->ResponseMessages->CreateItemResponseMessage->ResponseClass;
    
        $json['success'] = ($response->ResponseMessages->CreateItemResponseMessage->ResponseCode == EWSType_ResponseCodeType::NO_ERROR);
        $json['data'] = array(
            'code' => $response_code,
            'class' => $response_class
        );
    } catch(Exception $e) {
        $json['message'] = $e->getMessage();
    }
    
    echo json_encode($json);
} else if($_REQUEST['type'] == 'testCredentials') {
    $json = array('success' => false);
    
    try {
        $request = new EWSType_GetFolderType();
        $request->FolderShape = new EWSType_FolderResponseShapeType();
        $request->FolderShape->BaseShape = EWSType_DefaultShapeNamesType::DEFAULT_PROPERTIES;
    
        $request->FolderIds = new EWSType_NonEmptyArrayOfBaseFolderIdsType();
        $request->FolderIds->DistinguishedFolderId = new EWSType_DistinguishedFolderIdType();
        $request->FolderIds->DistinguishedFolderId->Id = EWSType_DistinguishedFolderIdNameType::ROOT;
    
        $response = $ews->GetFolder($request);
    
        if(isset($_REQUEST['debug'])) {
            echo '<pre>'.print_r($response, true).'</pre>';
        }
        
        $json['success'] = ($response->ResponseMessages->GetFolderResponseMessage->ResponseCode == EWSType_ResponseCodeType::NO_ERROR);
    } catch(Exception $e) {
        $json['message'] = $e->getMessage();
    }
    
    echo json_encode($json);
}

/*

https://john.bizintro.com/exchange_api.php?type=getContacts&server=mail.ex2.secureserver.net&email=mike@chateaudemontigny.com&password=Norton01%23

https://john.bizintro.com/exchange_api.php?type=createAppointment&subject=Bizintro%20Test&body=This%20is%20a%20test%20meeting&startDate=2016-01-12T14:00:00&endDate=2016-01-12T16:00:00&email1=mike@chateaudemontigny.com&name1=Mike%20Norton&email2=john@nortoncrew.com&name2=John%20Norton&location=

https://john.bizintro.com/exchange_api.php?type=createAppointment&subject=Bizintro%20Test%203&body=This%20is%20another%20test%20meeting&startDate=2016-01-12T15:00:00Z&endDate=2016-01-12T16:00:00Z&email1=mike@chateaudemontigny.com&name1=Mike%20Norton&email2=john@nortoncrew.com&name2=John%20Norton&location=Virtual%20(Skype)
*/

?>