current_time=$(date "+%Y.%m.%d-%H.%M.%S")
if [ -d "/home/django/bizintro" ]; then
	new_file="bizintro.orig."$current_time
	mv /home/django/bizintro /home/django/$new_file
fi

if [ -d "/home/django/php" ]; then
	rm -Rf /home/django/php/
fi

if [ -f "/etc/nginx/sites-enabled/default" ]; then
	rm /etc/nginx/sites-enabled/default
fi

cp server-conf-files/gunicorn.conf /etc/init/
cp server-conf-files/django /etc/nginx/sites-enabled
cp server-conf-files/elasticsearch.yml /etc/elasticsearch/

mkdir -p /etc/nginx/ssl/
mkdir -p /var/log/celery/
mkdir -p /etc/gunicorn.d/

cp server-conf-files/gunicorn.py /etc/gunicorn.d/
cp server-conf-files/celery/celery.conf /etc/supervisor/conf.d/celery.conf
cp server-conf-files/ssl/* /etc/nginx/ssl/
cp -r django/bizintro /home/django/
cp -r php /home/django/ 

cat /home/django/bizintro/bizintro/settings.py | sed "s/DOMAINTOREPLACE/${BACKEND_DOMAIN}/" | sed "s/FRONTENDTOREPLACE/${FRONTEND_DOMAIN}/" | sed "s/INTERCOM_KEY_REPLACE/${INTERCOM_KEY}/" | sed "s/INTERCOM_API_KEY_REPLACE/${INTERCOM_API_KEY}/" | sed "s/DATABASE_URL/${DATABASE_IP}/"| sed "s/MSKEY/${MS_KEY}/" | sed "s/MSSECRET/${MS_SECRET}/" | sed "s/DEBUG = True/DEBUG = ${DEBUG}/" | sed "s/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS = \['${BACKEND_DOMAIN}'\]/" | sed "s/pk_test/${STRIPE_PK}/" | sed "s/sk_test/${STRIPE_SK}/" > /home/django/bizintro/bizintro/settings2.py
mv /home/django/bizintro/bizintro/settings2.py /home/django/bizintro/bizintro/settings.py

touch /var/log/django.log
chmod 666 /var/log/django.log

service elasticsearch restart
service gunicorn restart
service nginx reload
supervisorctl reread
sleep 1
sudo supervisorctl reload
sleep 5
supervisorctl restart celery
