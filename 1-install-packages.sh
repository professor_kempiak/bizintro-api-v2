echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" | tee -a /etc/apt/sources.list.d/pgdg.list
echo "deb https://packages.elastic.co/elasticsearch/2.x/debian stable main" | tee -a /etc/apt/sources.list.d/elasticsearch-2.x.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install -y git
apt-get install -y supervisor
apt-get install -y redis-server
apt-get install -y nginx
apt-get install -y python-pip
apt-get install -y libpq-dev
apt-get install -y python-dev
apt-get install -y php5-fpm php5-curl
apt-get install -y postgresql-9.3
apt-get install -y openjdk-7-jdk
apt-get install -y python-lxml
apt-get install -y elasticsearch=2.3.1
