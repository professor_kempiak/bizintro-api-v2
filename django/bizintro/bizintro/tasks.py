from datetime import datetime

from bizintro import celery_app
from bizintro.helpers import combine_contacts, sync_contacts
from bizintro.models import AsyncJob, UserProfile, ImportContact, FullContactData

import logging
from django.conf import settings
import requests
import json

import urllib2




logger = logging.getLogger(__name__)

@celery_app.task(rate_limit="1/s")
def FullContactTask(fullcontact_id):
 
    contact = FullContactData.objects.get(id=fullcontact_id)
    logger.info("calling fullcontact for %s" % contact.email)
    api_key = settings.FULL_CONTACT_API_KEY
    req = urllib2.Request('https://api.fullcontact.com/v3/person.enrich')
    req.add_header('Authorization', 'Bearer %s' % api_key)
    url = "https://api.fullcontact.com/v3/person.enrich"
    data = json.dumps({
        "email": contact.email 
    })
    response = urllib2.urlopen(req,data)
    data = json.loads(response.read())

    contact.data=data
    usefulData = {}
        
    try:
        usefulData["location"] = data["location"]
    except:
        usefulData["location"] = ""
    
    try:
        topics = []
        for topic in data["details"]["topics"]:
            topics.append(topic["name"])
        usefulData["topics"] = ", ".join(topics)
    except: 
        usefulData["topics"] = []
    
    try:
        usefulData["bio"] = data["bio"]
    except:
        usefulData["bio"] = ""

    try:
       usefulData["avatar"] = data["avatar"]
    except:
        usefulData["avatar"] = ""

    #try:
    #    positions = []
    #    for position in data["organizations"]:
    #        positions.append({"current":position["current"],"title":position["position"],"company":position["name"]})
    #    usefulData["positions"] = positions
    #except: 
    #    usefulData["positions"] = []

    contact.usefuldata = usefulData
    contact.last_checked=datetime.now()
    contact.save()

    return True

@celery_app.task()
def CombineTask(user_profile_id, source):
 
    # Update the state. The meta data is available in task.info dicttionary
    # The meta data is useful to store relevant information to the task
    # Here we are storing the upload progress in the meta. 
 
    CombineTask.update_state(state='PROGRESS', meta={'progress': 50})

    try:
        if source != "all":
            combine_contacts(user_profile_id,source)
        else:
            user_profile = UserProfile.objects.get(id=user_profile_id)
            sources = list(ImportContact.objects.filter(contact__user_profile=user_profile).exclude(source=ImportContact.MANUAL).values_list('source', flat=True).distinct())
            for thesource in sources:
                combine_contacts(user_profile_id,thesource)

        status = AsyncJob.SUCCESS
        message = "Contact merging and enhancements completed successfully!"
    except:
        status = AsyncJob.FAILED
        message = "Contact merging and enhangements failed..."
      
    job = AsyncJob.objects.get(user_profile_id=user_profile_id,task_id=CombineTask.request.id)
    job.status = status
    job.message = message
    job.end_date_time=datetime.now()
    job.save()
    return True

@celery_app.task()
def SyncContactsTask(user_profile_id, source):
    SyncContactsTask.update_state(state='PROGRESS', meta={'progress': 50})
    try:
        any_errors = sync_contacts(user_profile_id, source)
        
        if any_errors:
            message = "Contact syncing failed for one of your accounts."
            status = AsyncJob.FAILED
        else:
            message = "Contact syncing completed successfully!"
            status = AsyncJob.SUCCESS
    except:
        status = AsyncJob.FAILED
        message = "Contact syncing failed..."
      
    job = AsyncJob.objects.get(user_profile_id=user_profile_id,task_id=SyncContactsTask.request.id)
    job.status = status
    job.message = message
    job.end_date_time=datetime.now()
    job.save()
    return True

