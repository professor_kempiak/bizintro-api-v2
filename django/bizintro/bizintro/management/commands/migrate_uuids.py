import uuid

from django.core.management.base import BaseCommand

from bizintro. models import Appt, ApptRequest, Introduction, PotentialIntroduction, UserProfile


class Command(BaseCommand):
    """
    When adding the UUID field to each of these models, Django can't generate a
    unique UUID for each object at the SQL level; rather, each object gets the
    same "default" UUID. This script iterates through all the objects in these
    tables and generates a new unique value.
    """

    def handle(self, *args, **options):
        for model in (Appt, ApptRequest, Introduction, PotentialIntroduction, UserProfile):
            for obj in model.objects.all():
                obj.uuid = uuid.uuid4()
                obj.save()
