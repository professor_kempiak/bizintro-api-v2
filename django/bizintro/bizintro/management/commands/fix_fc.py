import uuid

from django.core.management.base import BaseCommand

from bizintro.models import FullContactData


class Command(BaseCommand):
    """
    When adding the UUID field to each of these models, Django can't generate a
    unique UUID for each object at the SQL level; rather, each object gets the
    same "default" UUID. This script iterates through all the objects in these
    tables and generates a new unique value.
    """

    def handle(self, *args, **options):
        for contact in FullContactData.objects.all():
            data = contact.data
            usefulData = {}
                
            try:
                usefulData["location"] = data["demographics"]["locationGeneral"]
            except:
                usefulData["location"] = ""
            
            try:
                topics = []
                for topic in data["digitalFootprint"]["topics"]:
                    topics.append(topic["value"])
                usefulData["topics"] = ", ".join(topics)
            except: 
                usefulData["topics"] = []
            
            try:
                linkedin = ""
                for profile in data["socialProfiles"]:
                    if profile["typeName"] == "LinkedIn":
                        linkedin = profile["bio"]
                usefulData["bio"] = linkedin
            except:
                usefulData["bio"] = ""

            try:
                avatar = ""
                for photo in data["photos"]:
                    if photo["type"] == "linkedin":
                        avatar = photo["url"]
                usefulData["avatar"] = avatar
            except:
                usefulData["avatar"] = ""

            try:
                positions = []
                for position in data["organizations"]:
                    positions.append({"current":position["current"],"title":position["title"],"company":position["name"]})
                usefulData["positions"] = positions
            except: 
                usefulData["positions"] = []

            contact.usefuldata = usefulData
            contact.save()