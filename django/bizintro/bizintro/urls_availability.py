from django.conf.urls import url
from bizintro.apis import availability

urlpatterns = (
    url(r'^$', availability.AvailabilityView.as_view(), name='availability'),
)