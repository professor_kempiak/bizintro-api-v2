import datetime
from haystack import indexes
from bizintro.models import Contact


class ContactIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    #user_profile = indexes.CharField(model_attr='user_profile', null=True)
    user_profile = indexes.IntegerField(model_attr='user_profile__pk', null=True)
    last_name = indexes.CharField(model_attr='last_name')
    #first_name = indexes.CharField(model_attr='first_name')
    #contact_id = indexes.CharField(model_attr='id')
    #emails = indexes.MultiValueField(model_attr='emails')

    def get_model(self):
        return Contact
    def index_queryset(self, using=None):
        return self.get_model().objects.all()
