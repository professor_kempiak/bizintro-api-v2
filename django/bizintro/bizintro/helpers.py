import datetime
import logging
import re
import time
import urllib

import httplib2
import usaddress
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.template.loader import get_template
from djstripe.decorators import subscriber_passes_pay_test
from gdata.gauth import OAuth2Token
from intercom.client import Client
from oauth2client.django_orm import Storage as DjangoStorage
from social.apps.django_app.default.models import UserSocialAuth

import vobject
from bizintro.models import Activity, ApptRequest, Contact, Help, Introduction, License, PotentialIntroduction, User, ContactShare, ImportContact, UserProfile, AsyncJob, UPCache, Nudge

logger = logging.getLogger(__name__)


def onboarding_required(function=None):
    """Check that the user is onboarded

    This decorator ensures that the view functions it is called on can be 
    accessed only by onboarded users. When a non-onboarded user accesses
    such a protected view, they are redirected to the onboarding page.
    """
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            emails = request.user.emailaddress_set.filter(is_primary=True)
            contacts_count = Contact.objects.filter(user_profile=request.user.profile).count()

            has_contacts = False
            if contacts_count > 9:
                has_contacts = True

            has_verified_email = False
            if len(emails) > 0 and emails[0].is_verified():
                has_verified_email = True

            try:
                intro_uuid = request.GET["intro_uuid"]
            except:
                intro_uuid = None

            if (has_contacts and has_verified_email) or intro_uuid:
                return view_func(request, *args, **kwargs)
            else:
                return redirect("/#not_onboarded")

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)

class SocialAPI(object):
    logger = None  # override in subclass

    def __init__(self, user, social_auth):
        self.user = user
        self.social_auth = social_auth

    def _get_log_suffix_and_args(self):
        suffix = ' for user %d and social auth %d'
        extra_args = (self.user.id, self.social_auth.id)
        return suffix, extra_args

    def _log_message(self, log_function, msg, *args, **kwargs):
        suffix, extra_args = self._get_log_suffix_and_args()
        msg += suffix
        args += extra_args
        log_function(msg, *args, **kwargs)

    def log_debug(self, msg, *args, **kwargs):
        self._log_message(self.logger.debug, msg, *args, **kwargs)

    def log_info(self, msg, *args, **kwargs):
        self._log_message(self.logger.info, msg, *args, **kwargs)

    def log_warning(self, msg, *args, **kwargs):
        self._log_message(self.logger.warning, msg, *args, **kwargs)

    def log_error(self, msg, *args, **kwargs):
        self._log_message(self.logger.error, msg, *args, **kwargs)

    def log_exception(self, msg, *args, **kwargs):
        self._log_message(self.logger.exception, msg, *args, **kwargs)

    def log_critical(self, msg, *args, **kwargs):
        self._log_message(self.logger.critical, msg, *args, **kwargs)


class Provider(object):
    """String enum for various python-social-auth providers."""
    Google = 'google-oauth2'
    Microsoft = 'microsoft-oauth2'
    LinkedIn = 'linkedin-oauth2'
    Salesforce = 'salesforce-oauth2'
    Exchange = 'exchange'
    Yahoo = 'yahoo-oauth2'

    Calendars = frozenset([Google, Microsoft, Exchange])


def send_event_to_intercom(event_name, user):
    try:
        intercom = Client(personal_access_token=settings.INTERCOM_API_KEY)
        intercom.events.create(event_name=event_name, email=user.email, created_at=long(time.mktime(datetime.datetime.now().timetuple())))
        intercom_user = intercom.users.find(email=user.email)
        intercom_user.custom_attributes["appointments_offered"] = ApptRequest.objects.filter(user_profile=user.profile).count()
        intercom_user.custom_attributes["introductions_offered"] = Introduction.objects.filter(user_profile=user.profile).count()
        intercom_user.custom_attributes["helps_offered"] = Help.objects.filter(user_profile=user.profile).count()
        intercom_user.custom_attributes["contacts"] = Contact.objects.filter(user_profile=user.profile).count()
        intercom.users.save(intercom_user)
    except:
        pass

def get_user_contacts(user):
    """
    Returns a list of Contact objects for a given user, setting the `bizintro_user_uuid` property on each Contact.
    """
    contacts = Contact.objects.filter(user_profile=user.profile)
    return find_bizintro_users(contacts)

def search_user_contacts(user, params={}):
    """
    Returns an object with a filtered list of Contact objects for a given user, setting the `bizintro_user_uuid` property on each Contact.
    """
    start = int(params.get('start', 0))
    length = int(params.get('length', 0))
    
    contacts = Contact.objects.filter(user_profile=user.profile).order_by('last_name')
    returnObj = {'recordsTotal': contacts.count()}

    search = params.get('q', '')

    if not search:
        search = params.get('search[value]', '')
        
    source = params.get('source', '')
    if search:
        parts = search.split(' ',2)
        if len(parts) == 2:
            contacts = contacts.filter(Q(first_name__istartswith=parts[0]), Q(last_name__istartswith=parts[1])).order_by('first_name', 'last_name', 'email')
            if source:
                contacts = contacts.filter(importcontact__source=source)        
            contacts = list(contacts)
        else:
            like_search = '%' + search + '%'

            if source:
                contacts = Contact.objects.raw("select bizintro_contact.* from bizintro_contact,bizintro_importcontact where bizintro_contact.user_profile_id = %s and bizintro_importcontact.source = %s and bizintro_contact.id = bizintro_importcontact.contact_id and (exists (select 1 from unnest(emails) as un(email) where un.email ilike %s) or first_name ilike %s or last_name ilike %s) order by first_name,last_name,email",[user.profile.id,source,like_search,like_search,like_search])
            else:
                contacts = Contact.objects.raw("select * from bizintro_contact where user_profile_id = %s and (exists (select 1 from unnest(emails) as un(email) where un.email ilike %s) or first_name ilike %s or last_name ilike %s) order by first_name,last_name,email",[user.profile.id,like_search,like_search,like_search])
                #contacts = Contact.objects.raw("select * from bizintro_contact where bizintro_contact.user_profile_id = %s and bizintro_importcontact = %s and bizintro_contact.id = bizintro_importcontact.contact_id and ((exists (select 1 from unnest(emails) as un(email) where un.email ilike %s) or first_name ilike %s or last_name ilike %s) order by first_name,last_name,email",[source,like_search,like_search,like_search])
            contacts = list(contacts)
    elif source:
        contacts = contacts.filter(importcontact__source=source)        

        

    if source == 'b':
        # need to do some crazy filtering here to get proper list of bizintro users
        find_bizintro_users(contacts)
        bizintro_contacts = [contact for contact in contacts if contact.bizintro_user_uuid]
        returnObj['recordsFiltered'] = len(bizintro_contacts)
        returnObj['data'] = bizintro_contacts[start:start+length]
        return returnObj
    

    returnObj['recordsFiltered'] = len(contacts)
    
    if length > 0:
        contacts = contacts[start:start+length]
        
    returnObj['data'] = find_bizintro_users(contacts)
    return returnObj

def get_cached_contacts(profile_id):
    try:
        cache = UPCache.objects.get(user_profile_id=profile_id, type=UPCache.CONTACTS)
        return cache.data
    except:
        user_profile = UserProfile.objects.get(id=profile_id)
        return cache_contacts(user_profile.user)

def get_cached_typeahead_contacts(profile_id):
    try:
        cache = UPCache.objects.get(user_profile_id=profile_id, type=UPCache.TYPEAHEAD)
        return cache.data
    except:
        user_profile = UserProfile.objects.get(id=profile_id)
        return cache_typeahead_contacts(user_profile.user)

def get_cached_help_contacts(profile_id, help_uuid):
    help = Help.objects.get(uuid=help_uuid)
    if help.recipient_user_profile.id == profile_id:
        try:
            cache = UPCache.objects.get(user_profile_id=help.user_profile.id, type=UPCache.HELP)
            return cache.data
        except:
            user_profile = UserProfile.objects.get(id=help.user_profile.id)
            return cache_help_contacts(user_profile.user)
    else:
        return None

def cache_contacts(user):
    contacts = find_bizintro_users(Contact.objects.filter(user_profile=user.profile).order_by('last_name'))
    json_data = {}
    contactObjs = []
    for contact in contacts:
        contactObjs.append({
                    "email": contact.email,
                    "full_name": contact.full_name(),
                    "last_name": contact.last_name,
                    "first_name": contact.first_name,
                    "sources": contact.sources(),
                    "sources_displays": contact.sources_displays(),
                    "occupation": contact.occupation,
                    "address": contact.address,
                    "phone_number": contact.phone_number,
                    "occupations": contact.occupations,
                    "addresses": contact.addresses,
                    "phone_numbers": contact.phone_numbers,
                    "emails": contact.emails,
                    "bizintro_user_uuid": str(contact.bizintro_user_uuid) if contact.bizintro_user_uuid else '',
                    "extra_data": contact.extra_data,
                    "id": contact.id})

    logger.info("Caching %d contacts for user_profile %s" % (len(contactObjs),user.profile.id))
    json_data["recordsTotal"] = len(contactObjs)
    json_data["recordsFiltered"] = len(contactObjs)
    json_data["data"] = contactObjs

    try:
        obj = UPCache.objects.get(user_profile=user.profile, type=UPCache.CONTACTS)
        obj.data = json_data
        obj.save()
    except UPCache.DoesNotExist:
        obj = UPCache(user_profile=user.profile, type=UPCache.CONTACTS, data=json_data)
        obj.save()
    return json_data

def cache_typeahead_contacts(user):
    contacts = find_bizintro_users(Contact.objects.filter(user_profile=user.profile).order_by('last_name'))
    json_data = {}
    contactObjs = []
    for contact in contacts:
        for email in contact.emails:
            contactObjs.append({
                "email": email,
                "full_name": contact.full_name(),
                "sources": contact.sources(),
                "avatar": contact.avatar_link(80),
                "bizintro_user_uuid": str(contact.bizintro_user_uuid) if contact.bizintro_user_uuid else '',
                "id": contact.id
            })
    json_data["status"] = True
    json_data["error"] = None
    json_data["data"] = {}
    json_data["data"]["user"] = contactObjs

    logger.info("Caching %d contacts for user_profile %s" % (len(contactObjs),user.profile.id))

    try:
        obj = UPCache.objects.get(user_profile=user.profile, type=UPCache.TYPEAHEAD)
        obj.data = json_data
        obj.save()
    except UPCache.DoesNotExist:
        obj = UPCache(user_profile=user.profile, type=UPCache.TYPEAHEAD, data=json_data)
        obj.save()

    return json_data

def cache_help_contacts(user):
    contacts = find_bizintro_users(Contact.objects.filter(user_profile=user.profile).order_by('last_name'))
    json_data = {}
    helpContactObjs = []
    for contact in contacts:
        helpContactObjs.append({
                "first_name": contact.first_name,
                "last_initial": contact.last_name[:1],
                "occupation": contact.occupation,
                "avatar_link": contact.avatar_link(80),
                "extra_data": contact.extra_data,
                "id": contact.id
            })
    
    json_data["recordsTotal"] = len(helpContactObjs)
    json_data["recordsFiltered"] = len(helpContactObjs)
    json_data["data"] = helpContactObjs

    try:
        obj = UPCache.objects.get(user_profile=user.profile, type=UPCache.HELP)
        obj.data = json_data
        obj.save()
    except UPCache.DoesNotExist:
        obj = UPCache(user_profile=user.profile, type=UPCache.HELP, data=json_data)
        obj.save()

    return json_data

def quick_cache_contact(user, contact):
    logger.info("in quick_cache_contact")
    contact = find_bizintro_users([contact])[0]
    new_contact={
                "email": contact.email,
                "full_name": contact.full_name(),
                "last_name": contact.last_name,
                "first_name": contact.first_name,
                "sources": contact.sources(),
                "sources_displays": contact.sources_displays(),
                "occupation": contact.occupation,
                "address": contact.address,
                "phone_number": contact.phone_number,
                "occupations": contact.occupations,
                "addresses": contact.addresses,
                "phone_numbers": contact.phone_numbers,
                "emails": contact.emails,
                "bizintro_user_uuid": str(contact.bizintro_user_uuid) if contact.bizintro_user_uuid else '',
                "extra_data": contact.extra_data,
                "id": contact.id
            }
    try:
        obj = UPCache.objects.get(user_profile=user.profile, type=UPCache.CONTACTS)
        new_data = obj.data
        try:
            idx = next(index for (index, d) in enumerate(obj.data["data"]) if d["id"]==contact.id)
            new_data["data"][idx] = new_contact
        except:
            new_data["data"].append(new_contact)
        obj.data = new_data
        obj.save()
    except UPCache.DoesNotExist:
        json_data = {}
        json_data["recordsTotal"] = 1
        json_data["recordsFiltered"] = 1
        json_data["data"] = [new_contact]
        obj = UPCache(user_profile=user.profile, type=UPCache.CONTACTS, data=json_data)
        obj.save()

def quick_cache_typeahead_contact(user, contact):
    contact = find_bizintro_users([contact])[0]
    new_contact={
                "email": contact.email,
                "full_name": contact.full_name(),
                "sources": contact.sources(),
                "avatar": contact.avatar_link(80),
                "bizintro_user_uuid": str(contact.bizintro_user_uuid) if contact.bizintro_user_uuid else '',
                "id": contact.id
            }
    
    try:
        obj = UPCache.objects.get(user_profile=user.profile, type=UPCache.TYPEAHEAD)
        new_data = obj.data
        try:
            idx = next(index for (index, d) in enumerate(obj.data["data"]["user"]) if d["id"]==contact.id)
            new_data["data"]["user"][idx] = new_contact
        except:
            new_data["data"]["user"].append(new_contact)
        obj.data = new_data
        obj.save()
    except UPCache.DoesNotExist:
        json_data = {}
        json_data["status"] = True
        json_data["error"] = None
        json_data["data"] = {}
        json_data["data"]["user"] = [new_contact]
        obj = UPCache(user_profile=user.profile, type=UPCache.TYPEAHEAD, data=json_data)
        obj.save()

def quick_cache_help_contact(user, contact):
    new_contact={
                "first_name": contact.first_name,
                "last_initial": contact.last_name[:1],
                "occupation": contact.occupation,
                "avatar_link": contact.avatar_link(80),
                "extra_data": contact.extra_data,
                "id": contact.id
            }
    
    try:
        obj = UPCache.objects.get(user_profile=user.profile, type=UPCache.HELP)
        new_data = obj.data
        try:
            idx = next(index for (index, d) in enumerate(obj.data["data"]) if d["id"]==contact.id)
            new_data["data"][idx] = new_contact
        except:
            new_data["data"].append(new_contact)
        obj.data = new_data
        obj.save()
    except UPCache.DoesNotExist:
        json_data = {}
        json_data["recordsTotal"] = 1
        json_data["recordsFiltered"] = 1
        json_data["data"] = [new_contact]
        obj = UPCache(user_profile=user.profile, type=UPCache.HELP, data=json_data)
        obj.save()
    
def list_user_contact_sources(user):
    """
    Returns a list of unique contact sources a user currently has.
    """
    sources = list(ImportContact.objects.filter(contact__user_profile=user.profile).values_list('source', flat=True).distinct())
    
    contact_emails = list(Contact.objects.filter(user_profile=user.profile).values_list('email', flat=True).distinct())
    count_bizintro_users = User.objects.filter(email__in=contact_emails).count()
    if count_bizintro_users > 0:
        sources.append('b')
    
    return { 'sources': sources }


def is_valid_phone_number(phone_number):
    return re.search(r'^((\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4})?$', phone_number)


def is_valid_email(email):
    return re.search(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', email)


class OAuth2TokenFromCredentials(OAuth2Token):
    def __init__(self, credentials):
        self.credentials = credentials
        super(OAuth2TokenFromCredentials, self).__init__(None, None, None, None)
        self.UpdateFromCredentials()

    def UpdateFromCredentials(self):
        self.client_id = self.credentials.client_id
        self.client_secret = self.credentials.client_secret
        self.user_agent = self.credentials.user_agent
        self.token_uri = self.credentials.token_uri
        self.access_token = self.credentials.access_token
        self.refresh_token = self.credentials.refresh_token

    def generate_authorize_url(self, *args, **kwargs): raise NotImplementedError
    def get_access_token(self, *args, **kwargs): raise NotImplementedError
    def revoke(self, *args, **kwargs): raise NotImplementedError
    def _extract_tokens(self, *args, **kwargs): raise NotImplementedError

    def _refresh(self, unused_request):
        self.credentials._refresh(httplib2.Http().request)
        self.UpdateFromCredentials()


def create_activity(message, link, user_profile):
    a = Activity(user_profile=user_profile, message=message, link=link)
    a.save()


def base_send_email(subject, plain, html, fullname, emails, replyto, ical=None, bcc_sender=False, v_card=None):
    if settings.DEBUG and settings.DOMAIN != 'appdev.bizintro.com':
        new_emails = []
        reply_parts = replyto.split("@")
        for email in emails:
            new_email = reply_parts[0] + "+" + email.replace("@","_") + "@" + reply_parts[1]
            new_emails.append(new_email)
    else:
        new_emails = emails

    from_email = fullname + ' (Bizintro) <reply@bizintro.com>'
    headers = {'Reply-To': replyto}
    bcc = None
    if bcc_sender:
        bcc = [replyto]

    message = EmailMultiAlternatives(subject=subject,
                                     body=plain,
                                     from_email=from_email,
                                     to=new_emails,
                                     bcc=bcc,
                                     headers=headers)
    message.attach_alternative(populate_html_template(html), "text/html")

    if ical:
        message.attach('Meeting.ics', ical, 'text/calendar')

    if v_card:
        message.attach('%s_%s.vcf' % (v_card.n.value.given, v_card.n.value.family), v_card.serialize(), 'text/x-vcard')

    if message.send(fail_silently=True):
        return True
    else:
        return False


def send_email(subject, body, fullname, emails, replyto, ical=None, bcc_sender=False, v_card=None):
    return base_send_email(subject, body, body, fullname, emails, replyto, ical, bcc_sender, v_card)

def send_email_with_button(subject, body, link, title, fullname, emails, replyto, ical=None, bcc_sender=False, v_card=None):
    html = body + make_html_link(link, title)
    plain = body + '\n\n%s: %s' % (title, link)
    return base_send_email(subject, plain, html, fullname, emails, replyto, ical, bcc_sender, v_card)

def send_email_with_two_buttons(subject, body, link1, title1, link2, title2, class2, fullname, emails, replyto,
                                ical=None, bcc_sender=False):
    html = body + make_html_link(link1, title1) + make_html_link(link2, title2, class2)
    plain = body + '\n\n%s: %s' % (title1, link1) + '\n\n%s: %s' % (title2, link2)
    return base_send_email(subject, plain, html, fullname, emails, replyto, ical, bcc_sender)

    
def make_html_link(link, title, css=''):
    return """
         <table class="btn-primary %s" cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td>
                <a href="%s">%s</a>
              </td>
            </tr>
          </table>
          """ % (css, link, title)

def make_introduction(user_profile, contact_a_id, contact_b_id, recipient_a_email, recipient_b_email, intro_message, intro_secondary_message, potential_uuid, introducer_email, include_vcf_a=False, include_vcf_b=False):
    if contact_a_id and contact_b_id:
        try:
            contact_a = Contact.objects.get(id=contact_a_id)
            contact_b = Contact.objects.get(id=contact_b_id)
        except:
            return {'status': False, 'redirect': HttpResponse(status=404)} 
        
        if not recipient_a_email:
            recipient_a_email = contact_a.email
        if not recipient_b_email:
            recipient_b_email = contact_b.email    
        
        intro_message = intro_message.replace("{contact_a}", contact_a.full_name()).replace("{contact_b}", contact_b.full_name())
        intro_secondary_message = intro_secondary_message.replace("{contact_a}", contact_a.full_name()).replace("{contact_b}", contact_b.full_name())
        introduction = Introduction(user_profile=user_profile,
                                    contact_a=contact_a,
                                    contact_b=contact_b,
                                    recipient_a_email=recipient_a_email,
                                    recipient_b_email=recipient_b_email,
                                    message=intro_message,
                                    secondaryMessage=intro_secondary_message)
        try:
            recipient_user_a = User.objects.get(email=contact_a.email)
            introduction.recipient_user_profile_a = recipient_user_a.profile
        except:
            pass
        try:
            recipient_user_b = User.objects.get(email=contact_b.email)
            introduction.recipient_user_profile_b = recipient_user_b.profile
        except:
            pass
        introduction.save()
        subject = "Introducing %s and %s" % (contact_a.first_name, contact_b.first_name)
        link = 'https://' + settings.FRONTENDDOMAIN + '/appointmentrequest'
        link_a = '%s/%s/%s' % (link, str(contact_a.id), str(introduction.uuid))  # this link should be sent to contact_b
        link_b = '%s/%s/%s' % (link, str(contact_b.id), str(introduction.uuid))  # this link should be sent to contact_a
        body = intro_message
        body2 = intro_secondary_message
        link_title = 'Accept Introduction'
        create_activity(
            ('You made an introduction between %s and %s.' % (contact_a.full_name(), contact_b.full_name())),
            'https://' + settings.FRONTENDDOMAIN + '/introductions', user_profile)
        if introduction.recipient_user_profile_a:
            create_activity(
                ('%s made an introduction between you and %s.' % (user_profile.full_name(), contact_b.full_name())),
                link_b, introduction.recipient_user_profile_a)
        if introduction.recipient_user_profile_b:
            create_activity(
                ('%s made an introduction between you and %s.' % (user_profile.full_name(), contact_a.full_name())),
                link_a, introduction.recipient_user_profile_b)
        if potential_uuid:
            try:
                potential_intro = PotentialIntroduction.objects.get(uuid=potential_uuid)
                potential_intro.introduction = introduction
                potential_intro.status='c'
                potential_intro.save()
            except:
                pass

        if include_vcf_a:
            v_card_a = create_v_card(contact_a_id, recipient_a_email)
        else:
            v_card_a = None

        if include_vcf_b:
            v_card_b = create_v_card(contact_b_id, recipient_b_email)
        else:
            v_card_b = None

        # send individual email to each contact
        if (send_email_with_button(subject, body, link_b, link_title, user_profile.full_name(), [recipient_a_email], introducer_email, v_card=v_card_b) and
            send_email(subject, body2, user_profile.full_name(), [recipient_b_email], introducer_email,v_card=v_card_a)):
            if potential_uuid:
                potential_intro = PotentialIntroduction.objects.get(uuid=potential_uuid)
                return {'status': True, 'redirect': HttpResponseRedirect("/helps/%s" % potential_intro.help.uuid), 'message': 'help_success'}
            else:
                return {'status': True, 'redirect': HttpResponseRedirect("/introductions"), 'message': 'success'}
        else:
            return {'status': False, 'redirect': HttpResponse(status=404)}
    else:
        return {'status': True, 'redirect': HttpResponseRedirect("/make_intro"), 'message': 'failure'}


def share_a_contact(user_profile, recipient_contact_id, recipient_email, shared_contact_id, shared_contact_email, message, sharer_email, include_vcf, share_type):
    if recipient_contact_id and shared_contact_id:
        recipient_contact = get_object_or_404(Contact, pk=recipient_contact_id)
        shared_contact = get_object_or_404(Contact, pk=shared_contact_id)
        message = message.replace("{recipient_contact}", recipient_contact.first_name).replace("{shared_contact}", shared_contact.full_name())
        contact_share = ContactShare(user_profile=user_profile,
                                     recipient_contact=recipient_contact,
                                     recipient_email=recipient_email,
                                     shared_contact=shared_contact,
                                     message=message,
                                     share_type=share_type)
        try:
            recipient_user = User.objects.get(email=recipient_contact.email)
            contact_share.recipient_user_profile = recipient_user.profile
        except:
            pass
        try:
            shared_user = User.objects.get(email=shared_contact.email)
            contact_share.shared_user_profile = shared_user.profile
        except:
            pass
        contact_share.save()
        subject = "Sharing contact %s with %s" % (shared_contact.first_name, recipient_contact.first_name)
        link = 'https://' + settings.FRONTENDDOMAIN + '/accept_contactshare/%s' % str(contact_share.uuid)
        body = message
        link_title = 'Accept Contact Share'
        create_activity(
            ('You shared your contact %s with %s.' % (shared_contact.full_name(), recipient_contact.full_name())),
            link, user_profile)
        if contact_share.recipient_user_profile:
            create_activity(
                ('%s shared his contact %s with you.' % (user_profile.full_name(), shared_contact.full_name())),
                link, contact_share.recipient_user_profile)

        if include_vcf:
            v_card = create_v_card(shared_contact_id, shared_contact_email)
        else:
            v_card = None

        # send individual email to contact
        if send_email_with_button(subject, body, link, link_title, user_profile.full_name(), [recipient_email], sharer_email, v_card=v_card):
            return {'status': True, 'redirect': HttpResponseRedirect("/contact_shares"), 'message': 'success'}
        else:
            return {'status': False, 'redirect': HttpResponse(status=404)}
    else:
        return {'status': True, 'redirect': HttpResponseRedirect("/share_contact"), 'message': 'failure'}


def populate_html_template(body):
    html_template = get_template('email/notification.html')
    context = { 'domain': settings.FRONTENDDOMAIN, 'content': body }
    return html_template.render(context)


def find_bizintro_users(contacts):
    """
    Given a list of Contact objects, determines whether they are Bizintro users.

    Does this optimally by only querying the DB once.
    """
    emails = [contact.email for contact in contacts]
    bizintro_users = dict(User.objects.filter(email__in=emails).values_list('email', 'userprofile__uuid'))
    for contact in contacts:
        contact.bizintro_user_uuid = bizintro_users.get(contact.email, None)
    return contacts


def finish_signup(user, timezone=None):
    user_profile = user.profile
    user_profile.helpTemplate = settings.DEFAULT_HELP_TEMPLATE.replace('{user}', user_profile.full_name())
    user_profile.introTemplate = settings.DEFAULT_INTRO_TEMPLATE.replace('{user}', user_profile.full_name())
    user_profile.introSecondaryTemplate = settings.DEFAULT_INTRO_SECONDARY_TEMPLATE.replace('{user}', user_profile.full_name())
    user_profile.apptTemplate = settings.DEFAULT_APPT_TEMPLATE.replace('{user}', user_profile.full_name())
    user_profile.contactShareTemplate = settings.DEFAULT_CONTACT_SHARE_TEMPLATE.replace('{user}', user_profile.full_name())
    if timezone is not None:
        user_profile.timezone = timezone
    user_profile.save()
    
    connect_objects_to_user(user, user.email)

    # create trial license
    trial_days = 30
    now = datetime.datetime.now()
    trial = License(user_profile=user_profile, type='trial', start_date_time=now, end_date_time=now+datetime.timedelta(days=trial_days))
    trial.save()

    intercom = Client(personal_access_token=settings.INTERCOM_API_KEY)
    try:
        intercom_user = intercom.users.find(email=user.email)
    except:
        intercom_user = intercom.users.create(email=user.email, name=user_profile.full_name())
    
    intercom_user.custom_attributes["bizintro_user"] = True 
    intercom.users.save(intercom_user)

def subscriber_has_bizintro_pro(subscriber):
    return subscriber.profile.has_bizintro_pro

def bizintro_pro_required(function=None):
    """
    Decorator for views that require subscription payment, redirecting to the
    subscribe page if necessary.
    """

    actual_decorator = subscriber_passes_pay_test(
        subscriber_has_bizintro_pro
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


class Storage(DjangoStorage):
    """Overrides the base class just to make it overwrite the credentials by default."""
    def locked_put(self, credentials, overwrite=True):
        return super(Storage, self).locked_put(credentials, overwrite=overwrite)

def merge_manual_edits(user_profile, contact):
    manual_edits = ImportContact.objects.get(contact_id=contact.id, source='m')

    if contact.first_name != manual_edits.first_name():
        contact.first_name = manual_edits.first_name()
    if contact.last_name != manual_edits.last_name():
        contact.last_name = manual_edits.last_name()
    if contact.occupation != manual_edits.occupation():
        contact.occupation = manual_edits.occupation()
    contact.save()
    quick_cache_contact(user_profile.user, contact)
    quick_cache_help_contact(user_profile.user, contact)
    quick_cache_typeahead_contact(user_profile.user, contact)
    return contact

def find_contact_for_combining(imp_contact, contacts, user_profile):
    # Look for a Contact that already exists with this email address.
    logger.info("Processing import_contact #%s" % imp_contact.id)
    contact = []
    for email in imp_contact.data["emails"]:
      contact = []
      for temp_contact in contacts:
        if email in temp_contact["emails"]:
            logger.info("Possibly found contact: %s" % temp_contact["id"])
            contact = Contact.objects.filter(user_profile=user_profile, id=temp_contact["id"])[:1]
            if contact:
                logger.info("Yep, found contact #%s" % contact[0].id)
            return contact
    return contact

def combine_contacts(user_profile_id,source,imp_contact=None,original_contact=None):
    global contact
    contact = []
    user_profile = UserProfile.objects.get(id=user_profile_id)

    if imp_contact:
        imp_contacts = [imp_contact]
        only_one = True
    else:
        imp_contacts = ImportContact.objects.filter(user_profile=user_profile, source=source)
        only_one = False

    logger.info("Processing %d contacts (#%s) for user_profile %s and source %s" % (len(imp_contacts), imp_contacts[0].id, user_profile_id,source))

    contacts = Contact.objects.filter(user_profile=user_profile).values()

    for imp_contact in imp_contacts:
        if not original_contact:
            contact = find_contact_for_combining(imp_contact, contacts, user_profile)
        else:
            contact = [original_contact]
                        
        # If it doesn't exist, create it and link it.
        if len(contact)==0:
          contact = Contact(user_profile=user_profile)
          contact.save()
        else:
          contact = contact[0]
        # Link it, update data and see if the name conflicts.
        imp_contact.contact = contact
        imp_contact.save()

        if contact.first_name == "":
          contact.first_name = imp_contact.first_name()
        if contact.last_name == "":
          contact.last_name = imp_contact.last_name()
        if contact.occupation == "":
          contact.occupation = imp_contact.occupation()
        if contact.email == "":
          contact.email = imp_contact.first_email()
        if contact.address == "":
          contact.address = imp_contact.first_address()
        if contact.phone_number == "":
          contact.phone_number = imp_contact.first_phone_number()

        occupation = imp_contact.occupation()
        new_emails=list(set(contact.emails+imp_contact.emails()))
        new_addresses=list(set(contact.addresses+imp_contact.addresses()))
        new_phone_numbers=list(set(contact.phone_numbers+imp_contact.phone_numbers()))
        if occupation:
          new_occupations=list(set(contact.occupations+[occupation]))
        else:
          new_occupations=list(set(contact.occupations))

        contact.occupations = new_occupations
        contact.emails = new_emails
        contact.addresses = new_addresses
        contact.phone_numbers = new_phone_numbers

        contact.save()

    if only_one:
        quick_cache_contact(user_profile.user, contact)
        quick_cache_help_contact(user_profile.user, contact)
        quick_cache_typeahead_contact(user_profile.user, contact)
    else:
        cache_contacts(user_profile.user)
        cache_help_contacts(user_profile.user)
        cache_typeahead_contacts(user_profile.user)
    logger.info("DONE processing %d contacts for user_profile %s and source %s" % (len(imp_contacts),user_profile_id,source))


def create_v_card(contact_id, email):
    contact = get_object_or_404(Contact, pk=contact_id)

    v_card = vobject.vCard()
    o = v_card.add('fn')
    o.value = contact.full_name()

    o = v_card.add('n')
    o.value = vobject.vcard.Name(family=contact.last_name, given=contact.first_name)

    o = v_card.add('email')
    o.value = email

    o = v_card.add('tel')
    o.value = contact.phone_number

    occupation = contact.occupation.split(' at ')

    o = v_card.add('title')
    o.value = occupation[0]

    o = v_card.add('org')
    o.value = [occupation[1] if len(occupation) > 1 else '']

    try:
        o = v_card.add('adr')
        address = dict(usaddress.tag(contact.address)[0])
        street = [address.get('AddressNumber', ''),
                  address.get('StreetName', ''),
                  address.get('StreetNamePostType', ''),
                  address.get('StreetNamePreDirectional', ''),
                  address.get('OccupancyType', ''),
                  address.get('OccupancyIdentifier', ''),
                  address.get('USPSBoxType', ''),
                  address.get('USPSBoxID', '')]
        o.value = vobject.vcard.Address(street=' '.join([x for x in street if x]),
                                        city=address.get('PlaceName', ''),
                                        region=address.get('StateName', ''),
                                        code=address.get('ZipCode', ''),
                                        country=address.get('CountryName', ''))
    except usaddress.RepeatedLabelError:
        pass

    f = urllib.urlopen(contact.avatar_link())
    photo = f.read()
    f.close()
    o = v_card.add('photo')
    o.type_param = 'jpeg'
    o.encoding_param = 'b'
    o.value = photo

    return v_card

def combine_contacts_async(user_profile_id,source="all"):
    from bizintro.tasks import CombineTask
    t = CombineTask.delay(user_profile_id, source)
    job = AsyncJob(user_profile_id=user_profile_id,type=AsyncJob.COMBINE_CONTACTS,status=AsyncJob.PROGRESS,message="Merging and enhancing contacts...",task_id=t.id)
    job.save()

def sync_contacts_async(user_profile_id,source="all"):
    from bizintro.tasks import SyncContactsTask
    t = SyncContactsTask.delay(user_profile_id, source)
    job = AsyncJob(user_profile_id=user_profile_id,type=AsyncJob.SYNC_CONTACTS,status=AsyncJob.PROGRESS,message="Syncing contacts...",task_id=t.id)
    job.save()

def sync_contacts(user_profile_id,source):
    user = User.objects.get(userprofile__id=user_profile_id)
    
    any_errors = False

    if source == "all":
        for social_auth in user.social_auth.all():
            if not sync_contacts_for_real(user,social_auth):
                any_errors=True
    else:
        user = User.objects.get(userprofile__id=user_profile_id)
        social_auth = UserSocialAuth.objects.get(id=source)
        sync_contacts_for_real(user,social_auth)
    return any_errors

def sync_contacts_for_real(user,social_auth):
    try:
        if social_auth.provider == Provider.Google:
            from bizintro.google import GoogleAPI
            api = GoogleAPI(user, social_auth)
            api.get_contacts(async=False)
        elif social_auth.provider == Provider.Salesforce:
            from bizintro.salesforce import SalesforceAPI
            api = SalesforceAPI(user, social_auth)
            api.get_contacts(async=False)
        elif social_auth.provider == Provider.Yahoo:
            from bizintro.yahoo import YahooAPI
            api = YahooAPI(user, social_auth)
            api.get_contacts(async=False)
        elif social_auth.provider == Provider.Microsoft:
            from bizintro.microsoft import MicrosoftAPI
            api = MicrosoftAPI(user, social_auth)
            api.get_contacts(async=False)
        elif social_auth.provider == Provider.Exchange:
            from bizintro.exchange import ExchangeAPI
            api = ExchangeAPI(user, social_auth)
            api.get_contacts(async=False)
        return True
    except:
        return False


def connect_objects_to_user(user,email):
    helps = Help.objects.all()
    for help in helps:
        if help.contact.email == email or help.recipient_email == email:
            help.recipient_user_profile = user.profile
            help.save()
            link = 'https://' + settings.FRONTENDDOMAIN + '/helps/' + str(help.uuid)
            create_activity("%s has offered to help you." % help.user_profile.full_name(), link, user.profile)
            create_contact(user.profile, help.user_profile.user.email, help.user_profile.user.first_name, help.user_profile.user.last_name, help.user_profile.title, help.user_profile.company, help.user_profile.address, help.user_profile.phone_number)
    intros = Introduction.objects.all()
    for intro in intros:
        link = 'https://' + settings.FRONTENDDOMAIN + '/accept_intro/' + str(intro.uuid)
        if intro.contact_a.email == email or intro.recipient_a_email == email:
            intro.recipient_user_profile_a = user.profile
            create_activity(('%s made an introduction between you and %s.' % (
                intro.user_profile.full_name(), intro.contact_b.full_name())), link, user.profile)
            intro.save()
            create_contact(user.profile, intro.user_profile.user.email, intro.user_profile.user.first_name, intro.user_profile.user.last_name, intro.user_profile.title, intro.user_profile.company, intro.user_profile.address, intro.user_profile.phone_number)
        if intro.contact_b.email == email or intro.recipient_b_email == email:
            intro.recipient_user_profile_b = user.profile
            create_activity(('%s made an introduction between you and %s.' % (
                intro.user_profile.full_name(), intro.contact_a.full_name())), link, user.profile)
            intro.save()
            create_contact(user.profile, intro.user_profile.user.email, intro.user_profile.user.first_name, intro.user_profile.user.last_name, intro.user_profile.title, intro.user_profile.company, intro.user_profile.address, intro.user_profile.phone_number)
    appt_requests = ApptRequest.objects.all()
    for appt_request in appt_requests:
        if appt_request.contact.email == email or appt_request.recipient_email == email:
            appt_request.recipient_user_profile = user.profile
            appt_request.save()
            link = 'https://' + settings.FRONTENDDOMAIN + '/appointments/requests/' + str(appt_request.uuid)
            create_activity('%s requested an appointment with you.' % appt_request.user_profile.full_name(), link,
                            user.profile)
            create_contact(user.profile, appt_request.user_profile.user.email, appt_request.user_profile.user.first_name, appt_request.user_profile.user.last_name, appt_request.user_profile.title, appt_request.user_profile.company, appt_request.user_profile.address, appt_request.user_profile.phone_number)
    contact_shares = ContactShare.objects.all()
    for contact_share in contact_shares:
        link = 'https://' + settings.FRONTENDDOMAIN + '/contact_shares/' + str(contact_share.uuid)
        if contact_share.recipient_contact.email == email or contact_share.recipient_email == email:
            contact_share.recipient_user_profile = user.profile
            create_activity(('%s shared his contact %s with you.' % (
                contact_share.user_profile.full_name(), contact_share.shared_contact.full_name())), link, user.profile)
            contact_share.save()
            create_contact(user.profile, contact_share.user_profile.user.email, contact_share.user_profile.user.first_name, contact_share.user_profile.user.last_name, contact_share.user_profile.title, contact_share.user_profile.company, contact_share.user_profile.address, contact_share.user_profile.phone_number)

def create_contact(user_profile, email, first_name, last_name, title, company, address, phone_number):
    try:
        contact = Contact.objects.filter(user_profile=user_profile, email=email)[:1]
        contact_id = contact[0].id
    except:
        import_contact = ImportContact(user_profile=user_profile, source='m')

        import_contact.data = {
          'first_name': first_name,
          'last_name': last_name,
          'title': title,
          'company': company,
          'emails': [email],
          'phone_numbers': [phone_number],
          'addresses': [address],
        }
        import_contact.save()

        combine_contacts(user_profile.id, 'm', imp_contact=import_contact)

        contact = Contact.objects.filter(user_profile=user_profile, email=email)[:1]
        contact_id = contact[0].id

    return contact_id

def fill_default_template(template, user):
    return template.replace("\n", "\\n").replace("{user}", user.profile.full_name())

def nudge(the_obj, the_type, recipient_email):
    if the_type == 'introduction':
        message = "You have an Introduction still waiting for you to accept!"
        link = 'https://' + settings.FRONTENDDOMAIN + '/introductions/%s' % str(the_obj.uuid)
    elif the_type == 'appt_request': 
        message = "You have an Appointment Request still waiting for you to accept!" 
        link = 'https://' + settings.FRONTENDDOMAIN + '/appointments/%s' % str(the_obj.uuid)
    elif the_type == 'help': 
        message = "You have an Offer of Help still waiting for you to accept!"
        link = 'https://' + settings.FRONTENDDOMAIN + '/helps/%s' % str(the_obj.uuid)

    nudge = Nudge(content_object=the_obj)
    nudge.save()
    
    send_email_with_button("Nudge from Bizintro", message, link, "Accept", the_obj.user_profile.full_name(), [recipient_email], the_obj.user_profile.user.email)
