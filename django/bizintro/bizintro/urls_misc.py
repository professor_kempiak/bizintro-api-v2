from django.conf.urls import url
from bizintro.apis import misc

urlpatterns = (
	url(r'^jobstatus$', misc.JobStatusView.as_view(), name='jobstatus'),
)
