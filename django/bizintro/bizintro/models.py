import hashlib
import logging
import uuid
from datetime import datetime, timedelta
import json

import httplib2
from dirtyfields import DirtyFieldsMixin
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.functional import cached_property
from djstripe.utils import subscriber_has_active_subscription
from oauth2client.django_orm import CredentialsField
#from social_core.apps.django_app.default.models import UserSocialAuth
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from social_django.models import UserSocialAuth
from jsonfield import JSONField
from django.contrib.postgres.fields import ArrayField
from model_utils import FieldTracker
import requests

from encryption import EncryptedCharField

logger = logging.getLogger(__name__)

HELP_STATUS_CHOICES = (
    ('o', 'Open'),
    ('a', 'Accepted'),
    ('c', 'Complete'),
    ('d', 'Closed by recipient'),
    ('f', 'Closed by owner'),
)

POTENTIAL_INTRODUCTION_STATUS_CHOICES = (
    ('o', 'Open'),
    ('d', 'Declined'),
    ('c', 'Complete'),
)

APPT_REQUEST_STATUS_CHOICES = (
    ('o', 'Open'),
    ('c', 'Complete'),
    ('d', 'Closed by recipient'),
    ('f', 'Closed by owner'),
    ('t', 'Closed to find new times'),
)

INTRODUCTION_STATUS_CHOICES = (
    ('o', 'Open'),
    ('c', 'Complete'),
    ('d', 'Closed by recipient A'),
    ('e', 'Closed by recipient B'),
    ('f', 'Closed by owner'),
)

CONTACT_SHARE_STATUS_CHOICES = (
    ('o', 'Open'),
    ('c', 'Complete'),
    ('d', 'Closed by recipient'),
    ('f', 'Closed by owner'),
)

APPT_TYPE_CHOICES = (
    ('phone', 'Phone Meeting'),
    ('office', 'My Office'),
    ('your_office', 'Your Office'),
    ('online', 'Online Meeting'),
    ('other', 'Other'),
)

APPT_TYPE_CHOICES_DICT = dict(APPT_TYPE_CHOICES)

APPT_TYPE_ICON_CLASS_DICT = {
    'phone': 'fa-phone',
    'office': 'fa-building',
    'your_office': 'fa-building',
    'online': 'fa-globe',
    'other': 'fa-question',
}

LICENSE_TYPE_CHOICES = (
    ('manual', 'Manual'),
    ('trial', 'Trial'),
)


class UserProfile(models.Model):
    user = models.OneToOneField(User, unique=True)
    phone_number = models.CharField(max_length=200, default="")
    address = models.CharField(max_length=200, default="")
    timezone = models.CharField(max_length=50, default='America/Chicago')
    helpTemplate = models.TextField(max_length=1000, default="")
    introTemplate = models.TextField(max_length=1000, default="")
    introSecondaryTemplate = models.TextField(max_length=1000, default="")
    apptTemplate = models.TextField(max_length=1000, default="")
    contactShareTemplate = models.TextField(max_length=1000, default="")
    standing_meeting_times = models.TextField(max_length=1000, default="")
    linkedin_url = models.URLField(default="")
    summary = models.TextField(max_length=1000, default="")
    title = models.CharField(max_length=200, default="")
    company = models.CharField(max_length=200, default="")
    signature = models.TextField(max_length=1000, default="")
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def __unicode__(self):
        return self.full_name()

    def full_name(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)

    def avatar_link(self, s=80):
        return 'https://www.gravatar.com/avatar/%s?s=%d&d=mm' % (hashlib.md5(self.user.email.lower()).hexdigest(), s)

    def activities(self):
        now = datetime.now()
        one_week_ago = now - timedelta(days=7)
        activities = Activity.objects.filter(
            Q(user_profile=self) & Q(created_date_time__range=(one_week_ago, now)))
        newest_activities = sorted(activities, key=lambda activity: activity.created_date_time, reverse=True)[:5]
        return newest_activities

    def contacts(self):
        contacts = Contact.objects.filter(Q(user_profile=self))
        return contacts

    def helps(self):
        helps = Help.objects.filter(Q(user_profile=self))
        return helps

    def introductions(self):
        intros = Introduction.objects.filter(Q(user_profile=self))
        return intros

    def appts(self):
        appts = ApptRequest.objects.filter(Q(user_profile=self))
        return appts

    def contact_shares(self):
        contact_shares = ContactShare.objects.filter(Q(user_profile=self))
        return contact_shares

    def exchange_accounts(self):
        exchange_accounts = ExchangeAccount.objects.filter(Q(user_profile=self))
        return exchange_accounts

    def helps_r(self):
        helps = Help.objects.filter(Q(recipient_user_profile=self))
        return helps

    def introductions_r(self):
        intros = Introduction.objects.filter(Q(recipient_user_profile_a=self) | Q(recipient_user_profile_b=self))
        return intros

    def appts_r(self):
        appts = ApptRequest.objects.filter(Q(recipient_user_profile=self))
        return appts

    def contact_shares_r(self):
        contact_shares = ContactShare.objects.filter(Q(recipient_user_profile=self))
        return contact_shares

    def current_license(self):
        return License.get_current_license(self)

    def licenses(self):
        return License.objects.filter(user_profile=self)

    @cached_property
    def is_not_pro_or_trial(self):
        license = self.current_license()

        if license and license.valid():
            return False
        else:
            return True

    @cached_property
    def has_bizintro_pro(self):
        license = self.current_license()

        if license and license.valid() and license.type != 'trial':
            return True

        """Checks if a user has an active subscription."""
        return subscriber_has_active_subscription(self.user)

    @cached_property
    def has_bizintro_trial(self):
        """First check if user has a real stripe subscription, in which case we're no longer on a trial license"""
        if subscriber_has_active_subscription(self.user):
            return False

        """If user doesn't have a stripe subscription, we'll check if their current license is a trial"""
        license = self.current_license()

        if license and license.valid() and license.type == 'trial':
            return True

        return False

    def get_calendar_account(self):
        try:
            return self.calendar_account
        except CalendarAccount.DoesNotExist:
            return None

    def external_associated(self):
        return self.user.social_auth.exists()

    def primary_email_verified(self):
        emails = self.user.emailaddress_set.filter(is_primary=True)

        if len(emails) > 0 and emails[0].is_verified():
            return True 

        return False

    def is_onboarded(self):
        if self.primary_email_verified() and self.external_associated():
            return True
        else:
            return False

    def profile_complete(self):
        if self.phone_number and self.address and self.summary and self.title and self.company:
            return True
        return False

    def sync_in_progress(self):
        jobs = AsyncJob.objects.filter(user_profile=self, status=AsyncJob.PROGRESS)
        if jobs and len(jobs) > 0:
            return True
        else:
            return False


User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


class Tour(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    tour_key = models.CharField(max_length=200, default="")
    updated = models.DateTimeField(auto_now=True)

class Contact(DirtyFieldsMixin, models.Model):
    user_profile = models.ForeignKey(UserProfile, null=True)
    first_name = models.CharField(max_length=200, default="")
    last_name = models.CharField(max_length=200, default="")
    address = models.CharField(max_length=200, default="")
    email = models.CharField(max_length=200, default="")
    phone_number = models.CharField(max_length=200, default="")
    occupation = models.CharField(max_length=400, default="")
    addresses = ArrayField(models.CharField(max_length=200, default=""), default=[])
    emails = ArrayField(models.CharField(max_length=200, default=""), default=[])
    phone_numbers = ArrayField(models.CharField(max_length=200, default=""), default=[])
    occupations = ArrayField(models.CharField(max_length=400, default=""), default=[])
    bio = models.CharField(max_length=5000, default="")

    def as_json(self):
        return dict(
            first_name=self.first_name, last_name=self.last_name,
            email=self.email,emails=self.emails, address=self.address,avatar_link=self.avatar_link(), 
            addresses=self.addresses, phone_number=self.phone_number,phone_numbers=self.phone_numbers,
            occupation=self.occupation,occupations=self.occupations,bio=self.bio,id=self.id)

    def __unicode__(self):
        return self.full_name()

    def full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def sources(self):
        imp_contacts = self.import_contacts()
        sources = []
        for ic in imp_contacts:
            sources.append(ic.source)

        return list(set(sources))

    def sources_displays(self):
        imp_contacts = self.import_contacts()
        sources = []
        for ic in imp_contacts:
            sources.append(ic.get_source_display())

        return sources

    def avatar_link(self, s=80):
        try:
            return 'https://www.gravatar.com/avatar/%s?s=%d&d=mm' % (
            hashlib.md5(self.email.lower().encode('utf-8')).hexdigest(), s)
        except:
            return ''

    def import_contacts(self):
        import_contacts = ImportContact.objects.filter(Q(contact=self))
        return import_contacts

    @cached_property
    def extra_data(self):
        try:
            fcdata = FullContactData.objects.get(email=self.email).usefuldata
            if not fcdata:
                return {"avatar":""}
            else: 
                return fcdata
        except:
            return {"avatar":""}

    def save(self, *args, **kwargs):
        for email in self.emails:
            FullContactData.objects.get_or_create(email=email)

        super(Contact, self).save(*args, **kwargs)
 
    def delete(self):
        self.user_profile_id = None
        self.save() 

class ImportContact(DirtyFieldsMixin, models.Model):
    GOOGLE = 'g'
    YAHOO = 'y'
    MICROSOFT = 'a'
    LINKEDIN = 'l'
    SALESFORCE = 's'
    OUTLOOK = 'o'
    OTHER = 'e'
    MANUAL = 'm'
    EXCHANGE = 'x'

    CONTACT_SOURCE_CHOICES = (
        (GOOGLE, 'Google Contacts'),
        (YAHOO, 'Yahoo'),
        (LINKEDIN, 'LinkedIn'),
        (SALESFORCE, 'SalesForce'),
        (OUTLOOK, 'Outlook'),
        (EXCHANGE, 'Exchange'),
        (MICROSOFT, 'Office365'),
        (OTHER, 'Other'),
        (MANUAL, 'Manually Entered'),
    )

    contact = models.ForeignKey(Contact, null=True)
    user_profile = models.ForeignKey(UserProfile, null=True)
    source = models.CharField(max_length=1, choices=CONTACT_SOURCE_CHOICES, default=MANUAL)
    auth_source = models.ForeignKey(UserSocialAuth, null=True)
    external_id = models.CharField(max_length=200, null=True)
    last_imported = models.DateTimeField(auto_now=True)
    data = JSONField(null=True)

    def title(self):
        return self.data.get("title", "")

    def company(self):
        return self.data.get("company", "")

    def first_name(self):
        return self.data.get("first_name", "")

    def last_name(self):
        return self.data.get("last_name", "")

    def occupation(self):
        if self.title() != "" and self.company() != "" and self.title() is not None and self.company() is not None:
            occupation = " %s at %s" % (self.title(), self.company())
        elif self.title() != "":
            occupation = self.title()
        elif self.company() != "":
            occupation = self.company()
        else:
            occupation = ""

        return occupation

    def first_email(self):
        emails = self.emails()
        if len(emails) > 0:
            return emails[0]
        else:
            return ""

    def emails(self):
        return self.data.get("emails", [])

    def first_address(self):
        addresses = self.addresses()
        if len(addresses) > 0:
            return addresses[0]
        else:
            return ""

    def addresses(self):
        return self.data.get("addresses", [])

    def first_phone_number(self):
        phone_numbers = self.phone_numbers()
        if len(phone_numbers) > 0:
            return phone_numbers[0]
        else:
            return ""

    def phone_numbers(self):
        return self.data.get("phone_numbers", [])


class CalendarAccount(models.Model):
    user_profile = models.OneToOneField(UserProfile, related_name="calendar_account")
    social_auth = models.OneToOneField(UserSocialAuth)
    _calendar_id = models.CharField(max_length=200)

    @property
    def calendar_id(self):
        """
		If the user has not set a specific calendar ID and they are using Google,
		we should just be using "primary" as the default.
		"""
        from bizintro.helpers import Provider
        calendar_id = self._calendar_id
        if not self._calendar_id and self.social_auth.provider == Provider.Google:
            calendar_id = 'primary'
        return calendar_id

    @calendar_id.setter
    def calendar_id(self, value):
        self._calendar_id = value


class ExchangeAccount(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    username = models.CharField(max_length=255)
    password = EncryptedCharField(max_length=255)
    server = models.CharField(max_length=500)
    disabled = models.BooleanField(default=False)
    failed_attempts = models.IntegerField(default=0)

class Nudge(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    date_created = models.DateTimeField(auto_now_add=True)

class Introduction(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    contact_a = models.ForeignKey(Contact, related_name='+')
    contact_b = models.ForeignKey(Contact, related_name='+')
    recipient_a_email = models.CharField(max_length=200, default="")
    recipient_b_email = models.CharField(max_length=200, default="")
    recipient_user_profile_a = models.ForeignKey(UserProfile, blank=True, null=True, related_name='+')
    recipient_user_profile_b = models.ForeignKey(UserProfile, blank=True, null=True, related_name='+')
    message = models.TextField(max_length=1000, default="")
    secondaryMessage = models.TextField(max_length=1000, default="")
    created_date_time = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)
    status = models.CharField(max_length=1,
                              choices=INTRODUCTION_STATUS_CHOICES,
                              default='o')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    nudges = GenericRelation(Nudge)

    def __unicode__(self):
        if self.contact_a and self.contact_b:
            return self.contact_a.full_name() + " - " + self.contact_b.full_name()
        elif self.recipient_user_profile_a and self.recipient_user_profile_b:
            return self.recipient_user_profile_a.full_name() + " - " + self.recipient_user_profile_b.full_name()
        elif self.contact_a:
            return self.contact_a.full_name()
        elif self.contact_b:
            return self.contact_b.full_name()

    def is_from_help(self):
        potential_intro = PotentialIntroduction.objects.get(introduction=self.id)
        if potential_intro:
            return True
        else:
            return False

    def save(self, *args, **kwargs):
        if self.status in 'cdef':
            self.archived = True
        else:
            self.archived = False
        users = UserProfile.objects.filter(user__email=self.recipient_a_email)
        if users:
            self.recipient_user_profile_a = users[0]
        users = UserProfile.objects.filter(user__email=self.recipient_b_email)
        if users:
            self.recipient_user_profile_b = users[0]
        super(Introduction, self).save(*args, **kwargs)


class Help(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    contact = models.ForeignKey(Contact)
    recipient_email = models.CharField(max_length=200, default="")
    recipient_user_profile = models.ForeignKey(UserProfile, blank=True, null=True, related_name='+')
    message = models.TextField(max_length=1000, default="")
    created_date_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=1,
                              choices=HELP_STATUS_CHOICES,
                              default='o')
    archived = models.BooleanField(default=False)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def __unicode__(self):
        if self.contact:
            return self.user_profile.full_name() + " - " + self.contact.full_name()
        else:
            return self.user_profile.full_name() + " - " + self.recipient_user_profile.full_name()

    def save(self, *args, **kwargs):
        if self.status in 'cdf':
            self.archived = True
        else:
            self.archived = False
        users = UserProfile.objects.filter(user__email=self.recipient_email)
        if users:
            self.recipient_user_profile = users[0]
        super(Help, self).save(*args, **kwargs)


class ApptRequest(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    contact = models.ForeignKey(Contact)
    recipient_email = models.CharField(max_length=200, default="")
    recipient_user_profile = models.ForeignKey(UserProfile, blank=True, null=True, related_name='+')
    introduction = models.ForeignKey(Introduction, blank=True, null=True)
    title = models.CharField(max_length=200, default="")
    message = models.TextField(max_length=1000, default="")
    created_date_time = models.DateTimeField(auto_now_add=True)
    length = models.IntegerField()
    status = models.CharField(max_length=1,
                              choices=APPT_REQUEST_STATUS_CHOICES,
                              default='o')
    appt_type = models.CharField(max_length=20, null=True, choices=APPT_TYPE_CHOICES)
    location = models.CharField(max_length=200, default="")
    archived = models.BooleanField(default=False)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def pretty_appt_type(self):
        return APPT_TYPE_CHOICES_DICT.get(self.appt_type, APPT_TYPE_CHOICES_DICT.get('other'))

    def appt_type_icon_class(self):
        """
		Returns the appropriate HTML icon class type.
		"""
        return APPT_TYPE_ICON_CLASS_DICT.get(self.appt_type, APPT_TYPE_CHOICES_DICT.get('other'))

    def get_appt(self):
        return Appt.objects.get(appt_request__uuid=self.uuid)

    def is_from_intro(self):
        if self.introduction:
            return True
        else:
            return False
            
    def save(self, *args, **kwargs):
        if self.status in 'cdft':
            self.archived = True
        else:
            self.archived = False
        users = UserProfile.objects.filter(user__email=self.recipient_email)
        if users:
            self.recipient_user_profile = users[0]
        super(ApptRequest, self).save(*args, **kwargs)


class ContactShare(models.Model):
    ALL = 'a'
    PRIMARY = 'p'

    SHARE_CHOICES = (
        (ALL, 'All Contact Data'),
        (PRIMARY, 'Primary Data Only'),
    )
    user_profile = models.ForeignKey(UserProfile)
    recipient_contact = models.ForeignKey(Contact, related_name='+')
    recipient_email = models.CharField(max_length=200, default="")
    shared_contact = models.ForeignKey(Contact, related_name='+')
    recipient_user_profile = models.ForeignKey(UserProfile, blank=True, null=True, related_name='+')
    shared_user_profile = models.ForeignKey(UserProfile, blank=True, null=True, related_name='+')
    message = models.TextField(max_length=1000, default="")
    created_date_time = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)
    share_type = models.CharField(max_length=1, choices=SHARE_CHOICES, default=ALL)
    created_contact = models.ForeignKey(Contact, related_name='+', null=True)

    status = models.CharField(max_length=1,
                              choices=CONTACT_SHARE_STATUS_CHOICES,
                              default='o')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def __unicode__(self):
        if self.recipient_contact and self.shared_contact:
            return self.recipient_contact.full_name() + " - " + self.shared_contact.full_name()
        elif self.recipient_user_profile and self.shared_user_profile:
            return self.recipient_user_profile.full_name() + " - " + self.shared_user_profile.full_name()
        elif self.recipient_contact:
            return self.recipient_contact.full_name()
        elif self.shared_contact:
            return self.shared_contact.full_name()

    def save(self, *args, **kwargs):
        if self.status in 'cdf':
            self.archived = True
        else:
            self.archived = False
        users = UserProfile.objects.filter(user__email=self.recipient_email)
        if users:
            self.recipient_user_profile = users[0]
        super(ContactShare, self).save(*args, **kwargs)


class TimeSlot(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    appt_request = models.ForeignKey(ApptRequest)
    start_date_time = models.DateTimeField()
    end_date_time = models.DateTimeField()


class Appt(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    recipient_user_profile = models.ForeignKey(UserProfile, blank=True, null=True, related_name='+')
    appt_request = models.ForeignKey(ApptRequest)
    title = models.CharField(max_length=200, default="")
    comments = models.TextField(max_length=1000, default="")
    created_date_time = models.DateTimeField(auto_now_add=True)
    start_date_time = models.DateTimeField()
    end_date_time = models.DateTimeField()
    appt_type = models.CharField(max_length=20, null=True, choices=APPT_TYPE_CHOICES)
    location = models.CharField(max_length=200, default="")
    archived = models.BooleanField(default=False)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def pretty_appt_type(self):
        return APPT_TYPE_CHOICES_DICT.get(self.appt_type, APPT_TYPE_CHOICES_DICT.get('other'))

    def appt_type_icon_class(self):
        """
		Returns the appropriate HTML icon class type.
		"""
        return APPT_TYPE_ICON_CLASS_DICT.get(self.appt_type, APPT_TYPE_CHOICES_DICT.get('other'))


class PotentialIntroduction(models.Model):
    help = models.ForeignKey(Help)
    contact = models.ForeignKey(Contact)
    introduction = models.ForeignKey(Introduction, blank=True, null=True)
    notes = models.CharField(max_length=1000, null=True, default=None)
    status = models.CharField(max_length=1, choices=POTENTIAL_INTRODUCTION_STATUS_CHOICES, default='o')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)


class CredentialsModel(models.Model):
    auth = models.OneToOneField(UserSocialAuth)
    credential = CredentialsField()

    @classmethod
    def get_credentials_from_auth(cls, auth):
        from bizintro.helpers import Storage  # import locally to get around circular import
        storage = Storage(cls, 'auth', auth, 'credential')
        credential = storage.get()
        if credential is None:
            logger.info('No credential found for user %d and social auth %d', auth.user_id, auth.id)
        elif credential.invalid:
            logger.info('Invalid credential found for user %d and social auth %d, refreshing', auth.user_id, auth.id)
            try:
                http = httplib2.Http()
                credential.refresh(http)
                if credential.invalid:
                    logger.info('Refreshed credential but still invalid for user %d and social auth %d',
                                auth.user_id, auth.id)
                    credential = None
            except Exception:
                logger.exception('Failure refreshing credential for user %d and social auth %d', auth.user_id, auth.id)
                credential = None
        return credential


class Activity(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    created_date_time = models.DateTimeField(auto_now_add=True)
    message = models.CharField(max_length=300, default="")
    link = models.CharField(max_length=300, default="")
    archived = models.BooleanField(default=False)


class License(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    type = models.CharField(max_length=20, null=True, choices=LICENSE_TYPE_CHOICES)
    notes = models.TextField(max_length=1000, default="")
    created_date_time = models.DateTimeField(auto_now_add=True)
    start_date_time = models.DateTimeField()
    end_date_time = models.DateTimeField()
    revoked = models.BooleanField(default=False)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def valid(self):
        if self.revoked:
            return False

        return self.end_date_time > timezone.now() and self.start_date_time < timezone.now()

    @classmethod
    def get_current_license(cls, profile):
        try:
            return License.objects.filter(user_profile=profile, start_date_time__lte=timezone.now(),
                                          end_date_time__gt=timezone.now(), revoked=False).first()
        except License.DoesNotExist:
            return None


class AsyncJob(models.Model):
    PROGRESS = 'p'
    FAILED = 'f'
    SUCCESS = 's'
    COMBINE_CONTACTS = 'c'
    SYNC_CONTACTS = 's'

    JOB_STATUS_CHOICES = (
        (PROGRESS, 'PROGRESS'),
        (FAILED, 'FAILED'),
        (SUCCESS, 'SUCCESS'),
    )

    JOB_TYPE_CHOICES = (
        (COMBINE_CONTACTS, 'combine_contacts'),
        (SYNC_CONTACTS, 'sync_contacts'),
    )

    user_profile = models.ForeignKey(UserProfile)
    type = models.CharField(max_length=1, choices=JOB_TYPE_CHOICES)
    acknowledged = models.BooleanField(default=False)
    start_date_time = models.DateTimeField(auto_now_add=True)
    end_date_time = models.DateTimeField(null=True)
    status = models.CharField(max_length=1, choices=JOB_STATUS_CHOICES)
    message = models.CharField(max_length=200, default="")
    task_id = models.CharField(max_length=200, default="")

class UPCache(models.Model):
    TYPEAHEAD = 't'
    HELP = 'h'
    CONTACTS = 'c'
    
    CACHE_CHOICES = (
        (TYPEAHEAD, 'TYPEAHEAD'),
        (HELP, 'HELP'),
        (CONTACTS, 'CONTACTS'),
    )
    user_profile = models.ForeignKey(UserProfile, related_name="cache")
    type = models.CharField(max_length=1, choices=CACHE_CHOICES)
    data = JSONField(null=True)
    updated = models.DateTimeField(auto_now=True)

class FullContactData(models.Model):
    email = models.CharField(max_length=255, unique=True)
    data = JSONField(null=True)
    usefuldata = JSONField(null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    last_checked = models.DateTimeField(null=True)
    requestId = models.CharField(max_length=200, null=True)
    email_tracker = FieldTracker(fields=['email'])

    def save(self, *args, **kwargs):
        is_new = self.pk is None
        super(FullContactData, self).save(*args, **kwargs)
        if is_new:
            from bizintro.tasks import FullContactTask
            t = FullContactTask.delay(self.id)
        
class Template(models.Model):
    INTRODUCTION = 'i'
    SCHEDULING = 's' 
    TEMPLATE_CHOICES = (
        (INTRODUCTION, 'introduction'),
        (SCHEDULING, 'scheduling'),
    )

    user = models.ForeignKey(UserProfile)
    name = models.CharField(max_length=250, default="")
    description = models.CharField(max_length=250, default="")
    contents = models.TextField(max_length=1000, default="")
    category = models.CharField(max_length=1, choices=TEMPLATE_CHOICES)
    date_created = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(null=True)