import datetime

from django.conf import settings
from oauth2client import GOOGLE_REVOKE_URI, GOOGLE_TOKEN_URI
from oauth2client.client import OAuth2Credentials, _extract_id_token
from social.apps.django_app.default.models import UserSocialAuth
from social.backends.salesforce import SalesforceOAuth2
from social.backends.yahoo import YahooOAuth2
from social.exceptions import AuthException
import logging

from bizintro.google import GoogleAPI
from bizintro.helpers import Provider, send_event_to_intercom, finish_signup, Storage, sync_contacts_async, connect_objects_to_user
from bizintro.microsoft import MicrosoftAPI, MicrosoftOAuth2
from bizintro.models import CalendarAccount, CredentialsModel, ImportContact
from multimail.models import EmailAddress

logger = logging.getLogger(__name__)

def save_credentials(strategy, details, response, user, social, *args, **kwargs):
    """
    Saves a CredentialsModel object to the DB for easier API access.
    """
    if social.provider == Provider.Google:
        token_expiry = datetime.datetime.utcnow() + datetime.timedelta(seconds=int(response.get('expires_in')))
        id_token = _extract_id_token(response.get('id_token'))
        credential = OAuth2Credentials(
            access_token=response.get('access_token'),
            client_id=settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY,
            client_secret=settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET,
            refresh_token=response.get('refresh_token'),
            token_expiry=token_expiry,
            token_uri=GOOGLE_TOKEN_URI,
            user_agent=None,
            revoke_uri=GOOGLE_REVOKE_URI,
            id_token=id_token,
            token_response=response)
        storage = Storage(CredentialsModel, 'auth', social, 'credential')
        storage.put(credential)
    elif social.provider == Provider.Yahoo:
        token_expiry = datetime.datetime.utcnow() + datetime.timedelta(seconds=int(response.get('expires_in')))
        id_token = response.get('xoauth_yahoo_guid')
        credential = OAuth2Credentials(
            access_token=response.get('access_token'),
            client_id=settings.SOCIAL_AUTH_YAHOO_OAUTH2_KEY,
            client_secret=settings.SOCIAL_AUTH_YAHOO_OAUTH2_SECRET,
            refresh_token=response.get('refresh_token'),
            token_expiry=token_expiry,
            token_uri=YahooOAuth2.ACCESS_TOKEN_URL,
            user_agent=None,
            revoke_uri=None,
            id_token=id_token,
            token_response=response)
        storage = Storage(CredentialsModel, 'auth', social, 'credential')
        storage.put(credential)
    elif social.provider == Provider.Microsoft:
        token_expiry = datetime.datetime.utcnow() + datetime.timedelta(seconds=int(response.get('expires_in')))
        id_token = _extract_id_token(response.get('id_token'))
        credential = OAuth2Credentials(
            access_token=response.get('access_token'),
            client_id=settings.SOCIAL_AUTH_MICROSOFT_OAUTH2_KEY,
            client_secret=settings.SOCIAL_AUTH_MICROSOFT_OAUTH2_SECRET,
            refresh_token=response.get('refresh_token'),
            token_expiry=token_expiry,
            token_uri=MicrosoftOAuth2.ACCESS_TOKEN_URL,
            user_agent=None,
            revoke_uri=None,
            id_token=id_token,
            token_response=response)
        storage = Storage(CredentialsModel, 'auth', social, 'credential')
        storage.put(credential)
    elif social.provider == Provider.Salesforce:
        credential = OAuth2Credentials(
            access_token=response.get('access_token'),
            client_id=settings.SOCIAL_AUTH_SALESFORCE_OAUTH2_KEY,
            client_secret=settings.SOCIAL_AUTH_SALESFORCE_OAUTH2_SECRET,
            refresh_token=response.get('refresh_token'),
            token_expiry=None,
            token_uri=SalesforceOAuth2.ACCESS_TOKEN_URL,
            user_agent=None,
            revoke_uri=None,
            id_token=None,
            token_response=response)
        storage = Storage(CredentialsModel, 'auth', social, 'credential')
        storage.put(credential)


def save_contacts(strategy, user, is_new, social, *args, **kwargs):
    if is_new:
        if social.provider != Provider.LinkedIn:
            sync_contacts_async(user.profile.id,social.id)
    else:
        if social.provider != Provider.LinkedIn:
            sync_contacts_async(user.profile.id,"all")

def set_extra_data(strategy, user, social, *args, **kwargs):
    if social.provider in Provider.Calendars and user.profile.get_calendar_account() is None:
        calendar_id = get_default_calendar_id(social)
        CalendarAccount.objects.create(user_profile=user.profile, social_auth=social, calendar_id=calendar_id)
    if social.provider == Provider.LinkedIn:
        url = social.extra_data.get('public_profile_url')
        if url:
            user_profile = user.profile
            user_profile.linkedin_url = url
            user_profile.save()


def finish_signup_process(strategy, user, is_new, *args, **kwargs):
    if not is_new:
        return

    timezone = strategy.session_get('timezone')
    finish_signup(user, timezone)
    send_event_to_intercom("Signed Up", user)


def clear_credentials(strategy, entries, user, name, *args, **kwargs):
    CredentialsModel.objects.filter(auth__in=entries).delete()


def clear_contacts(strategy, entries, user, name, *args, **kwargs):
    ImportContact.objects.filter(user_profile=user.profile, auth_source=entries).delete()
    pass



def clear_extra_data(strategy, entries, user, name, *args, **kwargs):
    CalendarAccount.objects.filter(user_profile=user.profile, social_auth__in=entries).delete()
    if user.profile.get_calendar_account() is None:
        try:
            social = user.social_auth.filter(provider__in=Provider.Calendars).exclude(id__in=entries)[:1].get()
            calendar_id = get_default_calendar_id(social)
            CalendarAccount.objects.create(user_profile=user.profile, social_auth=social, calendar_id=calendar_id)
        except UserSocialAuth.DoesNotExist:
            pass

    if name == Provider.LinkedIn:
        user_profile = user.profile
        user_profile.linkedin_url = ''
        user_profile.save()


def get_default_calendar_id(social):
    calendar_id = None
    if social.provider == Provider.Google:
        google_api = GoogleAPI(social.user, social)
        calendars = google_api.get_calendars()
        if calendars:
            calendar_id = calendars[0]['id']
    elif social.provider == Provider.Microsoft:
        microsoft_api = MicrosoftAPI(social.user, social)
        calendars = microsoft_api.get_calendars()
        if calendars:
            calendar_id = calendars[0]['id']
    return calendar_id

def associate_by_email(backend, details, user=None, *args, **kwargs):
    """
    Associate current auth with a user with the same email address in the DB.
    This pipeline entry is not 100% secure unless you know that the providers
    enabled enforce email verification on their side, otherwise a user can
    attempt to take over another user account by using the same (not validated)
    email address on some provider.  This pipeline entry is disabled by
    default.
    """
    if user:
        return None

    email = details.get('email')
    if email:
        # Try to associate accounts registered with the same email address,
        # only if it's a single object. AuthException is raised if multiple
        # objects are returned.
        emails = EmailAddress.objects.filter(email__iexact=email)
        if len(emails) == 0:
            return None
        elif len(emails) > 1:
            raise AuthException(
                backend,
                'The given email address is associated with another account'
            )
        else:
            return {'user': emails[0].user}
           
def add_email_address(details, user, is_new, *args, **kwargs):
    email = details.get('email')
    try:
        addr = EmailAddress.objects.get(user=user, email__iexact=email)
        if not addr.is_verified():
            addr.verified_at = datetime.datetime.now()
            addr.save(verify=False)
    except EmailAddress.DoesNotExist:
        addr = EmailAddress(user=user, email=email, verified_at=datetime.datetime.now())
        addr.save(verify=False)
    connect_objects_to_user(user,email)
