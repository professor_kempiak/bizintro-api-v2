import logging

import requests
from django.db import IntegrityError

from social.apps.django_app.default.models import UserSocialAuth

from bizintro.helpers import SocialAPI, combine_contacts, combine_contacts_async, Provider
from bizintro.models import ImportContact, ExchangeAccount, CalendarAccount


def save_exchange_account_to_db(user, username, password, server):
    social_auth, created = UserSocialAuth.objects.get_or_create(user=user,provider=Provider.Exchange,uid=username)

    # If this is a new exchange account
    if created:
        # Create a new ExchangeAccount object
        exchange_account = ExchangeAccount(user_profile=user.profile,username=username,password=password,server=server,failed_attempts=0,disabled=False)
        exchange_account.save()
        social_auth.extra_data = { 'exchange_id': exchange_account.id }
        social_auth.save()
    else:
        exchange_id = social_auth.extra_data['exchange_id']
        exchange_account = ExchangeAccount.objects.get(id=exchange_id)
        exchange_account.username=username
        exchange_account.password=password
        exchange_account.server=server
        exchange_account.failed_attempts=0
        exchange_account.disabled=False
        exchange_account.save()
    exchange_api = ExchangeAPI(user, social_auth)
    count = exchange_api.get_contacts()
    calendars = exchange_api.get_calendars()
    if calendars:
        calendar_id = calendars[0]['id']
        calendar_account = CalendarAccount.objects.get(user_profile=user.profile)
        if not calendar_account:
            calendar_account = CalendarAccount(user_profile=user.profile)
        calendar_account.social_auth=social_auth
        calendar_account.calendar_id=calendar_id
        calendar_account.save()

def test_credentials(username,password,server):
    params = {'type': 'testCredentials', 'email': username, 'password': password, 'server': server}

    r = requests.get(ExchangeAPI.api, params=params)

    if r.json()['success']:
        return True
    else:
        return False
        
def autodiscover_exchange_server(username, password):
    params = {'type': 'autodiscover', 'email': username, 'password': password}

    r = requests.get(ExchangeAPI.api, params=params)

    json = r.json()
    if json['success']:
        return json['data']['server']
    else:
        return False

class ExchangeAPI(SocialAPI):
    logger = logging.getLogger(__name__)

    api = 'http://localhost:8889/exchange_api.php'

    def __init__(self, user, social_auth):
        self.user = user
        self.social_auth = social_auth
        self.exchange_account = ExchangeAccount.objects.get(id=social_auth.extra_data['exchange_id'], user_profile=self.user.profile)
        
    def get(self, params={}, thetype=None):
        params['type']=thetype
        params['email']=self.exchange_account.username
        params['password']=self.exchange_account.password
        params['server']=self.exchange_account.server

        if not self.exchange_account.disabled:
            r = requests.get(self.api, params=params)

            return_json = r.json()

            if not return_json['success']:
                self.handle_error()
                raise Exception

            return return_json
        else:
            self.log_exception("Account disabled so not even trying to connect")
            return []

    def get_availability(self, starttime, endtime):
        
        output_events = []

        self.log_info(starttime)
        self.log_info(endtime)

        for calendar in self.get_calendars():
            self.log_info(calendar['id'])
            calendar_id = calendar['id']

            try:
                params = {'calendar':calendar_id, 'timeZone':self.user.profile.timezone, 'startDate':starttime, 'endDate':endtime}
                events = self.get(params=params,thetype='getFreeBusy')
                for event in events['data']:
                    try:
                        temp_event = {
                            'title': event["title"],
                            'start': event["start"],
                            'end': event["end"],
                            'editable': False,
                            'className': 'unavailableTime',
                            'overlap': True
                        }
                        output_events.append(temp_event)
                    except Exception:
                        pass
            except Exception:
                return []

        return output_events

    def get_contacts(self,async=True):
        contacts = self.get(thetype='getContacts')

        source = ImportContact.EXCHANGE
        user_profile = self.user.profile

        bulk_create_contacts = []
        exchange_contacts_query = ImportContact.objects.filter(user_profile=user_profile, source=source)
        exchange_contacts_by_id = dict((contact.external_id, contact) for contact in exchange_contacts_query)

        try:
            for contact in contacts["data"]:
                try:
                    # required variables!
                    emails = []
                    for email in contact["EmailAddresses"]:
                        emails.append(email.get("Address"))

                    if any("reply.linkedin.com" in s for s in emails):
                        continue
                        
                    first_name = contact.get("GivenName") if contact.get("GivenName") else None
                    last_name = contact.get("Surname") if contact.get("Surname") else None
                    
                    if not first_name or not last_name or not len(emails)>0:
                        continue

                    # optional variables
                    addresses = []
                    for address in contact.get("PhysicalAddresses"):
                        addresses.append("%s %s %s, %s" % (address.get("Street"),address.get("City"),address.get("State"),address.get("PostalCode")))
                    title = contact.get("JobTitle") if contact.get("JobTitle") else ""
                    company = contact.get("CompanyName") if contact.get("CompanyName") else ""
                    phones = []
                    for phone in contact.get("PhoneNumbers"):
                        phones.append(phone.get("Phone"))
                    external_id = str(contact.get("Id"))
                except Exception:
                    self.log_exception('Could not parse Exchange Contacts entry')
                    continue

                try:
                    if external_id in exchange_contacts_by_id:
                        contact = exchange_contacts_by_id[external_id]
                        created = False
                    else:
                        contact = ImportContact(user_profile=user_profile, source=source, external_id=external_id)
                        created = True

                    contact.data={"first_name":first_name,"last_name":last_name,"emails":emails,"phone_numbers":phones,"addresses":addresses,"title":title,"company":company}

                    contact.auth_source = self.social_auth

                    if created:
                        # if this contact did not exist in DB yet, we can insert it in bulk at the end
                        bulk_create_contacts.append(contact)
                    elif contact.is_dirty(check_relationship=True):
                        # only save to DB if existing object's fields were modified
                        contact.save()
                except Exception:
                    self.log_exception('Could not save Contact to DB')
        except Exception:
            return []

        # now insert Contacts to DB in bulk for maximum efficiency
        if len(bulk_create_contacts):
            try:
                ImportContact.objects.bulk_create(bulk_create_contacts)
            except IntegrityError:
                # if there's an error saving in bulk, we'll have to do it the slow way: save each Contact individually
                for contact in bulk_create_contacts:
                    try:
                        contact.save()
                    except Exception:
                        self.log_exception('Could not save Contact to DB')

        if async:
            combine_contacts_async(self.user.profile.id, source)
        else:
            combine_contacts(self.user.profile.id, source)
        
        return len(ImportContact.objects.filter(user_profile=user_profile, auth_source=self.social_auth))

    def get_calendars(self):
        calendars = []

        try:
            exchange_calendars = self.get(thetype='getCalendars')

            for calendar in exchange_calendars['data']:
                calendars.append({
                    'id': calendar['id'],
                    'name': calendar['name'],
                    'provider_id': self.social_auth.id,
                    'provider_type': 'Exchange',
                    'provider_name': self.exchange_account.username
                })

        except Exception as e:
            return []

        return calendars

    def handle_error(self):
        self.exchange_account.failed_attempts += 1
        self.exchange_account.save()
        if self.exchange_account.failed_attempts >= 4:
            self.exchange_account.disabled = True
            self.exchange_account.save()


    def create_event(self, calendar_account, recipient, subject, body, location,
                     appt_start_date_time_tz_aware, appt_end_date_time_tz_aware):
        calendar_id = calendar_account.calendar_id
        
        params = {
            'calendar': calendar_id,
            'subject': subject,
            'body': body,
            'startDate': appt_start_date_time_tz_aware.strftime('%Y-%m-%dT%H:%M:%SZ'),
            'endDate': appt_end_date_time_tz_aware.strftime('%Y-%m-%dT%H:%M:%SZ'),
            'location': location,
            'email1': self.user.email,
            'email2': self.user.profile.full_name(),
            'name1': recipient.email,
            'name2': recipient.profile.full_name()
        }

        self.get(thetype='createAppointment',params=params)
