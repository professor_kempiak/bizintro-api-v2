from django.conf.urls import url
from bizintro.apis import help

urlpatterns = (
	url(r'^contacts$', help.HelpContactsView.as_view(), name='help_contacts'),
	url(r'^list$', help.HelpListView.as_view(), name='all_helps'),
	url(r'^decline_introduction$', help.HelpIntroductionDeclineView.as_view(), name='decline'),
	#url(r'^accept$', help.HelpAcceptView.as_view(), name='help_accept'),
    url(r'^$', help.HelpView.as_view(), name='help'),
    url(r'^(?P<help_uuid>[a-zA-Z0-9\-]+)$', help.HelpView.as_view(), name='help'),
)