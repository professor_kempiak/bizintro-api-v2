# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bizintro', '0002_nudge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tour_key', models.CharField(default=b'', max_length=200)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('user_profile', models.ForeignKey(to='bizintro.UserProfile')),
            ],
        ),
    ]
