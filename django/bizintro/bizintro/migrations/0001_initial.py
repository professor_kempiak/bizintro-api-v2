# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import bizintro.encryption
import dirtyfields.dirtyfields
import django.contrib.postgres.fields
from django.conf import settings
import oauth2client.django_orm
import uuid


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('social_django', '0005_auto_20160727_2333'),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date_time', models.DateTimeField(auto_now_add=True)),
                ('message', models.CharField(default=b'', max_length=300)),
                ('link', models.CharField(default=b'', max_length=300)),
                ('archived', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Appt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('comments', models.TextField(default=b'', max_length=1000)),
                ('created_date_time', models.DateTimeField(auto_now_add=True)),
                ('start_date_time', models.DateTimeField()),
                ('end_date_time', models.DateTimeField()),
                ('appt_type', models.CharField(max_length=20, null=True, choices=[(b'phone', b'Phone Meeting'), (b'office', b'My Office'), (b'your_office', b'Your Office'), (b'online', b'Online Meeting'), (b'other', b'Other')])),
                ('location', models.CharField(default=b'', max_length=200)),
                ('archived', models.BooleanField(default=False)),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='ApptRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('recipient_email', models.CharField(default=b'', max_length=200)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('message', models.TextField(default=b'', max_length=1000)),
                ('created_date_time', models.DateTimeField(auto_now_add=True)),
                ('length', models.IntegerField()),
                ('status', models.CharField(default=b'o', max_length=1, choices=[(b'o', b'Open'), (b'c', b'Complete'), (b'd', b'Closed by recipient'), (b'f', b'Closed by owner'), (b't', b'Closed to find new times')])),
                ('appt_type', models.CharField(max_length=20, null=True, choices=[(b'phone', b'Phone Meeting'), (b'office', b'My Office'), (b'your_office', b'Your Office'), (b'online', b'Online Meeting'), (b'other', b'Other')])),
                ('location', models.CharField(default=b'', max_length=200)),
                ('archived', models.BooleanField(default=False)),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='AsyncJob',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=1, choices=[(b'c', b'combine_contacts'), (b's', b'sync_contacts')])),
                ('acknowledged', models.BooleanField(default=False)),
                ('start_date_time', models.DateTimeField(auto_now_add=True)),
                ('end_date_time', models.DateTimeField(null=True)),
                ('status', models.CharField(max_length=1, choices=[(b'p', b'PROGRESS'), (b'f', b'FAILED'), (b's', b'SUCCESS')])),
                ('message', models.CharField(default=b'', max_length=200)),
                ('task_id', models.CharField(default=b'', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='CalendarAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_calendar_id', models.CharField(max_length=200)),
                ('social_auth', models.OneToOneField(to='social_django.UserSocialAuth')),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(default=b'', max_length=200)),
                ('last_name', models.CharField(default=b'', max_length=200)),
                ('address', models.CharField(default=b'', max_length=200)),
                ('email', models.CharField(default=b'', max_length=200)),
                ('phone_number', models.CharField(default=b'', max_length=200)),
                ('occupation', models.CharField(default=b'', max_length=400)),
                ('addresses', django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(default=b'', max_length=200), size=None)),
                ('emails', django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(default=b'', max_length=200), size=None)),
                ('phone_numbers', django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(default=b'', max_length=200), size=None)),
                ('occupations', django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(default=b'', max_length=400), size=None)),
            ],
            bases=(dirtyfields.dirtyfields.DirtyFieldsMixin, models.Model),
        ),
        migrations.CreateModel(
            name='ContactShare',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('recipient_email', models.CharField(default=b'', max_length=200)),
                ('message', models.TextField(default=b'', max_length=1000)),
                ('created_date_time', models.DateTimeField(auto_now_add=True)),
                ('archived', models.BooleanField(default=False)),
                ('share_type', models.CharField(default=b'a', max_length=1, choices=[(b'a', b'All Contact Data'), (b'p', b'Primary Data Only')])),
                ('status', models.CharField(default=b'o', max_length=1, choices=[(b'o', b'Open'), (b'c', b'Complete'), (b'd', b'Closed by recipient'), (b'f', b'Closed by owner')])),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('created_contact', models.ForeignKey(related_name='+', to='bizintro.Contact', null=True)),
                ('recipient_contact', models.ForeignKey(related_name='+', to='bizintro.Contact')),
            ],
        ),
        migrations.CreateModel(
            name='CredentialsModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('credential', oauth2client.django_orm.CredentialsField(null=True)),
                ('auth', models.OneToOneField(to='social_django.UserSocialAuth')),
            ],
        ),
        migrations.CreateModel(
            name='ExchangeAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=255)),
                ('password', bizintro.encryption.EncryptedCharField(max_length=255)),
                ('server', models.CharField(max_length=500)),
                ('disabled', models.BooleanField(default=False)),
                ('failed_attempts', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='FullContactData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(unique=True, max_length=255)),
                ('data', jsonfield.fields.JSONField(null=True)),
                ('usefuldata', jsonfield.fields.JSONField(null=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('last_checked', models.DateTimeField(null=True)),
                ('requestId', models.CharField(max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Help',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('recipient_email', models.CharField(default=b'', max_length=200)),
                ('message', models.TextField(default=b'', max_length=1000)),
                ('created_date_time', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(default=b'o', max_length=1, choices=[(b'o', b'Open'), (b'a', b'Accepted'), (b'c', b'Complete'), (b'd', b'Closed by recipient'), (b'f', b'Closed by owner')])),
                ('archived', models.BooleanField(default=False)),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('contact', models.ForeignKey(to='bizintro.Contact')),
            ],
        ),
        migrations.CreateModel(
            name='ImportContact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source', models.CharField(default=b'm', max_length=1, choices=[(b'g', b'Google Contacts'), (b'y', b'Yahoo'), (b'l', b'LinkedIn'), (b's', b'SalesForce'), (b'o', b'Outlook'), (b'x', b'Exchange'), (b'a', b'Office365'), (b'e', b'Other'), (b'm', b'Manually Entered')])),
                ('external_id', models.CharField(max_length=200, null=True)),
                ('last_imported', models.DateTimeField(auto_now=True)),
                ('data', jsonfield.fields.JSONField(null=True)),
                ('auth_source', models.ForeignKey(to='social_django.UserSocialAuth', null=True)),
                ('contact', models.ForeignKey(to='bizintro.Contact', null=True)),
            ],
            bases=(dirtyfields.dirtyfields.DirtyFieldsMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Introduction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('recipient_a_email', models.CharField(default=b'', max_length=200)),
                ('recipient_b_email', models.CharField(default=b'', max_length=200)),
                ('message', models.TextField(default=b'', max_length=1000)),
                ('secondaryMessage', models.TextField(default=b'', max_length=1000)),
                ('created_date_time', models.DateTimeField(auto_now_add=True)),
                ('archived', models.BooleanField(default=False)),
                ('status', models.CharField(default=b'o', max_length=1, choices=[(b'o', b'Open'), (b'c', b'Complete'), (b'd', b'Closed by recipient A'), (b'e', b'Closed by recipient B'), (b'f', b'Closed by owner')])),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('contact_a', models.ForeignKey(related_name='+', to='bizintro.Contact')),
                ('contact_b', models.ForeignKey(related_name='+', to='bizintro.Contact')),
            ],
        ),
        migrations.CreateModel(
            name='License',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=20, null=True, choices=[(b'manual', b'Manual'), (b'trial', b'Trial')])),
                ('notes', models.TextField(default=b'', max_length=1000)),
                ('created_date_time', models.DateTimeField(auto_now_add=True)),
                ('start_date_time', models.DateTimeField()),
                ('end_date_time', models.DateTimeField()),
                ('revoked', models.BooleanField(default=False)),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='PotentialIntroduction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('notes', models.CharField(default=None, max_length=1000, null=True)),
                ('status', models.CharField(default=b'o', max_length=1, choices=[(b'o', b'Open'), (b'd', b'Declined'), (b'c', b'Complete')])),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('contact', models.ForeignKey(to='bizintro.Contact')),
                ('help', models.ForeignKey(to='bizintro.Help')),
                ('introduction', models.ForeignKey(blank=True, to='bizintro.Introduction', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TimeSlot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date_time', models.DateTimeField()),
                ('end_date_time', models.DateTimeField()),
                ('appt_request', models.ForeignKey(to='bizintro.ApptRequest')),
            ],
        ),
        migrations.CreateModel(
            name='UPCache',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=1, choices=[(b't', b'TYPEAHEAD'), (b'h', b'HELP'), (b'c', b'CONTACTS')])),
                ('data', jsonfield.fields.JSONField(null=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone_number', models.CharField(default=b'', max_length=200)),
                ('address', models.CharField(default=b'', max_length=200)),
                ('timezone', models.CharField(default=b'America/Chicago', max_length=50)),
                ('helpTemplate', models.TextField(default=b'', max_length=1000)),
                ('introTemplate', models.TextField(default=b'', max_length=1000)),
                ('introSecondaryTemplate', models.TextField(default=b'', max_length=1000)),
                ('apptTemplate', models.TextField(default=b'', max_length=1000)),
                ('contactShareTemplate', models.TextField(default=b'', max_length=1000)),
                ('standing_meeting_times', models.TextField(default=b'', max_length=1000)),
                ('linkedin_url', models.URLField(default=b'')),
                ('summary', models.TextField(default=b'', max_length=1000)),
                ('title', models.CharField(default=b'', max_length=200)),
                ('company', models.CharField(default=b'', max_length=200)),
                ('signature', models.TextField(default=b'', max_length=1000)),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='upcache',
            name='user_profile',
            field=models.ForeignKey(related_name='cache', to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='timeslot',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='license',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='introduction',
            name='recipient_user_profile_a',
            field=models.ForeignKey(related_name='+', blank=True, to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='introduction',
            name='recipient_user_profile_b',
            field=models.ForeignKey(related_name='+', blank=True, to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='introduction',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='importcontact',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='help',
            name='recipient_user_profile',
            field=models.ForeignKey(related_name='+', blank=True, to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='help',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='exchangeaccount',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='contactshare',
            name='recipient_user_profile',
            field=models.ForeignKey(related_name='+', blank=True, to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='contactshare',
            name='shared_contact',
            field=models.ForeignKey(related_name='+', to='bizintro.Contact'),
        ),
        migrations.AddField(
            model_name='contactshare',
            name='shared_user_profile',
            field=models.ForeignKey(related_name='+', blank=True, to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='contactshare',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='contact',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='calendaraccount',
            name='user_profile',
            field=models.OneToOneField(related_name='calendar_account', to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='asyncjob',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='apptrequest',
            name='contact',
            field=models.ForeignKey(to='bizintro.Contact'),
        ),
        migrations.AddField(
            model_name='apptrequest',
            name='introduction',
            field=models.ForeignKey(blank=True, to='bizintro.Introduction', null=True),
        ),
        migrations.AddField(
            model_name='apptrequest',
            name='recipient_user_profile',
            field=models.ForeignKey(related_name='+', blank=True, to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='apptrequest',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='appt',
            name='appt_request',
            field=models.ForeignKey(to='bizintro.ApptRequest'),
        ),
        migrations.AddField(
            model_name='appt',
            name='recipient_user_profile',
            field=models.ForeignKey(related_name='+', blank=True, to='bizintro.UserProfile', null=True),
        ),
        migrations.AddField(
            model_name='appt',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
        migrations.AddField(
            model_name='activity',
            name='user_profile',
            field=models.ForeignKey(to='bizintro.UserProfile'),
        ),
    ]
