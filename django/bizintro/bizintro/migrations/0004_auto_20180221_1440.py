# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bizintro', '0003_tour'),
    ]

    operations = [
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=250)),
                ('description', models.CharField(default=b'', max_length=250)),
                ('contents', models.TextField(default=b'', max_length=1000)),
                ('category', models.CharField(max_length=1, choices=[(b'i', b'introduction'), (b's', b'scheduling')])),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_edited', models.DateTimeField(null=True)),
                ('user', models.ForeignKey(to='bizintro.UserProfile')),
            ],
        ),
        migrations.AddField(
            model_name='contact',
            name='bio',
            field=models.CharField(default=b'', max_length=5000),
        ),
    ]
