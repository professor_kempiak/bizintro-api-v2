from django.contrib import messages
from django.db.models import Q
from django.contrib.messages import MessageFailure
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.utils import timezone
from django.utils.http import urlquote
from social.apps.django_app.middleware import SocialAuthExceptionMiddleware
from social.exceptions import SocialAuthBaseException
from social.utils import social_logger
from bizintro.models import Contact, Help, ContactShare, ApptRequest, Introduction


class TimezoneMiddleware(object):
    def process_request(self, request):
        if request.user.is_authenticated():
            tz = request.user.profile.timezone
        else:
            tz = "America/Chicago"
        
        if tz:
            timezone.activate(tz)


class SocialAuthRedirectMiddleware(SocialAuthExceptionMiddleware):
    """Middleware that handles Social Auth AuthExceptions by providing the user
    with a message, logging an error, and redirecting to some next location.

    If user is logged in, will redirect to the "external" page; otherwise,
    will redirect to the "login" page.
    """
    def process_exception(self, request, exception):
        strategy = getattr(request, 'social_strategy', None)
        if strategy is None or self.raise_exception(request, exception):
            return

        if isinstance(exception, SocialAuthBaseException):
            backend = getattr(request, 'backend', None)
            backend_name = getattr(backend, 'name', 'unknown-backend')

            message = self.get_message(request, exception)
            social_logger.error(message)

            url = self.get_redirect_uri(request, exception)
            try:
                messages.error(request, message)
            except MessageFailure:
                url += ('?' in url and '&' or '?') + \
                       'message={0}&backend={1}'.format(urlquote(message),
                                                        backend_name)
            return redirect(url)

    def get_redirect_uri(self, request, exception):
        if not request.user.is_authenticated():
            return reverse('login')
        return reverse('external')

    def raise_exception(self, request, exception):
        return False