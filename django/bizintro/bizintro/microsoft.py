import base64
import json
import logging

import requests

from django.db import IntegrityError

from social.backends.oauth import BaseOAuth2

from bizintro.helpers import SocialAPI, combine_contacts, combine_contacts_async
from bizintro.models import CredentialsModel, ImportContact


class MicrosoftOAuth2(BaseOAuth2):
    """Microsoft OAuth authentication backend"""
    name = 'microsoft-oauth2'
    AUTHORIZATION_URL = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize'
    ACCESS_TOKEN_URL = 'https://login.microsoftonline.com/common/oauth2/v2.0/token'
    REDIRECT_STATE = False
    SCOPE_SEPARATOR = ' '
    ID_KEY='preferred_username'
    EXTRA_DATA = [('name','name')]
    logger = logging.getLogger(__name__)

    def get_user_details(self, response):
        """Return user details from Github account"""
        return {
                'email': response.get('preferred_username') or '',
                'name': response.get('name')}

    def user_data(self, access_token, *args, **kwargs):
      id_token = kwargs.get('response').get('id_token')
      token_parts = id_token.split('.')
      encoded_token = token_parts[1]

      # base64 strings should have a length divisible by 4
      # If this one doesn't, add the '=' padding to fix it
      leftovers = len(encoded_token) % 4
      if leftovers == 2:
          encoded_token += '=='
      elif leftovers == 3:
          encoded_token += '='

      # URL-safe base64 decode the token parts
      # NOTE: Per issue #2, added additional encode('utf-8') call on
      # encoded_token so this call will work in Python 2.*
      decoded = base64.urlsafe_b64decode(encoded_token.encode('utf-8')).decode('utf-8')

      # Load decoded token into a JSON object
      jwt = json.loads(decoded)

      #return jwt['preferred_username']
      return jwt


class MicrosoftAPI(SocialAPI):
    logger = logging.getLogger(__name__)

    def _get_headers(self, extra_headers=None):
        credentials = CredentialsModel.get_credentials_from_auth(self.social_auth)
        headers = {'Authorization': 'Bearer ' + credentials.access_token,
                   'Accept': 'application/json',
                   'User-Agent' : 'Bizintro/1.0',
                   'X-AnchorMailbox': self.social_auth.extra_data.get('preferred_username')}
        if extra_headers is not None:
            headers.update(extra_headers)
        return headers

    def get(self, url, params=None, extra_headers=None):
        try:
            headers = self._get_headers(extra_headers)
            r = requests.get(url, headers=headers, params=params)
            return_val = r.json().get("value")
        except:
            return_val = {}
        return return_val

    def post(self, url, json_data=None, extra_headers=None):
        headers = self._get_headers(extra_headers)
        r = requests.post(url, headers=headers, json=json_data)
        return r  # not sure if we need a return value for this or not

    def get_availability(self, starttime, endtime):
        events = []
        params = {'startDateTime': starttime,
                  'endDateTime': endtime}
        extra_headers = {'Prefer': 'outlook.timezone="%s"' % self.user.profile.timezone}

        for calendar in self.get_calendars():
            url = 'https://outlook.office.com/api/v2.0/me/calendars/%s/calendarview' % calendar['id']
            r = self.get(url, params=params, extra_headers=extra_headers)
            if not r:
                continue

            for event in r:
                events.append({
                    'title': event["Subject"],
                    'start': event["Start"]["DateTime"],
                    'end': event["End"]["DateTime"],
                    'editable': False,
                    'className': 'unavailableTime',
                    'overlap': True
                })

        return events

    def create_event(self, calendar_account, recipient, subject, body, location,
                     appt_start_date_time_tz_aware, appt_end_date_time_tz_aware):
        if calendar_account.calendar_id:
            url = 'https://outlook.office.com/api/v2.0/me/calendars/%s/events' % calendar_account.calendar_id
        else:
            url = 'https://outlook.office.com/api/v2.0/me/events'
        data = {
            'Subject': subject,
            'Body': {'Content Type': 'HTML', 'Content': body},
            'Start': appt_start_date_time_tz_aware.strftime('%Y-%m-%dT%H:%M:%SZ'),
            'End': appt_end_date_time_tz_aware.strftime('%Y-%m-%dT%H:%M:%SZ'),
            'Location': {'DisplayName': location},
            'Attendees': [
                {'EmailAddress': {'Address': self.user.email, 'Name': self.user.profile.full_name()}, 'Type': 'Required'},
                {'EmailAddress': {'Address': recipient.email, 'Name': recipient.profile.full_name()}, 'Type': 'Required'},
            ],
        }

        self.post(url, json_data=data)

    def get_calendars(self):
        url = 'https://outlook.office.com/api/v2.0/me/calendars'
        r = self.get(url)

        if not r:
            return []

        calendars = []
        for microsoft_calendar in r:
            calendars.append({
                'id': microsoft_calendar['Id'],
                'name': microsoft_calendar['Name'],
                'provider_id': self.social_auth.id,
                'provider_type': 'Office365',
                'provider_name': self.social_auth.extra_data.get('preferred_username'),
            })
        return calendars

    def get_contacts(self, async=True):
        url = 'https://outlook.office.com/api/v2.0/me/contacts'
        params = {'$top': '1000',
                  '$select': 'GivenName,Surname,EmailAddresses,JobTitle,CompanyName,HomePhones,BusinessPhones,MobilePhone1,HomeAddress,BusinessAddress,OtherAddress,Id',
                  '$orderby': 'GivenName ASC'}

        r = self.get(url, params=params)

        if not r:
            return 0

        source = ImportContact.MICROSOFT
        user_profile = self.user.profile

        bulk_create_contacts = []  # new Contact objects will be added to this list to be inserted to the DB in bulk
        microsoft_contacts_query = ImportContact.objects.filter(user_profile=user_profile, source=source)
        microsoft_contacts_by_id = dict((contact.external_id, contact) for contact in microsoft_contacts_query)

        for mscontact in r:
            first_name = mscontact.get("GivenName","")
            last_name = mscontact.get("Surname","")
            emailObj = mscontact.get("EmailAddresses",None)

            emails = []
            if emailObj:
                for email in emailObj:
                    emails.append(email.get("Address"))

            if any("reply.linkedin.com" in s for s in emails):
                continue
            
            title = mscontact.get("JobTitle","")
            company = mscontact.get("CompanyName","")

            if not title:
                title = ""
            if not company:
                company = ""
            
            phones = []
            phoneObj = mscontact.get("HomePhones",None)
            if phoneObj:
                for phone in phoneObj:
                    phones.append(phone)
            phoneObj = mscontact.get("BusinessPhones",None)
            if phoneObj:
                for phone in phoneObj:
                    phones.append(phone)
            phoneObj = mscontact.get("MobilePhone1",None)
            if phoneObj:
                phones.append(phoneObj)

            addresses = []
            addressObj = mscontact.get("HomeAddress","")
            if addressObj.get("Street"):
                try:
                    address = "%s %s %s, %s" % (addressObj.get("Street"),addressObj.get("City"),addressObj.get("State"),addressObj.get("PostalCode"))
                    addresses.append(address)
                except Exception:
                    pass
                
            addressObj = mscontact.get("BusinessAddress","")
            if addressObj.get("Street"):
                try:
                    address = "%s %s %s, %s" % (addressObj.get("Street"),addressObj.get("City"),addressObj.get("State"),addressObj.get("PostalCode"))
                    addresses.append(address)
                except Exception:
                    pass
                
            addressObj = mscontact.get("OtherAddress","")
            if addressObj.get("Street"):
                try:
                    address = "%s %s %s, %s" % (addressObj.get("Street"),addressObj.get("City"),addressObj.get("State"),addressObj.get("PostalCode"))
                    addresses.append(address)
                except Exception:
                    pass
                
            external_id = str(mscontact.get("Id",None))

            if external_id in microsoft_contacts_by_id:
                contact = microsoft_contacts_by_id[external_id]
                created = False
            else:
                contact = ImportContact(user_profile=user_profile, source=source, external_id=external_id)
                created = True

            contact.data={"first_name":first_name,"last_name":last_name,"emails":emails,"phone_numbers":phones,"addresses":addresses,"title":title,"company":company}
            contact.auth_source = self.social_auth

            if created:
                # if this contact did not exist in DB yet, we can insert it in bulk at the end
                bulk_create_contacts.append(contact)
            elif contact.is_dirty(check_relationship=True):
                # only save to DB if existing object's fields were modified
                contact.save()
        # now insert Contacts to DB in bulk for maximum efficiency
        if len(bulk_create_contacts):
            try:
                ImportContact.objects.bulk_create(bulk_create_contacts)
            except IntegrityError:
                # if there's an error saving in bulk, we'll have to do it the slow way: save each Contact individually
                for contact in bulk_create_contacts:
                    try:
                        contact.save()
                    except Exception:
                        self.log_exception('Could not save Contact to DB')

        if async:
            combine_contacts_async(self.user.profile.id, source)
        else:
            combine_contacts(self.user.profile.id, source)
        
        return len(bulk_create_contacts)
