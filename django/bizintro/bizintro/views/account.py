from django.conf import settings
from django.contrib.auth import get_user_model, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.shortcuts import render, resolve_url
from django.template.response import TemplateResponse
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from social.apps.django_app.default.models import UserSocialAuth
from django.contrib import messages

from bizintro.exchange import save_exchange_account_to_db
from bizintro.forms import PasswordResetForm
from bizintro.helpers import is_valid_phone_number
from bizintro.models import CalendarAccount, Introduction, Help, Appt, ApptRequest, ContactShare, User, Contact, \
    ImportContact, ExchangeAccount


@login_required
def settingsPage(request):
    global redirect

    if request.method == 'POST':
        user = request.user
        user_profile = user.profile
        if 'updatingGeneral' in request.POST:
            user_profile.timezone = request.POST['timezone']
            user_profile.signature = request.POST['signature']
            user_profile.save()
            redirect = '/settings/#tab_general'
        elif 'updatingTemplates' in request.POST:
            user_profile.introTemplate = request.POST['introTemplate']
            user_profile.introSecondaryTemplate = request.POST['introSecondaryTemplate']
            user_profile.helpTemplate = request.POST['helpTemplate']
            user_profile.apptTemplate = request.POST['apptTemplate']
            user_profile.contactShareTemplate = request.POST['contactShareTemplate']
            user_profile.save()
            redirect = '/settings/#tab_templates'
        elif 'updatingProfile' in request.POST:
            if not is_valid_phone_number(request.POST['phone_number']):
                return render(request, 'settings.html', {'message': 'Phone number is invalid!'})

            user.first_name = request.POST['first_name']
            user.last_name = request.POST['last_name']
            user.username = user.email
            user_profile.phone_number = request.POST['phone_number']
            user_profile.address = request.POST['address']
            user_profile.title = request.POST['title']
            user_profile.company = request.POST['company']
            user_profile.summary = request.POST['summary']
            user_profile.linkedin_url = request.POST['linkedin_url']

            user.save()
            user_profile.save()
            redirect = '/settings/'

        return HttpResponseRedirect(redirect)
    else:
        return render(request, 'settings.html', {
            'emails': request.user.emailaddress_set.order_by('-is_primary', 'email'),
            'defaultIntroTemplate': settings.DEFAULT_INTRO_TEMPLATE.replace("\n", "\\n").replace("{user}",
                                                                                                 request.user.profile.full_name()),
            'defaultIntroSecondaryTemplate': settings.DEFAULT_INTRO_SECONDARY_TEMPLATE.replace("\n", "\\n").replace("{user}",
                                                                                                 request.user.profile.full_name()),
            'defaultHelpTemplate': settings.DEFAULT_HELP_TEMPLATE.replace("\n", "\\n").replace("{user}",
                                                                                               request.user.profile.full_name()),
            'defaultApptTemplate': settings.DEFAULT_APPT_TEMPLATE.replace("\n", "\\n").replace("{user}",
                                                                                               request.user.profile.full_name()),
            'defaultContactShareTemplate': settings.DEFAULT_CONTACT_SHARE_TEMPLATE.replace("\n", "\\n").replace(
                "{user}",
                request.user.profile.full_name())})

@login_required
def password_set(request):
    if request.user.has_usable_password():
        return HttpResponseRedirect(reverse('password_change'))

    next = request.GET.get('next')
    post_set_redirect = next or reverse('password_set_done')
    if request.method == "POST":
        form = SetPasswordForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return HttpResponseRedirect(post_set_redirect)
    else:
        form = SetPasswordForm(user=request.user)

    context = {
        'form': form,
        'title': 'Set a Password',
    }

    return TemplateResponse(request, 'password_set_form.html', context)


@login_required
def password_set_done(request):
    return TemplateResponse(request, 'password_set_done.html')


@login_required
def password_change(request,
                    template_name='password_change_form.html',
                    post_change_redirect=None,
                    password_change_form=PasswordChangeForm,
                    current_app=None, extra_context=None):
    if not request.user.has_usable_password():
        return HttpResponseRedirect(reverse('password_set'))

    post_change_redirect = reverse('password_change_done')
    if request.method == "POST":
        form = password_change_form(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return HttpResponseRedirect(post_change_redirect)
    else:
        form = password_change_form(user=request.user)

    context = {
        'form': form,
        'title': 'Password change',
    }

    return TemplateResponse(request, template_name, context)


@login_required
def password_change_done(request,
                         template_name='password_change_done.html',
                         current_app=None, extra_context=None):
    context = {
        'title': 'Password change successful',
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)


# 4 views for password reset:
# - password_reset sends the mail
# - password_reset_done shows a success message for the above
# - password_reset_confirm checks the link the user clicked and
#   prompts for a new password
# - password_reset_complete shows a success message for the above

def password_reset(request, is_admin_site=False,
                   template_name='reset/password_reset_form.html',
                   email_template_name='reset/password_reset_email.html',
                   subject_template_name='reset/password_reset_subject.txt',
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email="noreply@bizintro.com",
                   current_app=None,
                   extra_context=None,
                   html_email_template_name='reset/password_reset_html_email.html'):
    post_reset_redirect = reverse('password_reset_done')
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
            }
            form.save(**opts)
            return HttpResponseRedirect(post_reset_redirect)
    else:
        form = password_reset_form()
    context = {
        'form': form,
        'title': 'Password reset',
    }
    return TemplateResponse(request, template_name, context)


def password_reset_done(request,
                        template_name='reset/password_reset_done.html',
                        current_app=None, extra_context=None):
    context = {
        'title': 'Password reset sent',
    }

    return TemplateResponse(request, template_name, context)


@sensitive_post_parameters()
@never_cache
def password_reset_confirm(request, uidb64=None, token=None,
                           template_name='reset/password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=SetPasswordForm,
                           post_reset_redirect=None,
                           current_app=None, extra_context=None):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    UserModel = get_user_model()
    assert uidb64 is not None and token is not None  # checked by URLconf
    post_reset_redirect = reverse('password_reset_complete')
    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = 'Enter new password'
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(user)
    else:
        validlink = False
        form = None
        title = 'Password reset unsuccessful'
    context = {
        'form': form,
        'title': title,
        'validlink': validlink,
    }

    return TemplateResponse(request, template_name, context)


def password_reset_complete(request,
                            template_name='reset/password_reset_complete.html',
                            current_app=None, extra_context=None):
    context = {
        'login_url': resolve_url(settings.LOGIN_URL),
        'title': 'Password reset complete',
    }

    return TemplateResponse(request, template_name, context)


def bizmetrics(request):
    user_profile = request.user.profile
    context = {'intros_made': len(Introduction.objects.filter(user_profile=user_profile)),
               'intros_received': len(Introduction.objects.filter(
                   Q(recipient_user_profile_a=user_profile) | Q(recipient_user_profile_b=user_profile))),
               'helps_offered': len(Help.objects.filter(user_profile=user_profile)),
               'helps_received': len(Help.objects.filter(recipient_user_profile=user_profile)), 'confirmed_appts': len(
            Appt.objects.filter(Q(user_profile=user_profile) | Q(recipient_user_profile=user_profile))),
               'appt_requests_made': len(ApptRequest.objects.filter(user_profile=user_profile)),
               'appt_requests_received': len(ApptRequest.objects.filter(recipient_user_profile=user_profile)),
               'contacts_shared': len(ContactShare.objects.filter(user_profile=user_profile)),
               'contacts_received': len(ContactShare.objects.filter(recipient_user_profile=user_profile))}

    contacts = Contact.objects.filter(user_profile=user_profile)
    emails = [contact.email for contact in contacts]
    bizintro_users = User.objects.filter(Q(email__in=emails) | Q(userprofile=user_profile))

    for bizintro_user in bizintro_users:
        bizintro_user.num_intros_made = len(Introduction.objects.filter(user_profile=bizintro_user.profile))
        bizintro_user.num_helps_offered = len(Help.objects.filter(user_profile=bizintro_user.profile))
        bizintro_user.num_contacts_shared = len(ContactShare.objects.filter(user_profile=bizintro_user.profile))
        bizintro_user.total_given = (bizintro_user.num_intros_made +
                                     bizintro_user.num_helps_offered +
                                     bizintro_user.num_contacts_shared)
        bizintro_user.num_intros_received = len(Introduction.objects.filter(
            Q(recipient_user_profile_a=bizintro_user.profile) | Q(recipient_user_profile_b=bizintro_user.profile)))
        bizintro_user.num_helps_received = len(Help.objects.filter(recipient_user_profile=bizintro_user.profile))
        bizintro_user.num_contacts_received = len(
            ContactShare.objects.filter(recipient_user_profile=bizintro_user.profile))
        bizintro_user.total_received = (bizintro_user.num_intros_received +
                                        bizintro_user.num_helps_received +
                                        bizintro_user.num_contacts_received)

    context['top_giver'] = max(bizintro_users, key=lambda user: user.total_given)
    context['top_receiver'] = max(bizintro_users, key=lambda user: user.total_received)

    return render(request, 'bizmetrics.html', context)
