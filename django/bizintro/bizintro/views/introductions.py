from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.conf import settings

from bizintro.helpers import make_introduction, get_user_contacts, send_event_to_intercom, combine_contacts, onboarding_required
from bizintro.models import ApptRequest, Contact, Introduction, ImportContact, PotentialIntroduction, Help


@login_required
def introductions(request):
    introductions = Introduction.objects.filter(Q(user_profile=request.user.profile) | Q(recipient_user_profile_a=request.user.profile) | Q(recipient_user_profile_b=request.user.profile))
    return render(request, 'introductions.html', {'introductions': introductions})


@login_required
def introductions_detail(request, introduction_uuid):
    global intro_contact, contact_email, import_contact
    intro = Introduction.objects.get(uuid=introduction_uuid)

    if request.user.profile != intro.user_profile and request.user.profile != intro.recipient_user_profile_a and request.user.profile != intro.recipient_user_profile_b:
        messages.info(request, settings.NOT_YOUR_OBJECT_MESSAGE % 'Introduction')
        return redirect("/introductions")

    if request.user.profile == intro.user_profile:
        return render(request, 'introduction_detail.html', {'introduction': intro}) 

    if intro.status=='o':
        if request.user.profile == intro.recipient_user_profile_a:
            intro_contact = intro.contact_b
            contact_email = intro.recipient_b_email
        elif request.user.profile == intro.recipient_user_profile_b:
            intro_contact = intro.contact_a
            contact_email = intro.recipient_a_email

        contact = Contact.objects.get(email=contact_email, user_profile=request.user.profile)
        contact_id = contact.id
        return redirect("/request_appt?intro_uuid=%s&contact_id=%s" % (introduction_uuid, contact_id))
    else:
        appt = ApptRequest.objects.get(introduction=intro)
        return redirect("/appointments/requests/%s" % appt.uuid)

@login_required
@onboarding_required
def make_intro(request):
    if request.method == 'POST':
        user_profile = request.user.profile
        contact_a_id = request.POST.get('contact-a-id', False)
        contact_b_id = request.POST.get('contact-b-id', False)
        recipient_a_email = request.POST.get('recipient_a_email', False)
        recipient_b_email = request.POST.get('recipient_b_email', False)
        include_vcf_a = True
        include_vcf_b = True 
        message = request.POST.get('message', None)
        message2 = request.POST.get('message2', None)
        potential_intro_uuid = request.POST.get('potential_intro_uuid', False)
        introducer_email = request.user.email
        obj = make_introduction(user_profile, contact_a_id, contact_b_id, recipient_a_email, recipient_b_email,
                                message, message2, potential_intro_uuid, introducer_email, include_vcf_a, include_vcf_b)
        send_event_to_intercom("Made Introduction", request.user.email)
        if obj['message'] == 'success':
            messages.success(request, 'Introduction made!')
        elif obj['message'] == 'failure':
            messages.error(request, 'Introduction failed!')
        elif obj['message'] == 'help_success':
            messages.error(request, 'Introductions made!')

        if potential_intro_uuid:
            help_id = PotentialIntroduction.objects.get(uuid=potential_intro_uuid).help_id

            if PotentialIntroduction.objects.filter(help=help_id, status="o").count() == 0:
                help = Help.objects.get(id=help_id) 
                help.status="c"
                help.save()
                messages.success(request, 'Offer of Help Complete!')
        return obj["redirect"]
    else:
        introductions = Introduction.objects.filter(user_profile=request.user.profile).count()

        if request.user.profile.is_not_pro_or_trial and introductions >= settings.MAX_FREE_OBJECTS:
            messages.error(request,"You need to be a subscriber to make more than %s introductions." % settings.MAX_FREE_OBJECTS)
            return redirect("/payments/subscribe/")

        try:
            potential_intro_uuid = request.GET['potential_intro_uuid']
        except:
            potential_intro_uuid = ""
        return render(request, 'make_intro.html', {'potential_intro_uuid': potential_intro_uuid,
                                                   'no_introductions': False if introductions > 0 else True})


@login_required(login_url='/auto_login/')
def accept_intro(request, intro_uuid):
    global import_contact
    introduction = get_object_or_404(Introduction, uuid=intro_uuid)

    if request.user.profile == introduction.user_profile:
        return redirect("/introductions/%s" % introduction.uuid)

    try:
        appt_request = ApptRequest.objects.get(introduction=introduction)
        return redirect("/appointments/requests/%s" % appt_request.uuid)
    except:
        try:
            original_contact_id = request.GET['contact_id']
            original_contact = Contact.objects.get(pk=original_contact_id)
            if introduction.contact_a == original_contact:
                intro_contact = introduction.contact_a
                contact_email = introduction.recipient_a_email
            elif introduction.contact_b == original_contact:
                intro_contact = introduction.contact_b
                contact_email = introduction.recipient_b_email
            else:
                messages.info(request, settings.NOT_YOUR_OBJECT_MESSAGE % 'Introduction')
                return redirect("/introductions")
            try:
                contact = Contact.objects.get(email=contact_email, user_profile=request.user.profile)
                contact_id = contact.id
            except:
                import_contact = ImportContact(user_profile=request.user.profile, source='d')

                occupation = intro_contact.occupation.split(' at ')

                import_contact.data = {
                  'first_name': intro_contact.first_name,
                  'last_name': intro_contact.last_name,
                  'title': occupation[0],
                  'company': occupation[1] if len(occupation) > 1 else '',
                  'emails': [contact_email],
                  'phone_numbers': [intro_contact.phone_number],
                  'addresses': [intro_contact.address],
                }
                import_contact.save()
                combine_contacts(request.user.profile.id, 'd', imp_contact=import_contact)

                messages.success(request, '%s %s added to contacts!' % (intro_contact.first_name,intro_contact.last_name))

                contact = Contact.objects.get(email=contact_email, user_profile=request.user.profile)
                contact_id = contact.id

            return redirect("/request_appt?intro_uuid=%s&contact_id=%s" % (introduction.uuid, contact_id))
        except:
            return redirect("/request_appt?intro_uuid=%s" % introduction.uuid)
