from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

from bizintro.google import GoogleAPI
from bizintro.exchange import ExchangeAPI
from bizintro.helpers import Provider
from bizintro.microsoft import MicrosoftAPI
from bizintro.models import CalendarAccount, ImportContact


@login_required
def do_external(request):
    new_default_calendar_id = request.GET.get('calendar_id')
    new_default_calendar_source_id = request.GET.get('calendar_source')
    if new_default_calendar_id and new_default_calendar_source_id:
        social_auth = request.user.social_auth.get(id=new_default_calendar_source_id)
        calendar_account, _ = CalendarAccount.objects.get_or_create(user_profile=request.user.profile)
        calendar_account.social_auth = social_auth
        calendar_account.calendar_id = new_default_calendar_id
        calendar_account.save()
        return HttpResponseRedirect('/accounts/external_accounts/')
    
    context = {
        'can_disconnect': request.user.has_usable_password() or request.user.social_auth.count() > 1,
        'google_accounts': [],
        'exchange_accounts': [],
        'linkedin_accounts': [],
        'salesforce_accounts': [],
        'microsoft_accounts': [],
        'yahoo_accounts': [],
    }

    for social_auth in request.user.social_auth.all():
        if social_auth.provider == Provider.Google:
            context['google_accounts'].append(get_account_info(request.user, social_auth, 'email'))
        elif social_auth.provider == Provider.LinkedIn:
            context['linkedin_accounts'].append(get_account_info(request.user, social_auth, 'email_address'))
        elif social_auth.provider == Provider.Yahoo:
            context['yahoo_accounts'].append(get_account_info(request.user, social_auth, 'id'))
        elif social_auth.provider == Provider.Salesforce:
            context['salesforce_accounts'].append(get_account_info(request.user, social_auth, 'email'))
        elif social_auth.provider == Provider.Microsoft:
            context['microsoft_accounts'].append(get_account_info(request.user, social_auth, 'preferred_username'))
        elif social_auth.provider == Provider.Exchange:
            context['exchange_accounts'].append(get_account_info(request.user, social_auth, 'username'))

    return render(request, 'external.html', context)
    

def get_account_info(user, social_auth, user_field):
    global exchange_api
    count_contacts = ImportContact.objects.filter(user_profile=user.profile,auth_source=social_auth).count()
    calendars = []
    if social_auth.provider == Provider.Google:
        google_api = GoogleAPI(user, social_auth)
        calendars = google_api.get_calendars()
    elif social_auth.provider == Provider.Microsoft:
        microsoft_api = MicrosoftAPI(user, social_auth)
        calendars = microsoft_api.get_calendars()
    elif social_auth.provider == Provider.Exchange:
        exchange_api = ExchangeAPI(user, social_auth)
        calendars = exchange_api.get_calendars()

    if social_auth.provider == Provider.Exchange:
        name = social_auth.uid
        disabled = exchange_api.exchange_account.disabled
        server = exchange_api.exchange_account.server
    elif social_auth.provider == Provider.Yahoo:
        name = user.email
        disabled=False
        server=False
    else:
        name = social_auth.extra_data.get(user_field)
        disabled=False
        server=False


        
    calendar_account = user.profile.get_calendar_account()
    if calendar_account:
        primary_calendar_id = calendar_account.calendar_id
        primary_calendar_account_id = calendar_account.social_auth_id
    else:
        primary_calendar_id = None
        primary_calendar_account_id = None
        
    for calendar in calendars:
        calendar['primary'] = (calendar['id'] == primary_calendar_id and
                               calendar['provider_id'] == primary_calendar_account_id)
    
    return {
        'name': name,
        'id': social_auth.id,
        'contacts': count_contacts,
        'calendars': calendars,
        'disabled': disabled,
        'server': server
    }
