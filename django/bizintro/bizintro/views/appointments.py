from datetime import datetime, timedelta

import pytz
from pytz import timezone
from dateutil import tz
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from icalendar import Calendar, Event

from bizintro.google import GoogleAPI
from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button,
                              send_event_to_intercom,combine_contacts, onboarding_required)
from bizintro.microsoft import MicrosoftAPI
from bizintro.exchange import ExchangeAPI
from bizintro.models import Appt, ApptRequest, Contact, ImportContact, Introduction, TimeSlot


@login_required
def appointments(request):
    apptRequests = ApptRequest.objects.filter(Q(user_profile=request.user.profile) | Q(recipient_user_profile=request.user.profile))
    return render(request, 'appointments.html', {'apptRequests': apptRequests})

@login_required
def rerequest_appt(request, appt_req_uuid):
    appt_request = get_object_or_404(ApptRequest, uuid=appt_req_uuid)
    if request.user.profile != appt_request.user_profile and request.user.profile != appt_request.recipient_user_profile:
        messages.info(request, settings.NOT_YOUR_OBJECT_MESSAGE % 'Appointment Request')
        return redirect("/appointments")
    else:
        if appt_request.status in "cdft":
            messages.info(request, 'This Appointment Request has already been processed.')
            return redirect("/appointments/requests/%s" % appt_req_uuid)

        appt_request.status = "t"
        appt_request.save()

        messages.info(request, 'Original appointment request canceled - please choose a better date/time to meet.')

        contacts = Contact.objects.filter(user_profile=request.user.profile, emails__contains=[appt_request.user_profile.user.email])
        try:
            # If it exists, nothing to do
            contact = contacts[0]
   
            redirect_url = "/request_appt/?contact_id=%s" % contact.id 
            if appt_request.introduction:
               redirect_url = redirect_url + "&intro_uuid=" + str(appt_request.introduction.uuid)
            return redirect(redirect_url)
        except IndexError:
            messages.error(request, 'You do not have this person in your contacts!')
            return redirect("/appointment_requests")
          

@login_required
def appt_detail(request, appt_uuid):
    appt = get_object_or_404(Appt, uuid=appt_uuid)
    if request.user.profile != appt.user_profile and request.user.profile != appt.recipient_user_profile:
        messages.info(request, settings.NOT_YOUR_OBJECT_MESSAGE % 'Appointment')
        return redirect("/appointments")
    else:
        return render(request, 'appt_detail.html', {'appt': appt})

@login_required(login_url='/auto_login/')
def accept_timeslot(request, appt_req_uuid, timeslot):
    appt_request = get_object_or_404(ApptRequest, uuid=appt_req_uuid)

    if request.user.profile != appt_request.user_profile and request.user.profile != appt_request.recipient_user_profile:
        messages.info(request, settings.NOT_YOUR_OBJECT_MESSAGE % 'Appointment Request')
        return redirect("/appointments")

    if appt_request.status == "c":
        messages.info(request, 'This Appointment Request has already been processed.')
        return redirect("/appointments/requests/%s" % appt_req_uuid)

    if process_accept_timeslot(appt_request, request.user, timeslot, ""):
        messages.success(request, 'Appointment successfully created!')
        return redirect("/appointments/requests/%s" % appt_req_uuid)
    else:
        return HttpResponse(status=404)



@login_required
def appt_req_detail(request, appt_req_uuid):
    appt_request = get_object_or_404(ApptRequest, uuid=appt_req_uuid)

    if request.user.profile != appt_request.user_profile and request.user.profile != appt_request.recipient_user_profile:
        messages.info(request, settings.NOT_YOUR_OBJECT_MESSAGE % 'Appointment Request')
        return redirect("/appointments")

    if request.method == 'POST':
        if request.POST.get('selected-timeslot', False):
            if process_accept_timeslot(appt_request, request.user, request.POST.get('selected-timeslot'), request.POST.get('comments')):
                messages.success(request, 'Appointment successfully created!')
                return redirect("/")
            else:
                return HttpResponse(status=404)
        else:
            messages.error(request, 'Please select a timeslot!')
            return redirect("/appointments/requests/%s" % appt_request.uuid)
    elif request.user.profile.id == appt_request.user_profile.id:
        return render(request, 'appt_req_detail.html', {'appt': appt_request})
    else:
        timeslot_days = get_timeslot_days(appt_request)

        return render(request, 'appt_req_detail.html',
                      {'appt': appt_request, 'timeslot_days': sorted(timeslot_days, key=lambda day: day['datetime'])})


@login_required
#@onboarding_required
def request_appt(request):
    if request.method == 'POST':
        user_profile = request.user.profile
        if request.POST.get('contact_id', False) and request.POST.get('timeslots_available', False):
            contact = get_object_or_404(Contact, pk=request.POST['contact_id'])
            recipient_email = request.POST.get('recipient_email', False)
            appt_length = int(request.POST.get('appt_length', 30))
            intro_uuid = request.POST.get('intro_uuid', '')
            appt_type = request.POST.get('appt_type', '')
            location = request.POST.get('appt-locationinput', '')
            appt_title = request.POST.get('title', '')
            appt_type_indicator = "in person"
    
            if appt_type == "phone":
                appt_type_indicator = "via phone"
            elif appt_type == "online":
                appt_type_indicator = "via online meeting"
    
            appt_message = request.POST.get('message', '').replace("{contact}", contact.full_name()).replace("{contact_first}", contact.first_name).replace(
                "{location}", appt_type_indicator)

            appt_request = ApptRequest(user_profile=user_profile,
                                       contact=contact,
                                       recipient_email=recipient_email,
                                       length=appt_length,
                                       title=appt_title,
                                       message=appt_message,
                                       appt_type=appt_type,
                                       location=location)
            try:
                recipient_user = User.objects.get(email=contact.email)
                appt_request.recipient_user_profile = recipient_user.profile
            except User.DoesNotExist:
                pass
            if intro_uuid:
                try:
                    introduction = Introduction.objects.get(uuid=intro_uuid)
                    introduction.status='c'
                    introduction.save()
                    appt_request.introduction = introduction
                except Introduction.DoesNotExist:
                    pass
            appt_request.save()
            link = 'https://' + settings.DOMAIN + '/appointments/requests/' + str(appt_request.uuid)
            redo_link = 'https://' + settings.DOMAIN + '/rerequest_appt/' + str(appt_request.uuid)
            create_activity(('You requested an appointment with %s.' % (contact.full_name())), link, user_profile)
            if appt_request.recipient_user_profile:
                create_activity(('%s requested an appointment with you.' % (user_profile.full_name())), link,
                                appt_request.recipient_user_profile)

            appt_message += """
            <b>Appointment Details:</b><br>
            Type: %s<br>
            Location: %s<br>
            Length: %s mins<br><br>
            <b>Available Dates/Times: (timezone is currently %s)</b><br>
            """ % (
            appt_request.appt_type, appt_request.location, appt_request.length, appt_request.user_profile.timezone)

            timeslots_available = request.POST['timeslots_available'].split(',')
            for slot in timeslots_available:
                times = slot.split('_')
                start_date_time = datetime.strptime(times[0], "%a %b %d %Y %H:%M:%S GMT+0000")
                start_date_time_tz_aware = pytz.timezone(user_profile.timezone).localize(start_date_time).astimezone(
                    pytz.utc)
                end_date_time = datetime.strptime(times[1], "%a %b %d %Y %H:%M:%S GMT+0000")
                end_date_time_tz_aware = pytz.timezone(user_profile.timezone).localize(end_date_time).astimezone(
                    pytz.utc)
                timeslot = TimeSlot(user_profile=user_profile,
                                    appt_request=appt_request,
                                    start_date_time=start_date_time_tz_aware,
                                    end_date_time=end_date_time_tz_aware)
                timeslot.save()

            timeslot_days = get_timeslot_days(appt_request)

            counter = 0
            for day in timeslot_days:
                if counter == 0:
                    appt_message = appt_message + '<table class="btn-green" cellpadding="0" cellspacing="5" border="0"><tr><th>' + day["label"] + '</th><th></th></tr>'
                for slot in day["slots"]:
                    counter += 1
                    if counter % 2 == 1:
                        appt_message += """
                        <tr>
                            <td>
                                <a href="%s">%s</a>
                            </td>
                        """ % ('https://' + settings.DOMAIN + "/accept_timeslot/" + str(appt_request.uuid) + "/" + str(
                            slot["utc_datetime"]),
                               slot["datetime"].astimezone(timezone(appt_request.user_profile.timezone)).strftime(
                                   '%I:%M %P'))
                    else:
                        appt_message += """
                            <td>
                                <a href="%s">%s</a>
                            </td>
                        </tr>
                        """ % ('https://' + settings.DOMAIN + "/accept_timeslot/" + str(appt_request.uuid) + "/" + str(
                            slot["utc_datetime"]),
                               slot["datetime"].astimezone(timezone(appt_request.user_profile.timezone)).strftime(
                                   '%I:%M %P'))
                if counter % 2 == 1:
                    appt_message += "</tr>"
                appt_message += "</table>"
                counter = 0

            subject = "Appointment Request from %s" % user_profile.full_name()
            
            if send_email_with_button(subject, appt_message, redo_link, "Suggest Different Times", user_profile.full_name(), [recipient_email], request.user.email):
                send_event_to_intercom("Requested Appointment", request.user.email)
                
                extra_message = ""
                if appt_request.appt_type == "office" and appt_request.location != user_profile.address:
                    update_url = 'https://' + settings.DOMAIN + "/update_profile_after_request?type=address&value=" + appt_request.location

                    extra_message = " The location of your meeting is not the address in your profile - <a href='%s'>click here</a> to update it." % update_url

                if appt_request.appt_type == "phone" and appt_request.location != user_profile.phone_number:
                    update_url = 'https://' + settings.DOMAIN + "/update_profile_after_request?type=phone_number&value=" + appt_request.location

                    extra_message = " The phone number for your meeting is not the phone number in your profile - <a href='%s'>click here</a> to update it." % update_url

                messages.success(request, "Appointment successfully requested!%s" % extra_message)

                return redirect("/appointments")
            else:
                return HttpResponse(status=404)
        else:
            messages.error(request, 'Appointment request failed!')
            return redirect("/request_appt")
    else:
        apptRequestsMade = ApptRequest.objects.filter(user_profile=request.user.profile).count()

        if request.user.profile.is_not_pro_or_trial and apptRequestsMade >= settings.MAX_FREE_OBJECTS:
            messages.error(request,"You need to be a subscriber to request more than %s appointments." % settings.MAX_FREE_OBJECTS)
            return redirect("/payments/subscribe/")
        
        intro_uuid = request.GET.get('intro_uuid', '')
        return render(request, 'request_appt.html', {'intro_uuid': intro_uuid,
                                                     'no_appts': False if apptRequestsMade > 0 else True})


def get_timeslot_days(appt_request):
    timeslot_days = []
    timeslots = TimeSlot.objects.filter(appt_request=appt_request)
    for timeslot in timeslots:  # create a "timeslot_day" for each TimeSlot object
        timeslot_start_date_time = timeslot.start_date_time
        timeslot_day = {'datetime': timeslot_start_date_time,
                        'label': timeslot_start_date_time.strftime('%a, %b %d'), 'slots': []}
        while True:  # create a list of slots from each TimeSlot every 30 minutes
            if timeslot_start_date_time >= timeslot.end_date_time:
                break
            timeslot_day['slots'].append(
                {'utc_datetime': timeslot_start_date_time.strftime('%c'), 'datetime': timeslot_start_date_time})
            timeslot_start_date_time += timedelta(minutes=30)
        same_day = False
        for day in timeslot_days:  # combine "timeslot_day"s if they're the same day
            if day['datetime'].strftime('%a, %b %d') == timeslot_day['datetime'].strftime('%a, %b %d'):
                day['slots'] += timeslot_day['slots']
                day['slots'] = sorted(day['slots'], key=lambda slot: slot['datetime'])
                same_day = True
                break
        if not same_day:
            timeslot_days.append(timeslot_day)
    for day in timeslot_days:  # split overnight "timeslot_day"s into multiple "timeslot_day"s
        new_day_slots = []
        slots_to_remove = []
        for slot in day['slots']:
            new_day_date_time = pytz.utc.localize(datetime.strptime(slot['utc_datetime'], '%c'))
            if new_day_date_time.strftime('%a, %b %d') != day['datetime'].strftime('%a, %b %d'):
                new_day_slots.append(slot)
                slots_to_remove.append(slot)
        for slot in slots_to_remove:
            day['slots'].remove(slot)
        if new_day_slots:
            timeslot_days.append({'datetime': new_day_date_time, 'label': new_day_date_time.strftime('%a, %b %d'),
                                  'slots': new_day_slots})
    return timeslot_days

def process_accept_timeslot(appt_request,user,timeslot,comments):
    global requestor, recipient, requestor, recipient, requestor, recipient
    appt_req_user = get_object_or_404(User, userprofile=appt_request.user_profile)
    appt_start_date_time = datetime.strptime(timeslot, '%c')
    appt_start_date_time_tz_aware = pytz.utc.localize(appt_start_date_time)
    appt_end_date_time = appt_start_date_time + timedelta(minutes=appt_request.length)
    appt_end_date_time_tz_aware = pytz.utc.localize(appt_end_date_time)

    appt = Appt(user_profile=appt_request.user_profile,
                recipient_user_profile=user.profile,
                appt_request=appt_request,
                title=appt_request.title,
                comments=comments,
                appt_type=appt_request.appt_type,
                location=appt_request.location,
                start_date_time=appt_start_date_time_tz_aware,
                end_date_time=appt_end_date_time_tz_aware)
    appt.save()

    meeting_desc = "Comments From Recipient:\n%s\n\nOriginal Request:\n%s" % (comments, appt_request.message)
    if appt.appt_type == 'phone':
        location = 'Phone Meeting: %s' % appt.location
    else:
        location = appt.location

    calendar_account = None

    if appt_req_user.profile.get_calendar_account():
        calendar_account = appt_req_user.profile.get_calendar_account()
        requestor = appt_req_user
        recipient = user
    elif user.profile.get_calendar_account():
        calendar_account = user.profile.get_calendar_account()
        requestor = user
        recipient = appt_req_user

    if calendar_account:
        ical_data = False
        if calendar_account.social_auth.provider == Provider.Google:
            google_api = GoogleAPI(requestor, calendar_account.social_auth)
            google_api.create_event(calendar_account, recipient, appt_request.title, meeting_desc, location,
                                    appt_start_date_time_tz_aware, appt_end_date_time_tz_aware)
        elif calendar_account.social_auth.provider == Provider.Microsoft:
            microsoft_api = MicrosoftAPI(requestor, calendar_account.social_auth)
            microsoft_api.create_event(calendar_account, recipient, appt_request.title, meeting_desc, location,
                                       appt_start_date_time_tz_aware, appt_end_date_time_tz_aware)
        elif calendar_account.social_auth.provider == Provider.Exchange:
            exchange_api = ExchangeAPI(requestor, calendar_account.social_auth)
            exchange_api.create_event(calendar_account, recipient, appt_request.title, meeting_desc, location,
                                       appt_start_date_time_tz_aware, appt_end_date_time_tz_aware)
    else:
        cal = Calendar()
        cal.add('prodid', '-//Bizintro//bizintro.com//EN')
        cal.add('version', '2.0')

        event = Event()
        event.add('summary', appt_request.title)
        event.add('description', meeting_desc)
        event.add('location', location)
        event.add('dtstamp', appt_start_date_time_tz_aware)
        event.add('dtstart', appt_start_date_time_tz_aware)
        event.add('dtend', appt_end_date_time_tz_aware)
        event['uid'] = 'cal-%d@bizintro.com' % appt.id

        cal.add_component(event)
        ical_data = cal.to_ical()

    subject = appt.title
    zone = tz.gettz(appt_req_user.profile.timezone)
    loc = appt_start_date_time_tz_aware.astimezone(zone)
    body = "Congratulations, you've setup an appointment for %s!<p><div style='font-style:italic;'>%s</div><p>" % (loc.strftime("%A, %B %d at %I:%M %p %Z"), meeting_desc)
    if ical_data:
        body += "You will need to open the .ics file attached to this email in your local calendar program or import it into your online calendar if you wish to actually have it on your schedule."
    if send_email(subject, body, "Bizintro", [appt_req_user.email,user.email], "reply@bizintro.com", ical_data):
        appt_request.status = "c"
        appt_request.save()

        link = 'http://' + settings.DOMAIN + '/appointments/' + str(appt.uuid)
        create_activity("You have created an appointment with %s." % appt.user_profile.full_name(), link,
                        user.profile)
        create_activity("%s has created an appointment with you." % user.profile.full_name(), link,
                        appt.user_profile)
        return True
    else:
        return False
