from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from bizintro.helpers import bizintro_pro_required
from django.dispatch import receiver

from djstripe.signals import WEBHOOK_SIGNALS
import logging

logger = logging.getLogger(__name__)


@receiver(WEBHOOK_SIGNALS['invoice.payment_succeeded'])
def invoice_paid(sender, **kwargs):
    logger.info('paid')

@receiver(WEBHOOK_SIGNALS['invoice.payment_failed'])
def invoice_failed(sender, **kwargs):
    logger.info('failed')