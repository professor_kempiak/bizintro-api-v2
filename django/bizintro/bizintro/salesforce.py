import logging

import requests
from django.db import IntegrityError, DataError
from django.conf import settings

from bizintro.helpers import SocialAPI, combine_contacts, combine_contacts_async
from bizintro.models import ImportContact


class SalesforceAPI(SocialAPI):
    logger = logging.getLogger(__name__)

    def __init__(self, *args, **kwargs):
        super(SalesforceAPI, self).__init__(*args, **kwargs)
        self.instance_url = self.social_auth.extra_data.get('instance_url', 'https://na1.salesforce.com')

    def _get_headers(self, extra_headers=None):
        headers = {'Authorization': 'Bearer ' + self.social_auth.extra_data.get('access_token'), 
                   'Accept': 'application/json',
                   'User-Agent' : 'Bizintro/1.0'}
        if extra_headers is not None:
            headers.update(extra_headers)
        return headers

    def _get_refresh_headers(self): 
        headers = {'refresh_token': self.social_auth.extra_data.get('refresh_token'),
                   'grant_type': 'refresh_token',
                   'client_id' : settings.SOCIAL_AUTH_SALESFORCE_OAUTH2_KEY,
                   'client_secret' : settings.SOCIAL_AUTH_SALESFORCE_OAUTH2_SECRET, 
                   'format' : 'json'
                  }
        return headers

    def get(self, url, params=None, extra_headers=None):
        headers = self._get_headers(extra_headers)
        r = requests.get(url, headers=headers, params=params)
          
        return r.json()

    def refresh_token(self):
        refresh_headers = self._get_refresh_headers()
        r = requests.post('https://login.salesforce.com/services/oauth2/token', params=refresh_headers)
        my_json=r.json()
        self.social_auth.extra_data["access_token"] = my_json["access_token"]
        self.social_auth.save() 

    def post(self, url, json_data=None, extra_headers=None):
        headers = self._get_headers(extra_headers)
        r = requests.post(url, headers=headers, json=json_data)
        return r  # not sure if we need a return value for this or not

    def get_contacts(self, async=True):
        url = self.instance_url + '/services/data/v20.0/query'
        fields = 'id,firstname,lastname,mailingstreet,mailingcity,mailingstate,mailingpostalcode,title,email,phone'
        ownerid = self.social_auth.uid.rsplit('/', 1)[-1]
        params = {'q': "SELECT %s from Contact WHERE OwnerId = '%s'" % (fields,ownerid)}

        r = self.get(url, params=params)
        try:
            if r[0]["errorCode"]=="INVALID_SESSION_ID":
                self.refresh_token()
                r = self.get(url, params=params)
        except:
            pass

        if not r:
            return 0

        contacts_list = r['records']  # salesforce has a max of 2000 contacts per query
        # TODO: this paging portion takes a long time (~20 seconds to pull 23k contacts)
        # while True:
        #     if r['done']:
        #         break
        #     url = self.instance_url + r['nextRecordsUrl']
        #     r = self.get(url, params=params)
        #     if not r:
        #         break
        #     contacts_list += r['records']

        source = ImportContact.SALESFORCE
        user_profile = self.user.profile

        bulk_create_contacts = []  # new Contact objects will be added to this list to be inserted to the DB in bulk
        salesforce_contacts_query = ImportContact.objects.filter(user_profile=user_profile, source=source)
        salesforce_contacts_by_id = dict((contact.external_id, contact) for contact in salesforce_contacts_query)

        for sf_contact in contacts_list:
            first_name = sf_contact.get('FirstName') or ''
            last_name = sf_contact.get('LastName') or ''
            email = sf_contact.get('Email') or ''
            title = sf_contact.get('Title') or ''
            phone = sf_contact.get('Phone') or ''
            street = sf_contact.get('MailingStreet') or ''
            city = sf_contact.get('MailingCity') or ''
            state = sf_contact.get('MailingState') or ''
            postal_code = sf_contact.get('MailingPostalCode') or ''
            external_id = sf_contact.get('Id') or ''
            company = ''

            if "reply.linkedin.com" in email:
                continue

            try:
                address = '%s %s %s, %s' % (street, city, state, postal_code)
                address = address[:200]  # TODO: should we make that field bigger in the DB?
            except Exception:
                address = ''

            if external_id in salesforce_contacts_by_id:
                contact = salesforce_contacts_by_id[external_id]
                created = False
            else:
                contact = ImportContact(user_profile=user_profile, source=source,external_id=external_id)
                created = True

            contact.first_name = first_name
            contact.last_name = last_name
            contact.data={"first_name":first_name,"last_name":last_name,"emails":[email],"phone_numbers":[phone],"addresses":[address],"title":title,"company":company}

            contact.auth_source = self.social_auth

            if created:
                # if this contact did not exist in DB yet, we can insert it in bulk at the end
                bulk_create_contacts.append(contact)
            elif contact.is_dirty(check_relationship=True):
                # only save to DB if existing object's fields were modified
                contact.save()
        # now insert Contacts to DB in bulk for maximum efficiency
        if len(bulk_create_contacts):
            try:
                ImportContact.objects.bulk_create(bulk_create_contacts)
            except (DataError, IntegrityError):
                # if there's an error saving in bulk, we'll have to do it the slow way: save each Contact individually
                for contact in bulk_create_contacts:
                    try:
                        contact.save()
                    except Exception as e:
                        self.log_exception('Could not save Contact to DB')

        if async:
            combine_contacts_async(self.user.profile.id, source)
        else:
            combine_contacts(self.user.profile.id, source)

        return len(bulk_create_contacts)

