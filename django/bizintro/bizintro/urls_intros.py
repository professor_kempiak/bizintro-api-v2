from django.conf.urls import url
from bizintro.apis import introduction

urlpatterns = (
    url(r'^list$', introduction.IntroductionListView.as_view(), name='all_intros'),
    url(r'^manage$', introduction.IntroductionManageView.as_view(), name='manage_intros'),
    url(r'^accept$', introduction.IntroductionAcceptView.as_view(), name='introduction'),
    url(r'^nudge$', introduction.IntroductionNudgeView.as_view(), name='nudge_introduction'),
    url(r'^$', introduction.IntroductionView.as_view(), name='create_introduction'),
)
