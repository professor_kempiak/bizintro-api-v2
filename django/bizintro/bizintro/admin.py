from django.contrib import admin

from bizintro.models import (Activity, Appt, ApptRequest, Contact, CredentialsModel, Help, Introduction,
                             PotentialIntroduction, TimeSlot, UserProfile, License)


# Register your models here.
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone_number', 'address', 'timezone')
admin.site.register(UserProfile, UserProfileAdmin)


class CredentialAdmin(admin.ModelAdmin):
    list_display = ('auth', 'credential')
admin.site.register(CredentialsModel, CredentialAdmin)


class ContactAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'first_name', 'last_name', 'address', 'email', 'phone_number', 'occupation')
admin.site.register(Contact, ContactAdmin)


class IntroductionAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'contact_a', 'contact_b', 'message', 'created_date_time', 'archived')
admin.site.register(Introduction, IntroductionAdmin)


class HelpAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'contact', 'recipient_user_profile', 'message', 'created_date_time', 'status', 'archived')
admin.site.register(Help, HelpAdmin)


class PotentialIntroductionAdmin(admin.ModelAdmin):
    list_display = ('help', 'contact', 'introduction')
admin.site.register(PotentialIntroduction, PotentialIntroductionAdmin)


class ApptRequestAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'contact', 'recipient_user_profile', 'introduction', 'title', 'message', 'created_date_time', 'length', 'status', 'appt_type', 'location', 'archived')
admin.site.register(ApptRequest, ApptRequestAdmin)


class TimeSlotAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'appt_request', 'start_date_time', 'end_date_time')
admin.site.register(TimeSlot, TimeSlotAdmin)


class ApptAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'recipient_user_profile', 'appt_request', 'title', 'comments', 'created_date_time', 'start_date_time', 'end_date_time', 'appt_type', 'location', 'archived')
admin.site.register(Appt, ApptAdmin)


class ActivityAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'created_date_time', 'message', 'link', 'archived')
admin.site.register(Activity, ActivityAdmin)


class LicenseAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'type', 'start_date_time', 'end_date_time', 'revoked')
admin.site.register(License, LicenseAdmin)
