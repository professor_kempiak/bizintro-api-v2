from datetime import datetime
import json

from bizintro.helpers import (Provider, create_activity, list_user_contact_sources, onboarding_required, get_user_contacts, send_email, send_email_with_button,
                              send_event_to_intercom, sync_contacts_async, combine_contacts, is_valid_email, is_valid_phone_number, make_introduction, finish_signup,
                               combine_contacts_async, merge_manual_edits, get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.models import ApptRequest, ContactShare, Help, Introduction
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponse
from intercom.client import Client
from rest_framework.authtoken.models import Token
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView


class PreSignupView(APIView):
    parser_classes = (JSONParser,)
    def post(self, request, format=None):
        email = request.data.get("email")
        first_name = request.data.get("first_name","")
        last_name = request.data.get("last_name","")

        intercom = Client(personal_access_token=settings.INTERCOM_API_KEY)
        try:
            intercom_user = intercom.users.find(email=email)
        except:
            intercom_user = intercom.users.create(email=email, name="%s %s" % (first_name,last_name))

        intercom_user.custom_attributes["bizintro_user"] = False
        intercom.users.save(intercom_user)
  
        return HttpResponse(
                    json.dumps({'message': "Success"}),
                    content_type="application/json"
                )

class AutoSignupView(APIView):
    parser_classes = (JSONParser,)
    def post(self, request, format=None):
        the_uuid = request.data.get("uuid")
        type = request.data.get("type")
        
        user = None
        new = False

        if type == "appt_request":
            appt = ApptRequest.objects.get(uuid=the_uuid)
            if appt.recipient_user_profile == None:
                user = auto_create_user(appt.contact.first_name,appt.contact.last_name,appt.recipient_email)
                new = True
            else:
                user = appt.recipient_user_profile.user 
        elif type == "contactshare":
            contactshare = ContactShare.objects.get(uuid=the_uuid)
            if contactshare.recipient_user_profile == None:
                user = auto_create_user(contactshare.contact.first_name,contactshare.contact.last_name,contactshare.recipient_email)
                new = True
            else:
                user = contactshare.recipient_user_profile.user 
        elif type == "help":
            help = Help.objects.get(uuid=the_uuid)
            if help.recipient_user_profile == None:
                user = auto_create_user(help.contact.first_name,help.contact.last_name,help.recipient_email)
                new = True
            else:
                user = help.recipient_user_profile.user 
        elif type == "introduction":
            introduction = Introduction.objects.get(uuid=the_uuid)
            if introduction.recipient_user_profile_a == None:
                user = auto_create_user(introduction.contact_a.first_name,introduction.contact_a.last_name,introduction.recipient_a_email)
                new = True
            else:
                user = introduction.recipient_user_profile_a.user
        
        if user is not None:
            if user.is_active:
                token = Token.objects.get(user=user)
                return HttpResponse(
                    json.dumps({'token': token.key, 'message': "Success", "new": new, "email": user.email, "first_name": user.first_name, "lastname": user.last_name}),
                    content_type="application/json"
                )
            else:
                return HttpResponse(
                    json.dumps({'token': "", 'message': "User is not active."}),
                    content_type="application/json"
                )
        else:
            return HttpResponse(
                json.dumps({'token': "", 'message': "Something went wrong. This may not be a real UUID for this type of object."}),
                content_type="application/json"
            )
        

class LoginView(APIView):
    parser_classes = (JSONParser,)
    def post(self, request, format=None):
        user = authenticate(username=request.data['username'], password=request.data['password'])
        if user is not None:
            if user.is_active:
                sync_contacts_async(user.profile.id,"all")
                token = Token.objects.get(user=user)
                return HttpResponse(
                    json.dumps({'token': token.key, 'message': "Success"}),
                    content_type="application/json"
                )
            else:
                return HttpResponse(
                    json.dumps({'token': "", 'message': "User is not active."}),
                    content_type="application/json"
                )
        else:
            return HttpResponse(
                json.dumps({'token': "", 'message': "The username and password were incorrect."}),
                content_type="application/json"
            )

class SignupView(APIView):
    parser_classes = (JSONParser,)
    def post(self, request, format=None):
        
        if User.objects.filter(email=request.data['email']).exists():
            return HttpResponse(
                json.dumps({'token': "", 'message': 'An account with that email already exists!'}),
                content_type="application/json"
            )
        errors = []
        if not is_valid_email(request.data.get("email","")):
            errors.append("Please enter a valid email address.")
        if request.data.get("first_name","") == "":
            errors.append("First Name is required.")
        if request.data.get("last_name","") == "":
            errors.append("Last Name is required.")
        if request.data.get("password","") == "":
            errors.append("Password is required.")

        if len(errors) > 0:
            return HttpResponse(
                json.dumps({'token': "", 'errors': errors}),
                content_type="application/json"
            )
        user = User.objects.create_user(username=request.data['email'], email=request.data['email'],
                                        password=request.data['password'], first_name=request.data['first_name'],
                                        last_name=request.data['last_name'])


        if user is not None:
            if user.emailaddress_set.count() > 0:
                addr = user.emailaddress_set.all()[0]
                addr.verified_at = datetime.now()
                addr.save(verify=False)
                # TODO - Re-enable verification
                #addr.send_verification()

            token = Token.objects.create(user=user)
            
            finish_signup(user, request.data["timezone"])
            
            user = authenticate(username=request.data['email'], password=request.data['password'])
            
            return HttpResponse(
                json.dumps({'token': token.key, 'message': "Success"}),
                content_type="application/json"
            )

        else:
            return HttpResponse(
                json.dumps({'token': "", 'message': "An error occurred."}),
                content_type="application/json"
            )    
            
def auto_create_user(first_name,last_name,email):
    user = User.objects.create_user(username=email, email=email, password="", first_name=first_name, last_name=last_name)
    if user.emailaddress_set.count() > 0:
        addr = user.emailaddress_set.all()[0]
        addr.verified_at = datetime.now()
        addr.save(verify=False)

    token = Token.objects.create(user=user)

    finish_signup(user, "America/Chicago")

    return user 
