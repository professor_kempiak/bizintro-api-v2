import json

from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button, list_user_contact_sources, nudge,
                              send_event_to_intercom, combine_contacts, onboarding_required, is_valid_email, is_valid_phone_number, make_introduction, 
                              combine_contacts_async, merge_manual_edits, get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.models import Appt, ApptRequest, Contact, Help, ImportContact, Introduction, PotentialIntroduction
from django.core import serializers
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView


class IntroductionView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        user_profile = request.user.profile
        contact_a_id = request.data.get('contact_a_id')
        contact_b_id = request.data.get('contact_b_id')
        recipient_a_email = request.data.get('recipient_a_email')
        recipient_b_email = request.data.get('recipient_b_email')
        include_vcf_a = False
        include_vcf_b = False
        message = request.data.get('message')
        message2 = request.data.get('message2')
        potential_intro_uuid = request.data.get('potential_intro_uuid',None)
        introducer_email = request.user.email
        
        obj = make_introduction(user_profile, contact_a_id, contact_b_id, recipient_a_email, recipient_b_email,
                                message, message2, potential_intro_uuid, introducer_email, include_vcf_a, include_vcf_b)
        return_message = ""
        if obj['message'] == 'success':
            return_message = 'Introduction made!'
        elif obj['message'] == 'failure':
            return_message = 'Introduction failed!'
        elif obj['message'] == 'help_success':
            return_message = 'Introductions made!'
            
        if potential_intro_uuid:
            help_id = PotentialIntroduction.objects.get(uuid=potential_intro_uuid).help_id

            if PotentialIntroduction.objects.filter(help=help_id, status="o").count() == 0:
                help = Help.objects.get(id=help_id) 
                help.status="c"
                help.save()
        send_event_to_intercom("Made Introduction", request.user)
        
        return HttpResponse(
            json.dumps({'message': return_message}),
            content_type="application/json"
        )

class IntroductionAcceptView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        global import_contact
        intro_uuid = request.data['intro_uuid']
        introduction = get_object_or_404(Introduction, uuid=intro_uuid)

        if request.user.profile == introduction.user_profile:
            return HttpResponse(
                json.dumps({'status': "error", 'message': "This is not your introduction."}),
                content_type="application/json"
            )
        try:
            appt_request = ApptRequest.objects.get(introduction=introduction)
            return HttpResponse(
                json.dumps({'status': "error", "appt_request_uuid":str(appt_request.uuid), 'message': "Introduction already accepted."}),
                content_type="application/json"
            )
        except:
            try:
                original_contact_id = request.data['contact_id']
                original_contact = Contact.objects.get(pk=original_contact_id)
                if introduction.contact_a == original_contact:
                    intro_contact = introduction.contact_a
                    contact_email = introduction.recipient_a_email
                elif introduction.contact_b == original_contact:
                    intro_contact = introduction.contact_b
                    contact_email = introduction.recipient_b_email
                else:
                    return HttpResponse(
                        json.dumps({'status': "error", 'message': "This is not your introduction."}),
                        content_type="application/json"
                    )
                introduction.status='c'
                introduction.save()
                try:
                    contact = Contact.objects.get(email=contact_email, user_profile=request.user.profile)
                    contact_id = contact.id
                except:
                    import_contact = ImportContact(user_profile=request.user.profile, source='d')

                    occupation = intro_contact.occupation.split(' at ')

                    import_contact.data = {
                      'first_name': intro_contact.first_name,
                      'last_name': intro_contact.last_name,
                      'title': occupation[0],
                      'company': occupation[1] if len(occupation) > 1 else '',
                      'emails': [contact_email],
                      'phone_numbers': [intro_contact.phone_number],
                      'addresses': [intro_contact.address],
                    }
                    import_contact.save()
                    combine_contacts(request.user.profile.id, 'd', imp_contact=import_contact)

                    contact = Contact.objects.get(email=contact_email, user_profile=request.user.profile)
                    contact_id = contact.id

                return HttpResponse(
                    json.dumps({'status': "complete", "intro_uuid": str(introduction.uuid), "contact_id": contact_id, 'message': "Accepted - send them to request appointment."}),
                        content_type="application/json"
                    )       
            except:
                return HttpResponse(
                    json.dumps({'status': "complete", "intro_uuid": str(introduction.uuid), 'message': "Accepted - send them to request appointment."}),
                    content_type="application/json"
                )

class IntroductionListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        introductions = Introduction.objects.filter(Q(user_profile=request.user.profile) | Q(recipient_user_profile_a=request.user.profile) | Q(recipient_user_profile_b=request.user.profile))
        json_data = serializers.serialize('json', introductions)

        return HttpResponse(
            json_data,
            content_type="application/json"
        )

class IntroductionManageView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
        response_arr = []
        introductions = Introduction.objects.filter(user_profile=request.user.profile)
        for introduction in introductions:
            intro_obj = {}
            intro_obj["id"] = introduction.id
            intro_obj["uuid"] = str(introduction.uuid)
            intro_obj["date_made"] = introduction.created_date_time.strftime('%Y-%m-%dT%H:%M:%S')
            intro_obj["status"] = introduction.status
            intro_obj["contact_a"] = introduction.contact_a.as_json()
            intro_obj["contact_b"] = introduction.contact_b.as_json()

            nudge = introduction.nudges.last() 
            if nudge:
                intro_obj["date_last_nudged"] = nudge.date_created.strftime('%Y-%m-%dT%H:%M:%S')
                intro_obj["number_of_times_nudged"] = introduction.nudges.count()
            
            appt_request = ApptRequest.objects.filter(introduction=introduction).first()
                
            if appt_request:
                appt_obj = {}
                appt_obj["id"] = appt_request.id
                appt_obj["uuid"] = str(appt_request.uuid)
                appt_obj["status"] = appt_request.status
                appt_obj["type"] = appt_request.appt_type
                appt_obj["date_made"] = appt_request.created_date_time.strftime('%Y-%m-%dT%H:%M:%S')
                intro_obj["date_accepted"] = appt_request.created_date_time.strftime('%Y-%m-%dT%H:%M:%S')

                appt = Appt.objects.filter(appt_request=appt_request).first()
                
                if appt:
                    appt_obj["date_accepted"] = appt.created_date_time.strftime('%Y-%m-%dT%H:%M:%S')
                
                intro_obj["appointment_details"] = appt_obj
            
            response_arr.append(intro_obj)

        return HttpResponse(
            json.dumps(response_arr),
            content_type="application/json"
        )


class IntroductionNudgeView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        intro_uuid = request.data['intro_uuid']
        introduction = get_object_or_404(Introduction, uuid=intro_uuid)

        if request.user.profile != introduction.user_profile:
            return HttpResponse(
                json.dumps({'status': "error", 'message': "This is not your introduction."}),
                content_type="application/json"
            )
        else:
            nudge(introduction,"introduction",introduction.recipient_a_email)
            return HttpResponse(
                json.dumps({'status': "success", 'message': "Nudged!"}),
                content_type="application/json"
            )
