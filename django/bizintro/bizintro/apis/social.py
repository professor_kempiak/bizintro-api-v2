from rest_social_auth.views import SocialTokenUserAuthView
from serializers import MyUserTokenSerializer

  class MySocialView(SocialTokenUserAuthView):
      serializer_class = MyUserTokenSerializer
