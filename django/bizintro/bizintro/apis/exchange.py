import json

from bizintro.exchange import autodiscover_exchange_server, test_credentials
from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button, list_user_contact_sources,
                              send_event_to_intercom,combine_contacts, onboarding_required, 
                              is_valid_email, is_valid_phone_number, make_introduction, combine_contacts_async, merge_manual_edits, 
                              get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.helpers import send_event_to_intercom
from bizintro.models import ImportContact
from django.http import HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
import simplejson as json

class TestCredentialsView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        username = request.data.get("username")
        password = request.data.get("password")
        server = request.data.get("server")
        if not server:
            server = autodiscover_exchange_server(username=username, password=password)
            
            if not server:
                return HttpResponse(
                    json.dumps({'success':False,'message':'Invalid credentials, or could not find server!'}),
                        content_type="application/json"
                )
        
        if test_credentials(username=username, password=password, server=server):
            return HttpResponse(
                json.dumps({'success':True,'server':server,'message':'Valid credentials!'}),
                    content_type="application/json"
            )
        else:
            return HttpResponse(
                json.dumps({'success':False,'message':'Invalid credentials!'}),
                    content_type="application/json"
            )

class AutoDiscoverView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        username = request.data.get("username")
        password = request.data.get("password")
        server = autodiscover_exchange_server(username=username, password=password)

        if server:
            return HttpResponse(
                json.dumps({'success': True, 'server': server, 'message': 'Found exchange server!'}),
                    content_type="application/json"
            )
        else:
            return HttpResponse(
                json.dumps({'success': False, 'message':'Could not find server!'}),
                    content_type="application/json"
            )



        
class ExchangeView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        user = request.user
        try:
            username = request.data.get("username")
            password = request.data.get("password")
            server = request.data.get("server")
            
            save_exchange_account_to_db(user=user, username=username, password=password, server=server)

            return HttpResponse(
                            json.dumps({'status': "success",
                                        'message': "Account Saved!'"}),
                            content_type="application/json"
                        )
        except Exception:
            return HttpResponse(
                            json.dumps({'status': "failure",
                                        'message': "An error has occurred."}),
                            content_type="application/json"
                        )

    def put(self, request, contact_id, format=None):
        user = request.user
        try:
            username = request.data.get("username")
            password = request.data.get("password")
            server = request.data.get("server")

            save_exchange_account_to_db(user=user, username=username, password=password, server=server)

            return HttpResponse(
                            json.dumps({'status': "success",
                                        'message': "Account Saved!'"}),
                            content_type="application/json"
                        )
        except Exception:
            return HttpResponse(
                            json.dumps({'status': "failure",
                                        'message': "An error has occurred."}),
                            content_type="application/json"
                        )


    def delete(self, request, contact_id, format=None):
        try:
            social_auth = UserSocialAuth.objects.get(user_id=request.user, id=exchange_id)

            ImportContact.objects.filter(user_profile=request.user.profile, auth_source_id=exchange_id).delete()
            CalendarAccount.objects.filter(user_profile=request.user.profile, social_auth=social_auth).delete()

            xchange_id = social_auth.extra_data['exchange_id']
            exchange_account = ExchangeAccount.objects.get(id=xchange_id)

            exchange_account.delete()
            social_auth.delete()

            return HttpResponse(
                            json.dumps({'status': "success",
                                        'message': "Account Deleted!'"}),
                            content_type="application/json"
                        )
        except Exception:
            return HttpResponse(
                            json.dumps({'status': "failure",
                                        'message': "An error has occurred."}),
                            content_type="application/json"
                        )