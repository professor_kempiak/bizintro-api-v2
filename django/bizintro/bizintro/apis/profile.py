from datetime import timedelta, datetime
import json

from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button, list_user_contact_sources,
                              send_event_to_intercom, combine_contacts, onboarding_required, 
                              is_valid_email, is_valid_phone_number, make_introduction, combine_contacts_async, merge_manual_edits, 
                              get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.helpers import is_valid_email, send_event_to_intercom
from bizintro.models import Contact, ImportContact, Tour
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from multimail.models import EmailAddress
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
import simplejson as json
import unicodecsv


class TourListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        tours = Tour.objects.filter(user_profile=request.user.profile)
        json_data = serializers.serialize('json', tours)

        return HttpResponse(
            json_data,
            content_type="application/json"
        )

class TourViewedView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        try:
            tour_key = request.data.get("tour_key")
            tour = Tour.objects.update_or_create(user_profile=request.user.profile, tour_key=tour_key)

            return HttpResponse(
                json.dumps({ 'success': True, 'message': "Success"}),
                content_type="application/json"
             )
        except:
            return HttpResponse(
                json.dumps({ 'success': False, 'message': 'Failed'}),
                content_type="application/json"
           )

class EmailListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        content = request.user.emailaddress_set.order_by('-is_primary', 'email')
        return Response(serializers.serialize('json', content))

class EmailMakePrimaryView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        try:
            email = EmailAddress.objects.get(user=request.user, pk=request.POST.get("id"))
            email.set_primary()
            
            return HttpResponse(
                json.dumps({ 'success': True, 'message': "Success"}),
                content_type="application/json"
            )
        except:
            return HttpResponse(
                json.dumps({ 'success': False, 'message': 'Failed'}),
                content_type="application/json"
           )

class EmailView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        try:
            new_email = request.data.get("email")
            if not is_valid_email(new_email):
                return HttpResponse(
                    json.dumps({ 'success': False, 'message': 'Email address is invalid'}),
                    content_type="application/json"
                )
                
            if EmailAddress.objects.filter(email__iexact=new_email).count() > 0:
                return HttpResponse(
                    json.dumps({ 'success': False, 'message': 'Email already exists'}),
                    content_type="application/json"
                )
            
            email = EmailAddress.objects.create(email=new_email, user=request.user)
            
            return HttpResponse(
                json.dumps({ 'success': True, 'message': "Success" }),
                content_type="application/json"
            )
        except Exception as e:
            return HttpResponse(
                json.dumps({ 'success': False, 'message': 'Failed %s' % e }),
                content_type="application/json"
            )

    def delete(self, request, contact_id, format=None):
        try:
            email = EmailAddress.objects.get(user=request.user, pk=request.POST.get("id"))
            if not email.is_primary:
                email.delete()
            else:
                return HttpResponse(
                    json.dumps({ 'success': False, 'message': "Failed - can't delete primary" }),
                    content_type="application/json"
                )
            
            return HttpResponse(
                json.dumps({ 'success': True, 'message': "Success" }),
                content_type="application/json"
            )
        except Exception:
            return HttpResponse(
                json.dumps({ 'success': False, 'message': 'Failed' }),
                content_type="application/json"
            )

class ChangePasswordView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        user = request.user
        password1 = request.data.get('password1')
        password2 = request.data.get('password2')

        if password1 == password2:
            user.set_password(password1)            
            user.save()

            return HttpResponse(
                json.dumps({ 'success': True, 'message': "Success"}),
                content_type="application/json"
            )
        else:
            return HttpResponse(
                json.dumps({ 'success': False, 'message': "Failed - Passwords don't match"}),
                content_type="application/json"
            )

class ProfileView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def get(self, request, format=None):
        user = request.user

        return HttpResponse(
            json.dumps({ 'profile': {
                'first_name': user.first_name,
                'last_name': user.last_name,
                'phone_number': user.profile.phone_number,
                'address': user.profile.address,
                'title': user.profile.title,
                'company': user.profile.company,
                'summary': user.profile.summary,
                'linkedin_url': user.profile.linkedin_url,
                'avatar_link': user.profile.avatar_link(),
                'timezone': user.profile.timezone,
                'signature': user.profile.signature}
            }),
            content_type="application/json"
        )

    def put(self, request, format=None):
        user = request.user
        user_profile = user.profile

        user.first_name = request.data.get('first_name', user.first_name)
        user.last_name = request.data.get('last_name', user.last_name)
        user_profile.address = request.data.get('address', user_profile.address)
        user_profile.title = request.data.get('title', user_profile.title)
        user_profile.company = request.data.get('company', user_profile.company)
        user_profile.summary = request.data.get('summary', user_profile.summary)
        user_profile.linkedin_url = request.data.get('linkedin_url', user_profile.linkedin_url)
        user_profile.timezone = request.data.get('timezone', user_profile.timezone)
        user_profile.signature = request.data.get('signature', user_profile.signature)
        if is_valid_phone_number(request.data.get('phone_number')):
            user_profile.phone_number = request.data.get('phone_number')

        user.save()
        user_profile.save()

        return HttpResponse(
            json.dumps({ 'success': True, 'message': "Success"}),
            content_type="application/json"
        )

class StandingAvailabilityView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        output_events = []

        standing_times = request.user.profile.standing_meeting_times

        if standing_times:
            standing_times = standing_times.split(',')

            for slot in standing_times:
                event = slot.split('_')
                output_events.append({ 'day': event[0], 'start': event[1], 'end': event[2] })

        return HttpResponse(
            json.dumps(output_events),
            content_type="application/json"
        )

    def put(self, request, format=None):
        try:
            input_events = json.loads(request.body)

            output_events = []

            for event in input_events:
                output_events.append('%s_%s_%s' % (event['day'], event['start'], event['end']))

            user_profile = request.user.profile
            user_profile.standing_meeting_times = ','.join(output_events)
            user_profile.save()

            return HttpResponse(
                json.dumps({ 'success': True, 'message': "Success"}),
                content_type="application/json"
            )
        except:
            return HttpResponse(
                json.dumps({ 'success': False, 'message': 'Failed'}),
                content_type="application/json"
           )
