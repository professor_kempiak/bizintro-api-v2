import json

from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button, list_user_contact_sources,
                              send_event_to_intercom, combine_contacts, onboarding_required, is_valid_email, is_valid_phone_number, make_introduction, 
                              combine_contacts_async, merge_manual_edits, get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts, 
                              fill_default_template, send_event_to_intercom)
from bizintro.models import Contact, ImportContact
from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from multimail.models import EmailAddress
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
import simplejson as json
import unicodecsv


class TemplateView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def get(self, request, format=None):
        user_profile = request.user.profile

        template_type = request.GET.get('type')

        if template_type == 'intro':
            message = user_profile.introTemplate
        elif template_type == 'intro_secondary':
            message = user_profile.introSecondaryTemplate
        elif template_type == 'help':
            message = user_profile.helpTemplate
        elif template_type == 'appt_request':
            message = user_profile.apptTemplate

        return HttpResponse(
            json.dumps({'template': message}),
            content_type="application/json"
        )

    def put(self, request, format=None):
        try:
            user_profile = request.user.profile

            template_type = request.data.get('type')
            message = request.data.get('message')

            if template_type == 'intro':
                user_profile.introTemplate = message
            elif template_type == 'intro_secondary':
                user_profile.introSecondaryTemplate = message
            elif template_type == 'help':
                user_profile.helpTemplate = message
            elif template_type == 'appt_request':
                user_profile.apptTemplate = message
            else:
                raise Exception

            user_profile.save()

            return HttpResponse(
                json.dumps({ 'success': True, 'message': "Success"}),
                content_type="application/json"
            )
        except:
            return HttpResponse(
                json.dumps({ 'success': False, 'message': 'Failed'}),
                content_type="application/json"
           )

class TemplateListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        user_profile = request.user.profile

        return HttpResponse(
            json.dumps({'templates': {
                'intro': user_profile.introTemplate,
                'intro_secondary': user_profile.introSecondaryTemplate,
                'help': user_profile.helpTemplate,
                'appt_request': user_profile.apptTemplate
            }}),
            content_type="application/json"
        )
