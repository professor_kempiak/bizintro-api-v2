import json

from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button, list_user_contact_sources,
                              send_event_to_intercom,combine_contacts, onboarding_required,send_email_with_two_buttons,get_cached_help_contacts, 
                              is_valid_email, is_valid_phone_number, make_introduction, combine_contacts_async, merge_manual_edits, 
                              get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.models import Contact, Help, PotentialIntroduction
from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import serializers
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView


class HelpListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        helps = Help.objects.filter(Q(user_profile=request.user.profile) | Q(recipient_user_profile=request.user.profile))
        json_data = serializers.serialize('json', helps)

        return HttpResponse(
            json_data,
            content_type="application/json"
        )

class HelpContactsView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        json_data = get_cached_help_contacts(request.user.profile.id, request.GET.get("help_uuid", ""))

        return HttpResponse(
            json.dumps(json_data),
            content_type="application/json"
        )
                
class HelpIntroductionDeclineView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        try:
            intro = PotentialIntroduction.objects.get(uuid=request.data['potential_intro_uuid'], help__user_profile=request.user.profile)
            intro.status = 'd'
            intro.save() 
            
            help_id = intro.help.id
     
            intros = PotentialIntroduction.objects.filter(help=help_id, status="o")
            if len(intros) == 0:
                help = Help.objects.get(id=help_id)
                help.status="c"
                help.save()
                return HttpResponse(
                    json.dumps({ 'message': 'Success', 'closed':True}),
                    content_type="application/json"
                )
            else: 
                return HttpResponse(
                    json.dumps({ 'message': 'Success'}),
                    content_type="application/json"
                )
        except:
            return HttpResponse(
                json.dumps({ 'message': 'Failed'}),
                content_type="application/json"
            )

class HelpView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def get(self, request, help_uuid, format=None):
        help = get_object_or_404(Help, uuid=help_uuid)

        if request.user.profile != help.user_profile and request.user.profile != help.recipient_user_profile:
            return HttpResponse(
                json.dumps({'status': "error",
                            'message': "not your offer of help"}),
                content_type="application/json"
            )
        else:
            potential_intros = PotentialIntroduction.objects.filter(help=help)
            return HttpResponse(
                json.dumps({'help': {
                    "uuid": help_uuid,
                    "message": help.message,
                    "created_date_time": help.created_date_time.isoformat(),
                    "status": help.status,
                    "contact_id": help.contact_id,
                    "recipient_email": help.recipient_email,
                    "recipient_profile_id": help.recipient_user_profile.id,
                    "potential_intros": serializers.serialize('json', potential_intros)}
                }),
                content_type="application/json"
            )

    def post(self, request, format=None):
        user_profile = request.user.profile
        if request.data.get('contact_id', False):
            contact = get_object_or_404(Contact, pk=request.data['contact_id'])
            recipient_email = request.data.get('recipient_email', False)
            help_message = request.data.get('message', '').replace("{contact}", contact.full_name()).replace("{contact_first}", contact.first_name)
            help = Help(user_profile=user_profile,
                        contact=contact,
                        recipient_email=recipient_email,
                        message=help_message)
            try:
                recipient_user = User.objects.get(email=contact.email)
                help.recipient_user_profile = recipient_user.profile
            except User.DoesNotExist:
                pass
            help.save()
            subject = "Bizintro Offer of Help from %s" % user_profile.full_name()
            link = 'http://' + settings.DOMAIN + '/accept_help/' + str(help.uuid)
            link_title = "Accept Offer of Help"
            decline_link = 'http://' + settings.DOMAIN + '/helps/decline/' + str(help.uuid.hex)
            decline_link_title = "No Thanks"
            decline_link_class = 'red'
            body = help_message
            create_activity(('You offered to help %s.' % contact.full_name()), link, user_profile)
            if help.recipient_user_profile:
                create_activity("%s has offered to help you." % user_profile.full_name(), link,
                                help.recipient_user_profile)
            if send_email_with_two_buttons(subject, body, link, link_title, decline_link, decline_link_title, decline_link_class, user_profile.full_name(), [recipient_email], request.user.email):
                send_event_to_intercom("Offered Help", request.user)
                return HttpResponse(
                    json.dumps({'message': 'Help successfully offered!'}),
                    content_type="application/json"
                )
            else:
                return HttpResponse(
                    json.dumps({'message': 'Offer of help failed!'}),
                    content_type="application/json"
                )
        else:
            return HttpResponse(
                json.dumps({'message': 'Offer of help failed!'}),
                content_type="application/json"
            )


## login is not required
#def helps_decline(request, help_uuid):
#    help = get_object_or_404(Help, uuid=help_uuid)
#    return render(request, 'helps_decline.html', {'help': help})
