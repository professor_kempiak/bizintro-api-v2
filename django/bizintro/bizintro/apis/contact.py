from itertools import chain
import json
import logging

from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button,
                              send_event_to_intercom, combine_contacts, onboarding_required,list_user_contact_sources,
                              is_valid_email, is_valid_phone_number, make_introduction, combine_contacts_async, merge_manual_edits, 
                              get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.helpers import is_valid_email
from bizintro.models import User, UserProfile, Contact, ImportContact, Help, Introduction, ApptRequest
from django.core import serializers
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from haystack.query import SearchQuerySet
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
import simplejson as json
import unicodecsv

logger = logging.getLogger(__name__)

class ContactSourceListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        content = list_user_contact_sources(request.user)
        return Response(content)

class ContactInteractionView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, contact_id, format=None):
        contact = Contact.objects.get(id=contact_id,user_profile=request.user.profile)
        bizintro_users = User.objects.filter(email=contact.email)
        if len(bizintro_users) == 1:
           bizintro_profile_of_contact = UserProfile.objects.get(user=bizintro_users[0])
           contact_is_bizintro = True
        else:
           contact_is_bizintro = False

        introductions_from_me = Introduction.objects.filter(Q(user_profile=request.user.profile) & (Q(contact_a=contact_id) | Q(contact_b=contact_id)))
        helps_from_me = Help.objects.filter(Q(user_profile=request.user.profile) & Q(contact=contact_id))
        appts_from_me = ApptRequest.objects.filter(Q(user_profile=request.user.profile) & Q(contact=contact_id))

        if contact_is_bizintro:
            introductions_to_me = Introduction.objects.filter(Q(user_profile=bizintro_profile_of_contact) & (Q(recipient_a=request.user.profile) | Q(recipient_b=request.user.profile)))
            helps_to_me = Help.objects.filter(Q(user_profile=bizintro_profile_of_contact) & Q(recipient=request.user.profile))
            appts_to_me = ApptRequest.objects.filter(Q(user_profile=bizintro_profile_of_contact) & Q(recipient=request.user.profile))

            intro_list = list(chain(introductions_to_me, introductions_from_me))
            help_list = list(chain(helps_to_me, helps_from_me))
            appt_list = list(chain(appts_to_me, appts_from_me))

            intro_json_data = serializers.serialize('json', intro_list)
            help_json_data = serializers.serialize('json', help_list)
            appt_json_data = serializers.serialize('json', appt_list)
        else:
            intro_json_data = serializers.serialize('json', introductions_from_me)
            help_json_data = serializers.serialize('json', helps_from_me)
            appt_json_data = serializers.serialize('json', appts_from_me)
     
        return HttpResponse(
                    #json.dumps({'introductions': intro_json_data, "helps": help_json_data, 'appointments': appt_json_data}),
                    '{"introductions": %s, "helps": %s, "appointments": %s}' % (intro_json_data,help_json_data,appt_json_data),
                    content_type="application/json"
                )

class ContactListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        #json_data = get_cached_contacts(request.user.profile.id)
        #logger.info("User: %s" % request.user.profile)
        page = int(request.GET.get('page',0))
        perpage = int(request.GET.get('perpage',0))

        contact_results = SearchQuerySet().filter(user_profile=request.user.profile.id).order_by('last_name')
        results = []
        if page == 0 or perpage == 0:
            for ob in contact_results:
                results.append(ob.object.as_json())
        else:
            start = (page-1) * perpage
            end = ((page-1) * perpage) + perpage
            logger.info("start == %s" % start)
            logger.info("end == %s" % end)
            for ob in contact_results[start:end]:
                results.append(ob.object.as_json())

        return HttpResponse(
            json.dumps(results),
            content_type="application/json"
        )

class ContactSearchView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, q, format=None):
        contact_results = SearchQuerySet().filter(user_profile=request.user.profile.id,content__contains=q).order_by('last_name')
        results = [ob.object.as_json() for ob in contact_results]
        #results = [ob.get_additional_fields() for ob in contact_results]

        return HttpResponse(
            json.dumps(results),
            content_type="application/json"
        )

class ContactImportView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        user_profile = request.user.profile
        try:
            upload_file = request.FILES['file']
            csv_file = unicodecsv.reader(upload_file, encoding='ISO-8859-1')
            source = request.POST["source"]
            header = csv_file.next()
            first = header.index('FirstName')
            last = header.index('LastName')
            email = header.index('EmailAddress')
            company = header.index('Company')
            title = header.index('Position')

            matched_count = 0
            new_count = 0
            for row in csv_file:
                if row:
                    try:
                        contact = ImportContact.objects.filter(data__contains=row[email]).filter(user_profile=user_profile).filter(source=source)[0]
                        matched_count += 1
                        contact.data = {"first_name": row[first], "last_name": row[last], "emails": [row[email]],
                                        "phone_numbers": [], "addresses": [], "title": row[title],
                                        "company": row[company]}
                        contact.save()
                    except:
                        contact = ImportContact(user_profile=user_profile,
                                                source=source,
                                                data={"first_name": row[first], "last_name": row[last],
                                                      "emails": [row[email]], "phone_numbers": [], "addresses": [],
                                                      "title": row[title], "company": row[company]})
                        contact.save()
                        new_count += 1
            combine_contacts_async(user_profile.id, source)
            send_event_to_intercom("Imported Contacts", request.user)
            message = "Successfully updated %d contacts and created %d new contacts" % (matched_count, new_count)
            return HttpResponse(json.dumps({'success': True, 'message': message}))
        except ValueError:
            error = 'Sorry! It looks like your contacts file is malformed. Please try exporting again from LinkedIn.'
        except Exception as e:
            error = 'Sorry! Something went wrong: %s' % e
        return HttpResponse(json.dumps({'success': False, 'error': error}))
        
class ContactView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        phone_numbers = request.data.get('phone_numbers')
        primary_phone_number = request.data.get('primary_phone_number')
        emails = request.data.get('emails')
        primary_email = request.data.get('primary_email')
        addresses = request.data.get('addresses')
        primary_address = request.data.get('primary_address')
        try:
            contact = Contact.objects.filter(user_profile=request.user.profile, emails__contains=emails)[:1]
            try:
                contact_id = contact[0].id
                message = "A contact with that email already exists!"
                
                return HttpResponse(
                    json.dumps({'contact_id': False,
                                'message': message}),
                    content_type="application/json"
                )
            except:
                for phone_number in phone_numbers:
                    if not is_valid_phone_number(phone_number):
                        message = "Phone number is invalid!"
                        return HttpResponse(
                            json.dumps({'contact_id': False,
                                        'message': message}),
                            content_type="application/json"
                        )
                for email in emails:
                    if not is_valid_email(email):
                        message = "Email address is invalid!"
                        return HttpResponse(
                            json.dumps({'contact_id': False,
                                        'message': message}),
                            content_type="application/json"
                        )

                import_contact = ImportContact(user_profile=request.user.profile,
                                               source='m')
                import_contact.data = {
                  'first_name': request.data['first_name'],
                  'last_name': request.data['last_name'],
                  'emails': emails,
                  'phone_numbers': phone_numbers,
                  'addresses': addresses,
                  'title': request.data['title'],
                  'company': request.data['company'],
                }
                import_contact.save()
                combine_contacts(request.user.profile.id, 'm', imp_contact=import_contact)
                contact = Contact.objects.filter(emails__contains=emails, user_profile=request.user.profile)[0]
                if primary_email != '':
                    contact.email = primary_email
                if primary_phone_number != '':
                    contact.phone_number = primary_phone_number
                if primary_address != '':
                    contact.address = primary_address
                contact.save()
                send_event_to_intercom("Created Contact", request.user)
        except Exception:
            message = "Failed to add contact"
            return HttpResponse(
                json.dumps({'contact_id': False,
                            'message': message}),
                content_type="application/json"
            )

        contact = Contact.objects.get(email=primary_email, user_profile=request.user.profile)
        contact_id = contact.id
        message = "Contact added!"
        return HttpResponse(
                json.dumps({'contact_id': contact_id,
                            'message': message}),
                content_type="application/json"
            )

    def put(self, request, contact_id, format=None):
        contact = get_object_or_404(Contact, pk=contact_id, user_profile=request.user.profile)
        
        phone_numbers = request.data.get('phone_numbers')
        primary_phone_number = request.data.get('primary_phone_number')
        emails = request.data.get('emails')
        primary_email = request.data.get('primary_email')
        addresses = request.data.get('addresses')
        primary_address = request.data.get('primary_address')
        
        try:
            saved_contact = Contact.objects.filter(user_profile=request.user.profile, emails__contains=emails)[:1]
            try:
                contact_id = saved_contact[0].id
                if contact_id != contact.id:
                    return HttpResponse(
                        json.dumps({}),
                        content_type="application/json"
                    )
                else:
                    raise Exception
            except:
                for phone_number in phone_numbers:
                    if not is_valid_phone_number(phone_number):
                        return HttpResponse(
                            json.dumps({}),
                            content_type="application/json"
                        )
                for email in emails:
                    if not is_valid_email(email):
                        return HttpResponse(
                            json.dumps({}),
                            content_type="application/json"
                        )

                try:
                    import_contact = ImportContact.objects.get(user_profile=request.user.profile, source='m', contact_id=contact.id)
                except ImportContact.DoesNotExist:
                    import_contact = ImportContact(user_profile=request.user.profile, source='m')
                import_contact.data = {
                  'first_name': request.data['first_name'],
                  'last_name': request.data['last_name'],
                  'emails': emails,
                  'phone_numbers': phone_numbers,
                  'addresses': addresses,
                  'title': request.data['title'],
                  'company': request.data['company'],
                }
                import_contact.save()
                combine_contacts(request.user.profile.id, 'm',imp_contact=import_contact,original_contact=contact)
                contact = Contact.objects.filter(emails__contains=emails, user_profile=request.user.profile)[0]
                contact = merge_manual_edits(request.user.profile, contact)
                if primary_email != '':
                    contact.email = primary_email
                if primary_phone_number != '':
                    contact.phone_number = primary_phone_number
                if primary_address != '':
                    contact.address = primary_address
                contact.save()
        except Exception:
            return HttpResponse(
                json.dumps({ 'success': False, 'message': 'Failed' }),
                content_type="application/json"
            )

        return HttpResponse(
                json.dumps({ 'success': True, 'message': 'Success' }),
                content_type="application/json"
            )

    def delete(self, request, contact_id, format=None):
        qs = Contact.objects.filter(user_profile=request.user.profile)
        contact = get_object_or_404(qs, pk=contact_id)
        contact.delete()
        return HttpResponse(
            json.dumps({}),
            content_type="application/json"
        )

    def get(self, request, contact_id, format=None):
        qs = Contact.objects.filter(user_profile=request.user.profile)
        contact = get_object_or_404(qs, pk=contact_id)

        return HttpResponse(
            json.dumps({'contact': {
                "email": contact.email,
                "full_name": contact.full_name(),
                "last_name": contact.last_name,
                "first_name": contact.first_name,
                "sources": contact.sources(),
                "avatar": contact.avatar_link(80),
                "occupation": contact.occupation,
                "address": contact.address,
                "phone_number": contact.phone_number,
                "emails": contact.emails,
                "occupations": contact.occupations,
                "addresses": contact.addresses,
                "phone_numbers": contact.phone_numbers,

                "id": contact.id,
                "display": "%s - %s" % (contact.full_name(), contact.email)}
            }),
            content_type="application/json"
        )
