from datetime import datetime, timedelta
import json
import time

from bizintro.exchange import ExchangeAPI
from bizintro.google import GoogleAPI
from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button,
                              send_event_to_intercom, combine_contacts, onboarding_required, list_user_contact_sources,
                              is_valid_email, is_valid_phone_number, make_introduction, combine_contacts_async, merge_manual_edits, 
                              get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.helpers import send_event_to_intercom
from bizintro.microsoft import MicrosoftAPI
from django.http import HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView


class AvailabilityView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        data = ''
        output_events = []
        for social_auth in request.user.social_auth.filter(provider__in=Provider.Calendars):
            starttime = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.strptime(request.GET["start"] + " 00:00:00", "%Y-%m-%d %H:%M:%S"))
            endtime = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.strptime(request.GET["end"] + " 23:59:59", "%Y-%m-%d %H:%M:%S"))
            if social_auth.provider == Provider.Google:
                google_api = GoogleAPI(request.user, social_auth)
                output_events += google_api.get_availability(starttime, endtime)
            elif social_auth.provider == Provider.Microsoft:
                microsoft_api = MicrosoftAPI(request.user, social_auth)
                output_events += microsoft_api.get_availability(starttime, endtime)
            elif social_auth.provider == Provider.Exchange:
                exchange_api = ExchangeAPI(request.user, social_auth)
                output_events += exchange_api.get_availability(starttime, endtime)

        standing_times = request.user.profile.standing_meeting_times
        if standing_times:
            standing_times = standing_times.split(',')
        
        # we'll take the passed in start date, and apply the standing meeting times to the next 7 days
        firstDay = datetime.strptime(request.GET["start"] + " 00:00:00", "%Y-%m-%d %H:%M:%S")
        dayOffset = int(datetime.strftime(firstDay, "%w")) # if today is tuesday, offset should = 2
            
        for slot in standing_times:
            event = slot.split('_')
            days = (int(event[0]) - dayOffset) % 7
            dateFormat = datetime.strftime(firstDay + timedelta(days=days), '%Y-%m-%d')
            dateFormat += 'T%s:00'

            temp_event = {
                        'title': "Unavailable",
                        'start': dateFormat % event[1],
                        'end': dateFormat % event[2],
                        'editable': False,
                        'className': 'standingTime',
                        'overlap': True
                    }

            output_events.append(temp_event)

        data = json.dumps(output_events)

        if output_events:
            data = json.dumps(output_events)
        return HttpResponse(data)