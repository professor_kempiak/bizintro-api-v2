from datetime import datetime
import json
import logging

from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button, list_user_contact_sources,
                              send_event_to_intercom, combine_contacts, onboarding_required, is_valid_email, is_valid_phone_number, make_introduction, 
                              combine_contacts_async, merge_manual_edits, get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.models import AsyncJob, FullContactData
from django.http import HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

logger = logging.getLogger(__name__)

class JobStatusView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        jobs = AsyncJob.objects.filter(user_profile_id=request.user.profile.id,acknowledged=False)
        return_obj = []
        for job in jobs:
            if job.status != AsyncJob.PROGRESS:
               job.acknowledged=True
               job.save()
            return_obj.append({'status':job.status,'message':job.message})

        return HttpResponse(json.dumps(return_obj))
