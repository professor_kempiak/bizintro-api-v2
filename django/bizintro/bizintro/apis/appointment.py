from datetime import datetime, timedelta
import json
import logging

from bizintro.google import GoogleAPI
from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button, list_user_contact_sources,
                              send_event_to_intercom, combine_contacts, onboarding_required, is_valid_email, is_valid_phone_number, make_introduction, 
                              combine_contacts_async, merge_manual_edits, get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.helpers import send_event_to_intercom
from bizintro.models import ApptRequest, Contact, Introduction, Appt, TimeSlot
from dateutil import tz
from django.conf import settings
from django.contrib.auth.models import User
from django.core import serializers
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from icalendar import Calendar, Event
from pytz import timezone
import pytz
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

logger = logging.getLogger(__name__)

class ApptRequestAcceptView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        appt_req_uuid = request.data.get("uuid","")
        timeslot = request.data.get("timeslot","")
     
        appt_request = get_object_or_404(ApptRequest, uuid=appt_req_uuid)

        if request.user.profile != appt_request.recipient_user_profile:
            return HttpResponse(
                json.dumps({'status': "error", 'message': settings.NOT_YOUR_OBJECT_MESSAGE % 'Appointment Request'}),
                content_type="application/json"
            )

        if appt_request.status == "c":
            return HttpResponse(
                json.dumps({'status': "error", 'message': "This Appointment Request has already been processed."}),
                content_type="application/json"
            )

        if process_accept_timeslot(appt_request, request.user, timeslot, ""):
            return HttpResponse(
                json.dumps({'status': "success", 'message': "Appointment Scheduled!"}),
                content_type="application/json"
            )

class ApptReRequestView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        appt_req_uuid = request.data.get("uuid","")

        appt_request = get_object_or_404(ApptRequest, uuid=appt_req_uuid)

        if request.user.profile != appt_request.recipient_user_profile:
            return HttpResponse(
                json.dumps({'status': "error", 'message': settings.NOT_YOUR_OBJECT_MESSAGE % 'Appointment Request'}),
                content_type="application/json"
            )

        if appt_request.status in "cdft":
            return HttpResponse(
                json.dumps({'status': "error", 'message': "This Appointment Request has already been processed."}),
                content_type="application/json"
            )

        appt_request.status = "t"
        appt_request.save()

        contacts = Contact.objects.filter(user_profile=request.user.profile, emails__contains=[appt_request.user_profile.user.email])

        return HttpResponse(
            json.dumps({'status': "success", 'message': "Success", 'contact_id': contacts[0].id, 'email': contacts[0].email}),
            content_type="application/json"
        )

class ApptRequestListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        apptRequests = ApptRequest.objects.filter(Q(user_profile=request.user.profile) | Q(recipient_user_profile=request.user.profile))
        json_data = serializers.serialize('json', apptRequests)

        return HttpResponse(
            json_data,
            content_type="application/json"
        )

class ApptRequestCreateView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        user_profile = request.user.profile
        if request.data.get('contact_id', False) and request.data.get('timeslots_available', False):
            contact = get_object_or_404(Contact, pk=request.data['contact_id'])
            recipient_email = request.data.get('recipient_email', False)
            appt_length = int(request.data.get('appt_length', 30))
            intro_uuid = request.data.get('intro_uuid', '')
            appt_type = request.data.get('appt_type', '')
            location = request.data.get('location', '')
            appt_title = request.data.get('title', '')
            appt_type_indicator = "in person"
    
            if appt_type == "phone":
                appt_type_indicator = "via phone"
            elif appt_type == "online":
                appt_type_indicator = "via online meeting"
    
            appt_message = request.data.get('message', '').replace("{contact}", contact.full_name()).replace("{contact_first}", contact.first_name).replace(
                "{location}", appt_type_indicator)

            appt_request = ApptRequest(user_profile=user_profile,
                                       contact=contact,
                                       recipient_email=recipient_email,
                                       length=appt_length,
                                       title=appt_title,
                                       message=appt_message,
                                       appt_type=appt_type,
                                       location=location)
            try:
                recipient_user = User.objects.get(email=contact.email)
                appt_request.recipient_user_profile = recipient_user.profile
            except User.DoesNotExist:
                pass
            if intro_uuid:
                try:
                    introduction = Introduction.objects.get(uuid=intro_uuid)
                    introduction.status='c'
                    introduction.save()
                    appt_request.introduction = introduction
                except Introduction.DoesNotExist:
                    pass
            appt_request.save()
            link = 'https://' + settings.FRONTENDDOMAIN + '/appointments/requests/' + str(appt_request.uuid)
            redo_link = 'https://' + settings.FRONTENDDOMAIN + '/rerequest_appt/' + str(appt_request.uuid)
            
            appt_message += """
            <b>Appointment Details:</b><br>
            Type: %s<br>
            Location: %s<br>
            Length: %s mins<br><br>
            <b>Available Dates/Times: (timezone is currently %s)</b><br>
            """ % (
            appt_request.appt_type, appt_request.location, appt_request.length, appt_request.user_profile.timezone)

            timeslots_available = request.data['timeslots_available'].split(',')
            for slot in timeslots_available:
                times = slot.split('_')
                start_date_time = datetime.strptime(times[0], "%a %b %d %Y %H:%M:%S") # GMT+0000")
                start_date_time_tz_aware = pytz.timezone(user_profile.timezone).localize(start_date_time).astimezone(pytz.utc)
                end_date_time = datetime.strptime(times[1], "%a %b %d %Y %H:%M:%S") # GMT+0000")
                end_date_time_tz_aware = pytz.timezone(user_profile.timezone).localize(end_date_time).astimezone(pytz.utc)
                logger.info(start_date_time)
                logger.info(start_date_time_tz_aware)
                logger.info(end_date_time)
                logger.info(end_date_time_tz_aware)
                timeslot = TimeSlot(user_profile=user_profile,
                                    appt_request=appt_request,
                                    start_date_time=start_date_time_tz_aware,
                                    end_date_time=end_date_time_tz_aware)
                #                    start_date_time=start_date_time,
                #                    end_date_time=end_date_time)
                timeslot.save()

            timeslot_days = get_timeslot_days(appt_request)

            counter = 0
            for day in timeslot_days:
                if counter == 0:
                    appt_message = appt_message + '<table class="btn-green" cellpadding="0" cellspacing="5" border="0"><tr><th>' + day["label"] + '</th><th></th></tr>'
                for slot in day["slots"]:
                    counter += 1
                    if counter % 2 == 1:
                        appt_message += """
                        <tr>
                            <td>
                                <a href="%s">%s</a>
                            </td>
                        """ % ('https://' + settings.FRONTENDDOMAIN + "/accept_timeslot/" + str(appt_request.uuid) + "/" + str(
                            slot["utc_datetime"]),
                               slot["datetime"].astimezone(timezone(appt_request.user_profile.timezone)).strftime(
                                   '%I:%M %P'))
                    else:
                        appt_message += """
                            <td>
                                <a href="%s">%s</a>
                            </td>
                        </tr>
                        """ % ('https://' + settings.FRONTENDDOMAIN + "/accept_timeslot/" + str(appt_request.uuid) + "/" + str(
                            slot["utc_datetime"]),
                               slot["datetime"].astimezone(timezone(appt_request.user_profile.timezone)).strftime(
                                   '%I:%M %P'))
                if counter % 2 == 1:
                    appt_message += "</tr>"
                appt_message += "</table>"
                counter = 0

            subject = "Appointment Request from %s" % user_profile.full_name()
            
            if send_email_with_button(subject, appt_message, redo_link, "Suggest Different Times", user_profile.full_name(), [recipient_email], request.user.email):
                return HttpResponse(
                    json.dumps({'status': "success", 'message': "Appointment request made."}),
                    content_type="application/json"
                )
            else:
                return HttpResponse(
                    json.dumps({'status': "error", 'message': "Appointment request failed."}),
                    content_type="application/json"
                )
        else:
            return HttpResponse(
                json.dumps({'status': "error", 'message': "Appointment request failed."}),
                content_type="application/json"
            )

def get_timeslot_days(appt_request):
    timeslot_days = []
    timeslots = TimeSlot.objects.filter(appt_request=appt_request)
    for timeslot in timeslots:  # create a "timeslot_day" for each TimeSlot object
        timeslot_start_date_time = timeslot.start_date_time
        timeslot_day = {'datetime': timeslot_start_date_time,
                        'label': timeslot_start_date_time.strftime('%a, %b %d'), 'slots': []}
        while True:  # create a list of slots from each TimeSlot every 30 minutes
            if timeslot_start_date_time >= timeslot.end_date_time:
                break
            timeslot_day['slots'].append(
                {'utc_datetime': timeslot_start_date_time.strftime('%c'), 'datetime': timeslot_start_date_time})
            timeslot_start_date_time += timedelta(minutes=30)
        same_day = False
        for day in timeslot_days:  # combine "timeslot_day"s if they're the same day
            if day['datetime'].strftime('%a, %b %d') == timeslot_day['datetime'].strftime('%a, %b %d'):
                day['slots'] += timeslot_day['slots']
                day['slots'] = sorted(day['slots'], key=lambda slot: slot['datetime'])
                same_day = True
                break
        if not same_day:
            timeslot_days.append(timeslot_day)
    for day in timeslot_days:  # split overnight "timeslot_day"s into multiple "timeslot_day"s
        new_day_slots = []
        slots_to_remove = []
        for slot in day['slots']:
            new_day_date_time = pytz.utc.localize(datetime.strptime(slot['utc_datetime'], '%c'))
            if new_day_date_time.strftime('%a, %b %d') != day['datetime'].strftime('%a, %b %d'):
                new_day_slots.append(slot)
                slots_to_remove.append(slot)
        for slot in slots_to_remove:
            day['slots'].remove(slot)
        if new_day_slots:
            timeslot_days.append({'datetime': new_day_date_time, 'label': new_day_date_time.strftime('%a, %b %d'),
                                  'slots': new_day_slots})
    logger.info("slots: %s" % timeslot_days[0]["slots"]) 
    return timeslot_days
def process_accept_timeslot(appt_request,user,timeslot,comments):
    global requestor, recipient, requestor, recipient, requestor, recipient
    appt_req_user = get_object_or_404(User, userprofile=appt_request.user_profile)
    appt_start_date_time = datetime.strptime(timeslot, '%c')
    appt_start_date_time_tz_aware = pytz.utc.localize(appt_start_date_time)
    appt_end_date_time = appt_start_date_time + timedelta(minutes=appt_request.length)
    appt_end_date_time_tz_aware = pytz.utc.localize(appt_end_date_time)

    appt = Appt(user_profile=appt_request.user_profile,
                recipient_user_profile=user.profile,
                appt_request=appt_request,
                title=appt_request.title,
                comments=comments,
                appt_type=appt_request.appt_type,
                location=appt_request.location,
                start_date_time=appt_start_date_time_tz_aware,
                end_date_time=appt_end_date_time_tz_aware)
    appt.save()

    meeting_desc = "Comments From Recipient:\n%s\n\nOriginal Request:\n%s" % (comments, appt_request.message)
    if appt.appt_type == 'phone':
        location = 'Phone Meeting: %s' % appt.location
    else:
        location = appt.location

    calendar_account = None

    if appt_req_user.profile.get_calendar_account():
        calendar_account = appt_req_user.profile.get_calendar_account()
        requestor = appt_req_user
        recipient = user
    elif user.profile.get_calendar_account():
        calendar_account = user.profile.get_calendar_account()
        requestor = user
        recipient = appt_req_user

    if calendar_account:
        ical_data = False
        if calendar_account.social_auth.provider == Provider.Google:
            google_api = GoogleAPI(requestor, calendar_account.social_auth)
            google_api.create_event(calendar_account, recipient, appt_request.title, meeting_desc, location,
                                    appt_start_date_time_tz_aware, appt_end_date_time_tz_aware)
        elif calendar_account.social_auth.provider == Provider.Microsoft:
            microsoft_api = MicrosoftAPI(requestor, calendar_account.social_auth)
            microsoft_api.create_event(calendar_account, recipient, appt_request.title, meeting_desc, location,
                                       appt_start_date_time_tz_aware, appt_end_date_time_tz_aware)
        elif calendar_account.social_auth.provider == Provider.Exchange:
            exchange_api = ExchangeAPI(requestor, calendar_account.social_auth)
            exchange_api.create_event(calendar_account, recipient, appt_request.title, meeting_desc, location,
                                       appt_start_date_time_tz_aware, appt_end_date_time_tz_aware)
    else:
        cal = Calendar()
        cal.add('prodid', '-//Bizintro//bizintro.com//EN')
        cal.add('version', '2.0')

        event = Event()
        event.add('summary', appt_request.title)
        event.add('description', meeting_desc)
        event.add('location', location)
        event.add('dtstamp', appt_start_date_time_tz_aware)
        event.add('dtstart', appt_start_date_time_tz_aware)
        event.add('dtend', appt_end_date_time_tz_aware)
        event['uid'] = 'cal-%d@bizintro.com' % appt.id

        cal.add_component(event)
        ical_data = cal.to_ical()

    subject = appt.title
    zone = tz.gettz(appt_req_user.profile.timezone)
    loc = appt_start_date_time_tz_aware.astimezone(zone)
    body = "Congratulations, you've setup an appointment for %s!<p><div style='font-style:italic;'>%s</div><p>" % (loc.strftime("%A, %B %d at %I:%M %p %Z"), meeting_desc)
    if ical_data:
        body += "You will need to open the .ics file attached to this email in your local calendar program or import it into your online calendar if you wish to actually have it on your schedule."
    if send_email(subject, body, "Bizintro", [appt_req_user.email,user.email], "reply@bizintro.com", ical_data):
        appt_request.status = "c"
        appt_request.save()

        link = 'http://' + settings.DOMAIN + '/appointments/' + str(appt.uuid)
        create_activity("You have created an appointment with %s." % appt.user_profile.full_name(), link,
                        user.profile)
        create_activity("%s has created an appointment with you." % user.profile.full_name(), link,
                        appt.user_profile)
        return True
    else:
        return False
