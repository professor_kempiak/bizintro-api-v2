from django.conf.urls import url
from bizintro.apis import profile

urlpatterns = (
    url(r'^tour/list$', profile.TourListView.as_view(), name='listtours'),
    url(r'^tour/set_as_viewed$', profile.TourViewedView.as_view(), name='tourviewed'),
    url(r'^changepassword$', profile.ChangePasswordView.as_view(), name='changepassword'),
    url(r'^email/list$', profile.EmailListView.as_view(), name='listemails'),
    url(r'^email/makeprimary$', profile.EmailMakePrimaryView.as_view(), name='makeprimary'),
    url(r'^email$', profile.EmailView.as_view(), name='email'),
    url(r'^email/(?P<email_id>[0-9]+)$', profile.EmailView.as_view(), name='email'),
    url(r'^standing_availability/$', profile.StandingAvailabilityView.as_view(), name='standingavailability'),
    url(r'^$', profile.ProfileView.as_view(), name='profile'),
)
