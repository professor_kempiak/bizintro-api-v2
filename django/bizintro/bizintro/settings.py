"""
Django settings for bizintro project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DOMAIN = 'DOMAINTOREPLACE'
FRONTENDDOMAIN = 'FRONTENDTOREPLACE'

MAX_FREE_OBJECTS=20

SERVER_EMAIL='reply@bizintro.com'
ADMIN_EMAIL='reply@bizintro.com'

ADMINS = (('Mike','mike@nortoncrew.com'),)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'bizintro'
EMAIL_HOST_PASSWORD = '!n8PSA$WJm'
EMAIL_USE_TLS = True

SITE_ID=1

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'KNgJZ0YF3gneQ0Phzc4sGy5af15oWNBISquCEsjnVCYKJ5ogaV'

# SECURITY WARNING: don't run with debug turned on in production!
#DEBUG = True

#TEMPLATE_DEBUG = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR,'templates/'),],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
INTERCOM_KEY="INTERCOM_KEY_REPLACE"
INTERCOM_API_KEY='INTERCOM_API_KEY_REPLACE'

ALLOWED_HOSTS = ['*']

TEMPLATE_DIRS = (
    'bizintro/templates',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)

TEMPLATE_LOADERS = (
 'django.template.loaders.filesystem.Loader',
 'django.template.loaders.app_directories.Loader',
 'django.template.loaders.eggs.Loader',
)

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'bizintro',
    'corsheaders',
    'debug_toolbar',
    'jsonify',
    #'social.apps.django_app.default',
    'social_django',
    'djcelery',
    'djstripe',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_social_auth',
    #'rest_framework_swagger',
    'rest_framework_docs',
    'multimail',
    'django_pdb',
    'haystack',
)

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'bizintro',
    },
}

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'bizintro.middleware.TimezoneMiddleware',
    'bizintro.middleware.SocialAuthRedirectMiddleware',
    'rollbar.contrib.django.middleware.RollbarNotifierMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'social_core.backends.linkedin.LinkedinOAuth2',
    'social_core.backends.yahoo.YahooOAuth2',
    'social_core.backends.salesforce.SalesforceOAuth2',
    'social_core.backends.google.GoogleOAuth2',
    'bizintro.microsoft.MicrosoftOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_REDIRECT_URL = '/'

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'django',
        'USER': 'django',
        'PASSWORD': '0zMT6P4F6b',
        'HOST': 'localhost',
        'PORT': '',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

DEFAULT_HELP_TEMPLATE="Hi {contact_first},<br><br>I hope you are doing well.<br><br>Would it be helpful to you if I were able to make some meaningful introductions?<br><br>I found a tool (Bizintro) that automates the process of making introductions.  When you click on the link below it will take you to my network, and will allow you to easily & discreetly look through it.  You can search by position, title, or company.<br><br>I know a lot of good people (like yourself) and I am happy to be able to connect you.<br><br>Thanks,<br><br>{user}<br><br>p.s. Bizintro makes introductions in a fraction of the time, and allows you to easily manage them"
DEFAULT_INTRO_TEMPLATE="Hi {primary_contact_first},<br><br>I hope you are doing well.<br><br>I wanted to make an introduction to {secondary_contact} (see attached V-card).  I am using Bizintro, an automated introduction tool.  When you click on the link below it will allow you to pick the type of meeting you would like (Phone, In-person, On-line, etc...) as well as the ability to pick a time and a date, and to customize your message to {secondary_contact_first}. All of this information gets populated in an email (very cool).  I will also be sending an email to {secondary_contact_first} to give a heads up that you will be reaching out.<br><br>Let me know if you need any other information.<br><br>Thanks,<br><br>{user}<br><br>p.s. Bizintro makes introductions in a fraction of the time, and allows you to easily manage them."
DEFAULT_INTRO_SECONDARY_TEMPLATE="Hi {secondary_contact_first},<br><br>I hope you are doing well.<br><br>I wanted to let you know that I have recently had a discussion with {primary_contact} (see attached V-card) regarding you, and I think it would be mutually beneficial for you both to meet.  {primary_contact_first} will be reaching out using Bizintro, an automated introduction tool that allows him to schedule dates and times right in the email itself.  You will get an email with suggested dates and if these do not work you will be able to offer some alternates.<br><br>Let me know if you need any other information.<br><br>Thanks,<br><br>{user}<br><br>p.s. Bizintro makes introductions in a fraction of the time, and allows you to easily manage them."
DEFAULT_APPT_TEMPLATE="Dear {contact_first},<br><br>I hope this finds you well.<br><br>Can we schedule a time to get together {location}?  You can click one of my available times here if those times work for you or suggest a different time if not.<br><br>Thanks,<br><br>{user}"
DEFAULT_CONTACT_SHARE_TEMPLATE="Dear {recipient_contact_first},<br><br>I hope you are doing well.<br><br>I wanted to share a valued contact of mine, {shared_contact}, with you.<br><br>Please click on this link to accept it.<br><br>Thanks,<br><p>{user}"


# Python Social Auth
# http://python-social-auth.readthedocs.org/en/latest/configuration/django.html

SOCIAL_AUTH_MICROSOFT_OAUTH2_KEY = 'MSKEY'
SOCIAL_AUTH_MICROSOFT_OAUTH2_SECRET = 'MSSECRET'
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '613941208128.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'wiyZ6kFFvfZ2N1TM_K7FD9RL'
#SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '278687638784-1dc2761h9c2dbljtu6o78bihma7glaie.apps.googleusercontent.com'
#SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = '9qdz7PiSXyN38xX_twdjw5zD'
SOCIAL_AUTH_YAHOO_OAUTH2_KEY = 'dj0yJmk9RHVnTUpsZEZjdmFmJmQ9WVdrOVdtOVhka1pRTnpnbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yNg--'
SOCIAL_AUTH_YAHOO_OAUTH2_SECRET = '5bbeba84b661b32241ae3b34bf4ac1c510fa3b33'
SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = 'td997ugxfrpn'
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = '2Jl23yFv9QhYJEuP'
SOCIAL_AUTH_SALESFORCE_OAUTH2_KEY = '3MVG9KI2HHAq33RziL1zcmLYXqI0kT82juGOu_6KUMPbvUgiMXahGnRknF2RKc7f5YTm8XaLtCa0En0QHoza7'
SOCIAL_AUTH_SALESFORCE_OAUTH2_SECRET = '4657263893700567958'

SOCIAL_AUTH_YAHOO_VERIFY_SSL = False

# Google apparently needs these to get scope right? TODO: verify this
SOCIAL_AUTH_GOOGLE_OAUTH2_IGNORE_DEFAULT_SCOPE = True
SOCIAL_AUTH_GOOGLE_OAUTH2_USE_DEPRECATED_API = True

# request extra scope
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = ['r_basicprofile', 'r_emailaddress']
SOCIAL_AUTH_MICROSOFT_OAUTH2_SCOPE = [ 
    'openid',
    'offline_access',
    'profile',
    'https://outlook.office.com/calendars.read',
    'https://outlook.office.com/contacts.read' 
]
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.google.com/calendar/feeds/',
    'https://www.google.com/m8/feeds',
    'https://www.googleapis.com/auth/userinfo.email',
]

# request extra fields to be returned in response
SOCIAL_AUTH_GOOGLE_OAUTH2_FIELD_SELECTORS = ['email']
SOCIAL_AUTH_LINKEDIN_OAUTH2_FIELD_SELECTORS = ['email-address', 'public-profile-url']
SOCIAL_AUTH_SALESFORCE_OAUTH2_FIELD_SELECTORS = ['email']

# store fields returned in DB `extra_data` column
SOCIAL_AUTH_LINKEDIN_OAUTH2_EXTRA_DATA = [
    ('id', 'id'),
    ('firstName', 'first_name'),
    ('lastName', 'last_name'),
    ('emailAddress', 'email_address'),
    ('publicProfileUrl', 'public_profile_url', True),
]
SOCIAL_AUTH_MICROSOFT_OAUTH2_EXTRA_DATA = [
    ('email', 'preferred_username'),
]
SOCIAL_AUTH_GOOGLE_OAUTH2_EXTRA_DATA = [
    ('refresh_token', 'refresh_token', True),
    ('expires_in', 'expires'),
    ('token_type', 'token_type', True),
    ('email', 'email'),
]
SOCIAL_AUTH_SALESFORCE_OAUTH2_EXTRA_DATA = [
    ('id', 'id'),
    ('instance_url', 'instance_url'),
    ('issued_at', 'issued_at'),
    ('signature', 'signature'),
    ('refresh_token', 'refresh_token'),
    ('email', 'email'),
]

# modify social auth user authentication pipeline, save credentials and contacts
SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'bizintro.pipeline.associate_by_email',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
    'bizintro.pipeline.save_credentials',
    'bizintro.pipeline.save_contacts',
    'bizintro.pipeline.set_extra_data',
    'bizintro.pipeline.add_email_address',
    'bizintro.pipeline.finish_signup_process',
)

# modify social auth user disconnection pipeline, clear credentials and contacts
SOCIAL_AUTH_DISCONNECT_PIPELINE = (
    'social_core.pipeline.disconnect.allowed_to_disconnect',
    'social_core.pipeline.disconnect.get_entries',
    'bizintro.pipeline.clear_credentials',
    'bizintro.pipeline.clear_contacts',
    'bizintro.pipeline.clear_extra_data',
    'social_core.pipeline.disconnect.revoke_tokens',
    'social_core.pipeline.disconnect.disconnect',
)

SOCIAL_AUTH_GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {
    'access_type': 'offline',
    #'approval_prompt': 'force',
    'prompt': 'select_account consent',
}
SOCIAL_AUTH_MICROSOFT_OAUTH2_AUTH_EXTRA_ARGUMENTS = {
    'response_type': 'code',
}

# use the email address for User.username.
USERNAME_IS_FULL_EMAIL = True

# these fields should only be set on initial user creation, not each addition social auth connection
SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['email', 'first_name', 'last_name', 'username']

# redirect URLs
SOCIAL_AUTH_DISCONNECT_REDIRECT_URL = '/accounts/external_accounts/'

SOCIAL_AUTH_FIELDS_STORED_IN_SESSION = ['next', 'timezone']

# needed to make this work with bootstrap labels
from django.contrib.messages import constants as messages
MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}

# stripe subscriptions
STRIPE_PUBLIC_KEY = os.environ.get("STRIPE_PUBLIC_KEY", "pk_test")
STRIPE_SECRET_KEY = os.environ.get("STRIPE_SECRET_KEY", "sk_test")

DJSTRIPE_PLANS = {
    "monthly": {
        "stripe_plan_id": "pro-monthly",
        "name": "Bizintro Pro ($10/month)",
        "description": "The monthly subscription plan to Bizintro",
        "price": 1000,  # $10.00
        "currency": "usd",
        "interval": "month"
    },
    "yearly": {
        "stripe_plan_id": "pro-yearly",
        "name": "Bizintro Pro ($100/year)",
        "description": "The annual subscription plan to Bizintro",
        "price": 10000,  # $100.00
        "currency": "usd",
        "interval": "year"
    }
}

DJSTRIPE_PRORATION_POLICY_FOR_UPGRADES = True
DJSTRIPE_INVOICE_FROM_EMAIL = 'info@bizintro.com'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/django.log',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
        'rest_social_auth': {
            'handlers': ['file', ],
            'level': "DEBUG",
            'propagate': True,
        },
        'social_django': {
            'handlers': ['file', ],
            'level': "DEBUG",
            'propagate': True,
        },
        'bizintro': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

ROLLBAR = {
    'access_token': 'c01ecd94c7eb48fea5f4b2f432635a8f',
    'environment': DOMAIN,
    'branch': 'master',
    'root': '/absolute/path/to/code/root',
}

import djcelery
djcelery.setup_loader()

 
BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

FULL_CONTACT_API_KEY='gbEoAhgD87oqFV1B0lPnJTbGu4Pu6h9C'

NOT_YOUR_OBJECT_MESSAGE='You do not have permission to view that %s - perhaps you need to <a href="https://mike.bizintro.com/accounts/profile/">add another email address</a>?'

CORS_ORIGIN_WHITELIST = (
    'https://dev.bizintro.com'
)
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
