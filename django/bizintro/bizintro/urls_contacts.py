from django.conf.urls import url
from bizintro.apis import contact

urlpatterns = (
    url(r'^source_list$', contact.ContactSourceListView.as_view(), name='contact_source_list'),
    url(r'^interactions/(?P<contact_id>[0-9]+)$', contact.ContactInteractionView.as_view(), name='contact_interaction'),
    url(r'^search/(?P<q>[0-9a-zA-Z\w\W\ ]+)$', contact.ContactSearchView.as_view(), name='contact_search'),
    url(r'^list$', contact.ContactListView.as_view(), name='all_contacts'),
    url(r'^import$', contact.ContactImportView.as_view(), name='import'),
    url(r'^$', contact.ContactView.as_view(), name='contact'),
    url(r'^(?P<contact_id>[0-9]+)$', contact.ContactView.as_view(), name='contact'),
)
