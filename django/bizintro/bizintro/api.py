import json
from rest_framework import serializers
from rest_framework import viewsets
import time
import logging
from datetime import timedelta, datetime
from django.core import serializers
from django.db.models import Q

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from django.views.decorators.csrf import csrf_exempt

from bizintro.google import GoogleAPI
from bizintro.exchange import ExchangeAPI, test_credentials, autodiscover_exchange_server
from bizintro.helpers import (Provider, get_user_contacts, list_user_contact_sources,
                              is_valid_email, is_valid_phone_number, make_introduction, combine_contacts,  combine_contacts_async, merge_manual_edits, get_cached_contacts, get_cached_typeahead_contacts, get_cached_help_contacts)
from bizintro.microsoft import MicrosoftAPI
from bizintro.models import Activity, ApptRequest, Appt, Contact, Help, Introduction, PotentialIntroduction, ContactShare, ImportContact, AsyncJob, FullContactData
from multimail.models import EmailAddress

from django.views.generic import TemplateView
from django.contrib.auth import logout, get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_social_auth.serializers import UserSerializer
from rest_social_auth.views import JWTAuthMixin
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from django.conf.urls import url
import re
import datetime

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import redirect, render

from bizintro.helpers import finish_signup, send_event_to_intercom, is_valid_email, sync_contacts_async
from bizintro.models import ApptRequest, Contact, Help, Introduction, ContactShare
from rest_framework.authtoken.models import Token
from datetime import datetime, timedelta

import pytz
from pytz import timezone
from dateutil import tz
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from icalendar import Calendar, Event

from bizintro.google import GoogleAPI
from bizintro.helpers import (Provider, create_activity, get_user_contacts, send_email, send_email_with_button,
                              send_event_to_intercom,combine_contacts, onboarding_required)
from bizintro.microsoft import MicrosoftAPI
from bizintro.exchange import ExchangeAPI
from bizintro.models import Appt, ApptRequest, Contact, ImportContact, Introduction, TimeSlot


logger = logging.getLogger(__name__)
   

@login_required
def decline_intro(request):
    try:
        intro = PotentialIntroduction.objects.get(uuid=request.GET['potential_intro_uuid'], help__user_profile=request.user.profile)
        intro.status = 'd'
        intro.save() 
        
        help_id = intro.help.id
 
        intros = PotentialIntroduction.objects.filter(help=help_id, status="o")
        if len(intros) == 0:
            help = Help.objects.get(id=help_id)
            help.status="c"
            help.save()
            return HttpResponse(
                json.dumps({ 'message': 'Success', 'closed':True}),
                content_type="application/json"
            )
        else: 
            return HttpResponse(
                json.dumps({ 'message': 'Success'}),
                content_type="application/json"
            )
    except:
        return HttpResponse(
            json.dumps({ 'message': 'Failed'}),
            content_type="application/json"
        )


@login_required
def quick_make_intro(request):
    try:
        user_profile = request.user.profile
        contact_a_id = request.GET.get('ent_id', False)
        contact_b_id = request.GET.get('contact_id', False)
        contact_a = Contact.objects.get(id=contact_a_id)
        contact_b = Contact.objects.get(id=contact_b_id)
        message = user_profile.introTemplate + user_profile.signature
        message = message.replace("{primary_contact}", contact_a.full_name()).replace("{primary_contact_first}", contact_a.first_name).replace("{secondary_contact}", contact_b.full_name()).replace("{secondary_contact_first}", contact_b.first_name)
        message2 = user_profile.introSecondaryTemplate + user_profile.signature
        message2 = message2.replace("{primary_contact}", contact_a.full_name()).replace("{primary_contact_first}", contact_a.first_name).replace("{secondary_contact}", contact_b.full_name()).replace("{secondary_contact_first}", contact_b.first_name)
        potential_uuid = request.GET.get('potential_intro_uuid', False)
        introducer_email = request.user.email
        make_introduction(user_profile, contact_a_id, contact_b_id, None, None,
                                message, message2, potential_uuid, introducer_email)

        intro = PotentialIntroduction.objects.get(uuid=potential_uuid)
        help_id = intro.help_id
        logger.info("HelpID in Quick: %s" % help_id )
        if PotentialIntroduction.objects.filter(help=help_id, status="o").count() == 0:
            logger.info("here")
            help = Help.objects.get(id=help_id) 
            help.status="c"
            help.save()
            return HttpResponse(
                json.dumps({ 'message': 'Success', 'closed':True}),
                content_type="application/json"
            )
        else:
            return HttpResponse(
                json.dumps({ 'message': 'Success'}),
                content_type="application/json"
            )
    except:
        return HttpResponse(
            json.dumps({ 'message': 'Failed'}),
            content_type="application/json"
        )


@login_required
def decline_all(request):
    try:
        help = Help.objects.get(uuid=request.GET.get("help_uuid"))
        
        intros = PotentialIntroduction.objects.filter(help=help, status="o")
        for intro in intros:
            intro.status = 'd'
            intro.save()
        
        help.status="c"
        help.save()

        messages.warning(request, "You have completed your offer of help!")
        
        return HttpResponse(
            json.dumps({ 'message': 'Success'}),
            content_type="application/json"
        )
    except:
        return HttpResponse(
            json.dumps({ 'message': 'Failed'}),
            content_type="application/json"
        )


@login_required
def decline_help(request):
    try:
        help = Help.objects.get(uuid=request.GET.get("help_uuid"))
        help.status="d"
        help.save()
        messages.warning(request, "You have declined %s's offer of help!" % help.user_profile.full_name())
        return HttpResponse(
            json.dumps({ 'message': 'Success'}),
            content_type="application/json"
        )
    except:
        return HttpResponse(
            json.dumps({ 'message': 'Failed'}),
            content_type="application/json"
        )
        
# login not required
def decline_help_offer(request):
    try:
        help = Help.objects.get(uuid=request.GET.get("help_uuid"))
        help.status="d"
        help.save()
        return HttpResponse(
            json.dumps({ 'message': 'Success'}),
            content_type="application/json"
        )
    except:
        return HttpResponse(
            json.dumps({ 'message': 'Failed'}),
            content_type="application/json"
        )


@login_required
def introduce_all(request):
    try:
        user_profile = request.user.profile
        contact_a_id = request.GET.get('ent_id', False)
        message = user_profile.introTemplate + user_profile.signature
        message2 = user_profile.introSecondaryTemplate + user_profile.signature
        introducer_email = request.user.email
        
        help = Help.objects.get(uuid=request.GET.get("help_uuid"))
        
        intros = PotentialIntroduction.objects.filter(help=help, status='o')
        for intro in intros:
          contact_b_id = intro.contact.id
          potential_uuid = intro.uuid
          obj = make_introduction(user_profile, contact_a_id, contact_b_id, None, None, message, potential_uuid, introducer_email)
          obj = make_introduction(user_profile, contact_a_id, contact_b_id, None, None,
                                message, message2, potential_uuid, introducer_email)
        
        help.status="c"
        help.save()
        messages.success(request, "You have completed your offer of help!")
        return HttpResponse(
            json.dumps({ 'message': "Success"}),
            content_type="application/json"
        )
    except:
        return HttpResponse(
            json.dumps({ 'message': 'Failed'}),
            content_type="application/json"
       )


@login_required
def toggle_archive(request):
    if request.method == 'POST':
        obj_type_to_model = {
            "intro": Introduction,
            "help": Help,
            "appt_request": ApptRequest,
            "appt": Appt,
            "activity": Activity,
            "contact_share": ContactShare,
        }
        model = obj_type_to_model.get(request.POST.get('obj_type'))
        if model is not None:
            try:
                obj = model.objects.get(uuid=request.POST.get('obj_uuid'))
                obj.status = request.POST.get('user_type')
                obj.save()
            except model.DoesNotExist:
                return HttpResponse(
                    json.dumps({'success': False}),
                    content_type="application/json"
                )

            return HttpResponse(
                json.dumps({'success': True}),
                content_type="application/json"
            )

    return HttpResponse(
        json.dumps({'success': False}),
        content_type="application/json"
    )

       


