import base64
import json
import logging

import requests

from django.db import IntegrityError

from social.backends.oauth import BaseOAuth2

from bizintro.helpers import SocialAPI, combine_contacts, combine_contacts_async
from bizintro.models import CredentialsModel, ImportContact


class YahooAPI(SocialAPI):
    logger = logging.getLogger(__name__)

    def get_contacts(self, async=True):
        try:
            credentials = CredentialsModel.get_credentials_from_auth(self.social_auth)
       
            headers = {'Authorization': 'Bearer ' + credentials.access_token}

            url = 'https://social.yahooapis.com/v1/user/%s/contacts' % self.social_auth.extra_data.get("id") 
            params = {'format': 'json'} 
            r = requests.get(url, headers=headers, params=params)
            return_val = r.json().get("contacts").get("contact")
        except:
            return_val = {}
        if not r:
            return 0

        source = ImportContact.YAHOO
        user_profile = self.user.profile

        bulk_create_contacts = []  # new Contact objects will be added to this list to be inserted to the DB in bulk
        yahoo_contacts_query = ImportContact.objects.filter(user_profile=user_profile, source=source)
        yahoo_contacts_by_id = dict((contact.external_id, contact) for contact in yahoo_contacts_query)

        for ycontact in return_val:
            first_name = ""
            last_name = ""
            title = ""
            company = ""
            emails = []
            phones = []
            addresses = []
            for field in ycontact.get("fields"):
                if field.get("type") == "email":
                    emails.append(field.get("value"))
                elif field.get("type") == "name":
                    first_name = field.get("value").get("givenName","")
                    last_name = field.get("value").get("familyName","")
                elif field.get("type") == "phone":
                    phones.append(field.get("value"))
                elif field.get("type") == "jobTitle":
                    title = field.get("value")
                elif field.get("type") == "company":
                    company = field.get("value")
                elif field.get("type") == "address":
                    first_name = field.get("value").get("givenName","")
                    address = "%s %s %s, %s" % (field.get("value").get("street",""),field.get("value").get("city",""),field.get("value").get("stateOrProvince",""),field.get("value").get("postalCode",""))
                    addresses.append(address)

            if any("reply.linkedin.com" in s for s in emails):
                continue
            
            if not emails:
                continue

            if not title:
                title = ""
            if not company:
                company = ""
            
            external_id = str(ycontact.get("id",None))

            if external_id in yahoo_contacts_by_id:
                contact = yahoo_contacts_by_id[external_id]
                created = False
            else:
                contact = ImportContact(user_profile=user_profile, source=source, external_id=external_id)
                created = True

            contact.data={"first_name":first_name,"last_name":last_name,"emails":emails,"phone_numbers":phones,"addresses":addresses,"title":title,"company":company}
            contact.auth_source = self.social_auth

            if created:
                # if this contact did not exist in DB yet, we can insert it in bulk at the end
                bulk_create_contacts.append(contact)
            elif contact.is_dirty(check_relationship=True):
                # only save to DB if existing object's fields were modified
                contact.save()
        # now insert Contacts to DB in bulk for maximum efficiency
        if len(bulk_create_contacts):
            try:
                ImportContact.objects.bulk_create(bulk_create_contacts)
            except IntegrityError:
                # if there's an error saving in bulk, we'll have to do it the slow way: save each Contact individually
                for contact in bulk_create_contacts:
                    try:
                        contact.save()
                    except Exception:
                        self.log_exception('Could not save Contact to DB')

        if async:
            combine_contacts_async(self.user.profile.id, source)
        else:
            combine_contacts(self.user.profile.id, source)
        
        return len(bulk_create_contacts)
