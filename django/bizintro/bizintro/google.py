import logging

import httplib2
from bs4 import BeautifulSoup
from django.db import IntegrityError
from gdata.contacts.client import ContactsClient, ContactsQuery

from oauth2client.client import AccessTokenRefreshError

from apiclient.discovery import build
from bizintro.helpers import OAuth2TokenFromCredentials, SocialAPI, combine_contacts, combine_contacts_async
from bizintro.models import CredentialsModel, ImportContact


class GoogleAPI(SocialAPI):
    logger = logging.getLogger(__name__)

    def _get_service(self, service_name):
        credential = CredentialsModel.get_credentials_from_auth(self.social_auth)
        http = httplib2.Http()
        http = credential.authorize(http)
        service = build(service_name, "v3", http=http)
        return service

    def get_availability(self, starttime, endtime):
        try:
            service = self._get_service('calendar')
        except Exception:
            self.log_exception('Google API could not get service')
            return []
        output_events = []

        for calendar in self.get_calendars():
            calendar_id = calendar['id']

            try:
                events = service.events().list(calendarId=calendar_id, timeZone=self.user.profile.timezone,
                                               timeMin=starttime, timeMax=endtime, singleEvents=True).execute()
            except AccessTokenRefreshError:
                # this probably means: "invalid_grant: Token has been revoked."
                self.log_exception('Google API AccessTokenRefreshError')
                return []

            for event in events['items']:
                try:
                    temp_event = {
                        'title': event["summary"],
                        'start': event["start"]["dateTime"],
                        'end': event["end"]["dateTime"],
                        'editable': False,
                        'className': 'unavailableTime',
                        'overlap': True
                    }
                    output_events.append(temp_event)
                except Exception:
                    pass

        return output_events

    def get_contacts(self,async=True):
        """
        Given a user, pulls down all their Google Contacts and updates our DB with the info.

        If a google contact with the email address doesn't exist for the user, will create a new one;
        otherwise, will update the existing google contact with the data provided.
        """
        global external_id
        credential = CredentialsModel.get_credentials_from_auth(self.social_auth)
        if credential is None:
            self.log_exception('Google API could not load credentials')
            return 0
        auth_token = OAuth2TokenFromCredentials(credential)

        try:
            contacts_client = ContactsClient()
            auth_token.authorize(contacts_client)
        except AccessTokenRefreshError:
            # this probably means: "invalid_grant: Token has been revoked."
            self.log_exception('Google API AccessTokenRefreshError')
            return 0

        query = ContactsQuery()
        query.max_results = 10000  # hopefully we won't have to page with a max this high! but you never know...
        contacts_feed = contacts_client.get_contacts(q=query)
        contacts = [contact for contact in contacts_feed.entry]

        # paginate through all contacts
        next_link = contacts_feed.get_next_link()
        while next_link is not None:
            contacts_feed = contacts_client.get_contacts(uri=next_link.href)
            contacts += [contact for contact in contacts_feed.entry]
            next_link = contacts_feed.get_next_link()

        source = ImportContact.GOOGLE
        user_profile = self.user.profile

        bulk_create_contacts = []  # new Contact objects will be added to this list to be inserted to the DB in bulk
        google_contacts_query = ImportContact.objects.filter(user_profile=user_profile, source=source)
        google_contacts_by_id = dict((contact.external_id, contact) for contact in google_contacts_query)

        for contact in contacts:
            try:
                # required variables!
                emails = []
                for email in contact.email:
                    emails.append(email.address)
                
                if any("reply.linkedin.com" in s for s in emails):
                    continue

                first_name = contact.name.given_name.text if contact.name and contact.name.given_name else None
                last_name = contact.name.family_name.text if contact.name and contact.name.family_name else None
                
                if not first_name or not last_name or not len(emails)>0:
                    continue

                # optional variables
                addresses = []
                for address in contact.structured_postal_address:
                    addresses.append(address.formatted_address.text)
                title = contact.organization.title.text if contact.organization and contact.organization.title else ""
                company = contact.organization.name.text if contact.organization and contact.organization.name else ""
                phones = []
                for phone in contact.phone_number:
                    phones.append(phone.text)
                external_id = str(contact.id)
            except Exception as e:
                self.log_exception('Could not parse Google Contacts entry')
                continue

            try:
                if external_id in google_contacts_by_id:
                    contact = google_contacts_by_id[external_id]
                    created = False
                else:
                    contact = ImportContact(user_profile=user_profile, source=source, external_id=external_id)
                    created = True

                contact.data={"first_name":first_name,"last_name":last_name,"emails":emails,"phone_numbers":phones,"addresses":addresses,"title":title,"company":company}

                contact.auth_source = self.social_auth

                if created:
                    # if this contact did not exist in DB yet, we can insert it in bulk at the end
                    bulk_create_contacts.append(contact)
                elif contact.is_dirty(check_relationship=True):
                    # only save to DB if existing object's fields were modified
                    contact.save()
            except Exception as e:
                self.log_exception('Could not save Contact to DB')

        # now insert Contacts to DB in bulk for maximum efficiency
        if len(bulk_create_contacts):
            try:
                ImportContact.objects.bulk_create(bulk_create_contacts)
            except IntegrityError:
                # if there's an error saving in bulk, we'll have to do it the slow way: save each Contact individually
                for contact in bulk_create_contacts:
                    try:
                        contact.save()
                    except Exception as e:
                        self.log_exception('Could not save Contact to DB')

        if async:
            combine_contacts_async(self.user.profile.id, source)
        else:
            combine_contacts(self.user.profile.id, source)
        
        return len(ImportContact.objects.filter(user_profile=user_profile, source=source, external_id=external_id))

    def get_calendars(self):
        try:
            service = self._get_service('calendar')
        except Exception:
            self.log_exception('Google API could not get service')
            return []

        calendars = []
        try:
            for google_calendar in service.calendarList().list(minAccessRole="owner").execute()['items']:
                calendars.append({
                    'id': google_calendar['id'],
                    'name': google_calendar['summary'],
                    'provider_id': self.social_auth.id,
                    'provider_type': 'Google',
                    'provider_name': self.social_auth.extra_data.get('email'),
                })
        except AccessTokenRefreshError:
            # this probably means: "invalid_grant: Token has been revoked."
            self.log_exception('Google API AccessTokenRefreshError')
            return []
        return calendars

    def create_event(self, calendar_account, recipient, subject, body, location,
                     appt_start_date_time_tz_aware, appt_end_date_time_tz_aware):
        try:
            service = self._get_service('calendar')
        except Exception:
            self.log_exception('Google API could not get service')
            return

        soup = BeautifulSoup(body)
        description = soup.get_text('\n')

        event = {
            'summary': subject,
            'location': location,
            'description': description,
            'start': {
                'dateTime': appt_start_date_time_tz_aware.strftime('%Y-%m-%dT%H:%M:%S.000Z')
            },
            'end': {
                'dateTime': appt_end_date_time_tz_aware.strftime('%Y-%m-%dT%H:%M:%S.000Z')
            },
            'attendees': [
                {
                    'email': self.user.email,
                    'displayName': self.user.profile.full_name(),
                    'responseStatus': 'accepted'
                },
                {
                    'email': recipient.email,
                    'displayName': recipient.profile.full_name(),
                },
            ],
        }
        try:
            service.events().insert(calendarId=calendar_account.calendar_id, sendNotifications=True, body=event).execute()
        except AccessTokenRefreshError:
            # this probably means: "invalid_grant: Token has been revoked."
            self.log_exception('Google API AccessTokenRefreshError')
