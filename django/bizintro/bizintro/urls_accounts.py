from django.conf.urls import url
from bizintro.apis import account

urlpatterns = (
	url(r'^login$', account.LoginView.as_view(), name='api_login'),
	url(r'^signup$', account.SignupView.as_view(), name='api_signup'),
	url(r'^presignup$', account.PreSignupView.as_view(), name='api_presignup'),
	url(r'^autosignuporlogin$', account.AutoSignupView.as_view(), name='api_autosignup'),
)
