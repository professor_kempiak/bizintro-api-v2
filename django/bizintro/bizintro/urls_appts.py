from django.conf.urls import url
from bizintro.apis import appointment

urlpatterns = (
    url(r'^list$', appointment.ApptRequestListView.as_view(), name='all_appts'),
    url(r'^accept$', appointment.ApptRequestAcceptView.as_view(), name='accept_appt'),
    url(r'^rerequest$', appointment.ApptReRequestView.as_view(), name='rerequest_appt'),
    url(r'^$', appointment.ApptRequestCreateView.as_view(), name='appt'),
)
