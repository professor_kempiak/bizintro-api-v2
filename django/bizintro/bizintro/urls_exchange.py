from django.conf.urls import url
from bizintro.apis import exchange

urlpatterns = (
    url(r'^test_credentials$', exchange.TestCredentialsView.as_view(), name='test_creds'),
    url(r'^autodiscover$', exchange.AutoDiscoverView.as_view(), name='autodiscover'),
    url(r'^$', exchange.ExchangeView.as_view(), name='exchange'),
    url(r'^(?P<exchange_id>[0-9]+)$', exchange.ExchangeView.as_view(), name='exchange'),
)
