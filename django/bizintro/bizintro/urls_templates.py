from django.conf.urls import url
from bizintro.apis import template

urlpatterns = (
    url(r'^list$', template.TemplateListView.as_view(), name='all_templates'),
    url(r'^$', template.TemplateView.as_view(), name='template'),
)
