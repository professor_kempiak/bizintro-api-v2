from bizintro import api
from bizintro import views
from django.conf.urls import include
from django.conf.urls import patterns
from django.conf.urls import url
from django.contrib import admin
from django.views.generic.base import RedirectView
from rest_framework.urlpatterns import format_suffix_patterns
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^accept_timeslot/(?P<appt_req_uuid>[a-zA-Z0-9\-]+)/(?P<timeslot>.*)', views.accept_timeslot, name='accept_timeslot'),
                       url(r'^settings/', views.settingsPage, name='settings'),
                       url(r'^bizmetrics/', views.bizmetrics, name='bizmetrics'),
                       url(r'^appointments/requests/(?P<appt_req_uuid>[a-zA-Z0-9\-]+)', views.appt_req_detail, name='appt_req_detail'),
                       url(r'^appointments/(?P<appt_uuid>[a-zA-Z0-9\-]+)', views.appt_detail, name='appt_detail'),
                       url(r'^appointments/', views.appointments, name='appointments'),
                       url(r'^rerequest_appt/(?P<appt_req_uuid>[a-zA-Z0-9\-]+)', views.rerequest_appt, name='rerequest_appt'),
                       url(r'^accounts/external_accounts/', views.do_external, name='external'),
                       url(r'^password_set/$', views.password_set, name='password_set'),
                       url(r'^password_set/done/$', views.password_set_done, name='password_set_done'),
                       url(r'^password_change/$', views.password_change, name='password_change'),
                       url(r'^password_change/done/$', views.password_change_done, name='password_change_done'),
                       url(r'^password_reset/$', views.password_reset, name='password_reset'),
                       
                       url(r'^password_reset/done/$', views.password_reset_done, name='password_reset_done'),
                       url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.password_reset_confirm, name='password_reset_confirm'),
                       url(r'^reset/done/$', views.password_reset_complete, name='password_reset_complete'),
                       url(r'^toggle_archive/', api.toggle_archive, name='toggle_archive'),
                       
                       # email verification stuff
                       (r'^mail/', include('multimail.urls')),
                       
                       # Known Good URLs
                       url(r'^misc/', include("bizintro.urls_misc")),                        
                       url(r'^availability/', include("bizintro.urls_availability")),                        
                       url(r'^account/', include("bizintro.urls_accounts")), 
                       url(r'^contact/', include("bizintro.urls_contacts")), 
                       url(r'^introduction/', include("bizintro.urls_intros")), 
                       url(r'^appt_request/', include("bizintro.urls_appts")), 
                       url(r'^help/', include("bizintro.urls_helps")), 
                       url(r'^exchange/', include("bizintro.urls_exchange")), 
                       url(r'^profile/', include("bizintro.urls_profile")), 
                       url(r'^template/', include("bizintro.urls_templates")),
                       url(r'^google_start', TemplateView.as_view(template_name='google_start.html')), 
                       url(r'^google_return', TemplateView.as_view(template_name='google_return.html')), 

                       url(r'^admin/', include(admin.site.urls)),
                       
                       url('', include('social.apps.django_app.urls', namespace='social')),

                       url(r'^api/login/', include('rest_social_auth.urls_token')),

                       url(r'^payments/', include('djstripe.urls', namespace="djstripe")),


                       )
urlpatterns = format_suffix_patterns(urlpatterns)
