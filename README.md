## Synopsis
This is the codebase for Bizintro.

## Local Installation
1. Install Vagrant
2. Install Virtualbox
3. Clone the repo from Bitbucket as "bizintro-api"
4. Run "cd bizintro-api && env.sh" with any changes you need to make for your environment variables.
5. Run "vagrant up" - this starts the VM and maps the "bizintro-api" directory to "/home/django/code/bizintro/" in the VM
6. Run "vagrant ssh" to get onto the box as the "django" user
7. In the VM:
	"cd code/bizintro"
	"sh first-time.sh" (or run each of the commands within it one by one in order)
8. To redeploy code after you've made changes, run "sh redeploy.sh"

## Making changes to the model
1. Make changes to django/bizintro/bizintro/models.py
2. Run: "cd django/bizintro && sudo python manage.py makemigrations bizintro && cd -" // This creates a new migrations file which will need to be added to source control 
3. Run: "sh redeploy.sh"
4. Run: "cd ~/bizintro && sudo python manage.py migrate --fake-initial bizintro" // This actually makes the changes to the database on your environment. You'll need to do this everywhere you push this code (other developers, a remote server, etc)